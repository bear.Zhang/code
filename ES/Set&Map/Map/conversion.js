// Map转化数组 
const map=new Map();

map.set(true,7);
map.set({foo:3},['abc']);
console.log([...map]);//[ [ true, 7 ], [ { foo: 3 }, [ 'abc' ] ] ]

// 数组转化Map
var arr=[[{foo:3},['abc']],[true,7]];
const map1=new Map(arr);
console.log(map1);//Map { { foo: 3 } => [ 'abc' ], true => 7 }

