// 对象数组去重
let arrObj = [
    { name: 'a', num: 1},
    { name: 'b', num: 1},
    { name: 'c', num: 1},
    { name: 'd', num: 1},
    { name: 'a', num: 1},
    { name: 'a', num: 1},
    { name: 'a', num: 1},
    { name: 'b', num: 1},
]


// ES6对象数组所有属性去重,筛选每个数组项的字符
let map=new Map();
map.set(JSON.stringify({name:'a',num:1}),1);
console.log(map);//Map { '{"name":"a","num":1}' => 1 }
console.log(!map.has(JSON.stringify({name:'b',num:1})) && map.set(JSON.stringify({name:'b',num:1}),1));//Map { '{"name":"a","num":1}' => 1, '{"name":"b","num":1}' => 1 }
console.log(!map.has(JSON.stringify({name:'a',num:1})) && map.set(JSON.stringify({name:'a',num:1}),1));//false

function unique1(arr) {
    const map = new Map()
    return arr.filter( item => !map.has(JSON.stringify(item)) && map.set(JSON.stringify(item), 1))
}

console.log(unique1(arrObj));
// [ { name: 'a', num: 1 },
//   { name: 'b', num: 1 },
//   { name: 'c', num: 1 },
//   { name: 'd', num: 1 } ]

// ES6根据一维对象数组某个属性去重且该属性的值为简单数据类型，比较实用的一种的方法，也基本没有什么性能影响
function unique3(arr, key) {
    const map = new Map()
    return arr.filter((item) => !map.has(item[key] + '') && map.set(item[key] + '', 1))
}
console.log(unique3(arrObj));//[ { name: 'a', num: 1 } ]



