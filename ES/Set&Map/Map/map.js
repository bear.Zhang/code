const m=new Map();
const o={p:'hello World'};

m.set(o,'content');
console.log(m.get(o));//content

console.log(m.has(o));//true
console.log(m.delete(o));//true
console.log(m.has(o));//false

// Map的参数是数组
let map=new Map([
    ['name','张'],
    ['auth','title']
]);
console.log(map);//Map { 'name' => '张', 'auth' => 'title' }
console.log(map.size);//2
console.log(map.has('name'));//true
console.log(map.get('name'));//张

// Set生成新的Map

const set = new Set([
    ['foo', 1],
    ['bar', 2]
]);
const m1 = new Map(set);
console.log(m1.get('foo'));// 1

// 同一个键多次赋值后面的会覆盖前面

let map1=new Map();

map1.set(1,'a');
map1.set(1,'b');
console.log(map1.get(1));//b

// 只有对同一个对象的引用，Map 结构才将其视为同一个键
const map2=new Map();
map2.set(['a'],55);
console.log(map2.get(['a']));//undefined
