// 遍历操作

// keys()：返回键名的遍历器
let set=new Set(['red','blue','pink','green']);

console.log(set.keys());//[Set Iterator] { 'red', 'blue', 'pink', 'green' }

for(let i of set.keys()){
    console.log(i);
}
//red blue pink green

// values()：返回键值的遍历器 
console.log(set.values());//[Set Iterator] { 'red', 'blue', 'pink', 'green' }
for(let i of set.values()){
    console.log(i);
}
//red blue pink green

console.log(set.entries());//[Set Iterator] { 'red', 'blue', 'pink', 'green' }
for(let i of set.entries()){
    console.log(i);
}
//[ 'red', 'red' ] [ 'blue', 'blue' ] [ 'pink', 'pink' ] [ 'green', 'green' ]

// forEach()：使用回调函数遍历每个成员
set.forEach((item)=>{
    console.log(item);
})
//red blue pink green

// Set使用map和filter

let arrSet = new Set([1,2,3,4,5]);
arrSet= new Set([...arrSet].map(x=>x*2));
console.log(arrSet);//Set { 2, 4, 6, 8, 10 }

let filterSet=new Set([1,3,2,4,5,6]);
filterSet=new Set([...filterSet].filter(x=>(x%2)===0));
console.log(filterSet);//Set { 2, 4, 6 }