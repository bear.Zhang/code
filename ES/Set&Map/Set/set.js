var newSet=new Set();
[2, 3, 5, 4, 5, 2, 2].forEach(item=>newSet.add(item));

console.log(newSet);//Set { 2, 3, 5, 4 }

for(let i of newSet){
    console.log(i);
}
//2 3 5 4

// 通过...可以把Set变成数组
const set=new Set([1,2,3,4,4]);

console.log([...set]);//[ 1, 2, 3, 4 ]
console.log(Array.isArray([...set]));//true

// Set()实例有个size属性表示Set成员的总数
const items=new Set([1,2,3,4,5,5,5,5]);
console.log(items.size);//5

// Set() 内部算法类似与===，但是在Set()内部两个NaN是相等的，
let s=new Set();
let a=NaN;
let b=NaN;
s.add(a);
s.add(b);
console.log(s);//Set { NaN }

console.log(NaN===NaN);false

// 在Set()中两个对象总是不相等

let setObj=new Set();
setObj.add({});
console.log(setObj.size);//1
setObj.add({});
console.log(setObj.size);//2
