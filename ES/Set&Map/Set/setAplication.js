// 应用

// 数组去重
var arr=[1,2,4,5,5,4,3,6,2];
console.log([...new Set(arr)]);//[ 1, 2, 4, 5, 3, 6 ]

// 去掉字符串重复的字
var str='ababcaccab';
console.log([...new Set(str)].join(''));

// Set实现并集，交集，差集

let a=new Set([1,2,3,4,5]);
let b=new Set([1,5,6,3,8,7]);

// 并集
let union=new Set([...a,...b]);
console.log(union);//Set { 1, 2, 3, 4, 5, 6, 8, 7 }

// 交集
let instersect=new Set([...a].filter(x=>b.has(x)));
console.log(instersect);//Set { 1, 3, 5 }

let difference=new Set([...a].filter(x=>!b.has(x)));
console.log(difference);//Set { 2, 4 }

// 对象数组去重
let arrObj = [
    { name: 'a', num: 1},
    { name: 'b', num: 1},
    { name: 'c', num: 1},
    { name: 'd', num: 1},
    { name: 'a', num: 1},
    { name: 'a', num: 1},
    { name: 'a', num: 1},
    { name: 'b', num: 1},
]

function unique2(arr) {
    return [...new Set(arr.map(e => JSON.stringify(e)))].map(e => JSON.parse(e))
}
console.log(unique2(arrObj));
// [ { name: 'a', num: 1 },
//   { name: 'b', num: 1 },
//   { name: 'c', num: 1 },
//   { name: 'd', num: 1 } ]