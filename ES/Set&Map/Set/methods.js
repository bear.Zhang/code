var set=new Set();

// add(vlaue)：添加某个值，返回 Set 结构本身。
set.add(1).add(2).add(2).add(4);
console.log(set);//Set { 1, 2,4}

// has(value)：返回一个布尔值，表示该值是否为Set的成员。
console.log(set.has(2));//true
console.log(set.has(3));//false

//  delete(value)：删除某个值，返回一个布尔值，表示删除是否成功。

console.log(set.delete(2));//true
console.log(set.has(2));//false

// clear():清除所有成员，没有返回值。

set.clear();
console.log(set);//Set {}
