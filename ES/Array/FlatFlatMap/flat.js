// 扁平化n维数组
var arr1=[1,[2,3]];
// Node 11.X支持
//console.log(arr1.flat(2)); //[1,2,3]

var arr2=[1,[2,3,[4,5]]];
console.log(arr2.toString().split(',').map(Number)); //[ 1, 2, 3, 4, 5 ]