/**
 * 浅复制数组的一部分到同一数组中的另一个位置，并返回它，不会改变原数组的长度。
 */
let arr =[1,2,3,4,5,6];
console.log(arr.copyWithin(3));//[ 1, 2, 3, 1, 2, 3 ]

let arr1=[1,2,3,4,5,6];
console.log(arr1.copyWithin(3,1));//[ 1, 2, 3, 2, 3, 4 ]

let arr2=[1,2,3,4,5,6];
console.log(arr2.copyWithin(2,4,5));//[ 1, 2, 5, 4, 5, 6]