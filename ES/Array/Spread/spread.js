/**
 * 添加属性 
 */

//复制对象的同时，为其添加新的属性
const user={
    id:1,
    name:'zhang'
};
const userWithPass={...user,password:'12344'};
console.log(userWithPass);//{ id: 1, name: 'zhang', password: '12344' }

/**
 * 合并多个对象
 */

//利用...可以合并多个对象到一个新的对象

const part1={
    id:1,
    name:'zhang'
};
const part2={
    id:2,
    password:'12134'
};

const part={...part1,...part2};
console.log(part);//{ id: 2, name: 'zhang', password: '12134' }

/**
 * 移除对象属性 
 */

//结合使用解构和剩余操作符删除对象的属性

const user1={
    id:1,
    name:'zhang',
    password:'1111'
};
const noPassword=({password,...rest})=>rest;
console.log(noPassword(user1));// { id: 1, name: 'zhang' }

/**
 * 动态移除属性
 */
const user2={
    id:1,
    name:'zhang',
    password:'1111'
};

const removePrototy=prop=>({[prop]:_,...rest})=>rest;

const removePrototyPrassword=removePrototy('password');
const removePrototyId=removePrototy('id');

console.log(removePrototyPrassword(user2));// { id: 1, name: 'zhang' }
console.log(removePrototyId(user2));//{ name: 'zhang', password: '1111' }

/**
 * 调整属性顺序
 */

// 为了把id移到最前面，可以把 id: undefined 放在展开 object的前面：
const user3 = {
    password: 'Password!',
    name: 'Bruce',
    id: 300
}
  
const organize = object => ({ id: undefined, ...object })
console.log(organize(user3));// { id: 300, password: 'Password!', name: 'Bruce' }

// 要把 password 移动到最后位置，先从 object中解构出 password ，然后把 password 放在展开object的后面：
const user4 = {
    password: 'Password!',
    name: 'Bruce',
    id: 300
}
const organize = ({ password, ...object }) =>({ ...object, password });
console.log(organize(user4));// { name: 'Bruce', id: 300, password: 'Password!' }

/**
 * 设置属性默认值
 */

// 当对象不存在某个属性时，需要给对象添加这个属性，并设置一个默认值。
const user5 = {
    id: 200,
    name: 'Jack Ma'
}
  
const user6 = {
    id: 400,
    name: '鲁迅',
    quotes: ["我没说过这句话……"]
}
  
//const setDefaults = ({ quotes = [], ...object}) =>({ ...object, quotes });
// 如果想让这个属性在最前面，可以这样写：
const setDefaults = ({ ...object}) => ({ quotes: [], ...object });

console.log(setDefaults(user5));// { id: 200, name: 'Jack Ma', quotes: [] }
// 如果此属性存在则保持不变
console.log(setDefaults(user6));// { id: 400, name: '鲁迅', quotes: [ '我没说过这句话……' ] }


/**
 * 属性重命名 
 */

// 先从object中解构出ID的值，然后再把这个值合并到新对象里，改成小写的id。
const renamed = ({ ID, ...object }) => ({ id: ID, ...object });
const user7 = {
  ID: 500,
  name: "zhang"
}
console.log(renamed(user7));// { id: 500, name: 'zhang' }

/**
 * 根据条件动态添加属性
 */

// 我们只在 password有值的情况才添加该属性
const user8 = { id: 110, name: 'Kayson Li' }
const password ='Password!';
const userWithPassword1 = {
  ...user8,
  id: 100,
  ...(password && { password })
}

console.log(userWithPassword1);// { id: 100, name: 'Kayson Li', password: 'Password!' }