/**
 * Array.prototype.keys()、Array.prototype.values()、Array.prototype.entries()
 */
let arr=[1,2,3,4,5];
let arrKeys,arrValues,arrEntries;
arrKeys=arr.keys();
for(let key of arrKeys){
    console.log(`key:${key}`);
}

arrValues=arr.values();
for(let val of arrValues){
    console.log(`val:${val}`);
}

arrEntries=arr.entries();
console.log(arrEntries.next().value);
for(let keyVal of arrEntries){
    console.log(`key：${keyVal[0]}-val：${keyVal[1]}`);
}

console.log(arrEntries.next());
var arrs = ["a", "b", "c"];
var iterator = arrs.entries();
console.log(iterator.next());