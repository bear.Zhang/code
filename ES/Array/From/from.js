// 给每一个类数组项乘2
const someNumbers = { '0': 10, '1': 15, length: 2 };

let newArr=Array.from(someNumbers, value => value * 2); 

console.log(newArr);//[ 20, 30 ]


// 求和
function sumArguments() {
    return Array.from(arguments).reduce((sum, num) => sum + num);
}

console.log(sumArguments(1, 2, 3)); // 6