let arr=[1,2,3];
console.log(arr.fill(6));//[ 6, 6, 6 ]

let arr1=[1,2,3];
console.log(arr1.fill(6,1));//[ 1, 6, 6 ]

let arr2=[1,2,3];
console.log(arr2.fill(6,1,2));//[ 1, 6, 3 ]
