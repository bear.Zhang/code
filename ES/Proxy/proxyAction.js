// proxy代理数据
let backAccount={
    balance:2021,
    name:'ZhangYanXiong'
},handle={
    get:function(target,prop,receiver){
        if(prop=='balance'){
            console.log(`Current Balance Of: ${target.name} Is: ${target.balance} `);
        }
        return target[prop];
    },
    set:function(target,prop,value,receiver){
        console.log(`Current Balance: ${target.balance}, New Balance: ${value}`)
        if(value<0){
            console.log(`We don't allow Negative Balance!`);
            return false;
        }
        target[prop]=value;
        return true;
    }
};

let wrappedBackAccount=new Proxy(backAccount,handle);
console.log(wrappedBackAccount.balance);
wrappedBackAccount.balance-=2000;
console.log(wrappedBackAccount.balance);
wrappedBackAccount.balance-=50;
console.log(wrappedBackAccount.balance);