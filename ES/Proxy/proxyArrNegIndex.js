// 数组负索引取值
const negativeArray=target=>new Proxy(target,{
    get:(target,k)=>{
        return Reflect.get(target,+k<0?String(target.length+ +k):k)
    }
});

let arr=[1,2,3,4];
console.log(typeof negativeArray(arr)[-2]);