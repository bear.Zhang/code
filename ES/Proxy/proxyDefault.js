// proxy修改默认值
const defaultValueObj=(target,defaultValue)=>new Proxy(target,{
    get:(target,prop)=>{
        return Reflect.has(target,prop)?target[prop]:defaultValue;
    }
})

let obj={
    name:'水果',
    price:12,
};
console.log(defaultValueObj(obj,0).level);
