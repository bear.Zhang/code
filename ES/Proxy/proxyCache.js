// Proxy缓存数据
let backAccount={
    balance:10,
    name:'ZhangYanXiong',
    get dollars(){
        console.log('Calculating Dollars');
        return this.balance*3.43008459;
    }
},cache={
    curBalance:null,
    curValue:null
}

handle={
    get:function(target,prop){
        if(prop=='dollars'){
            let value=cache.curBalance!==target.balance?target[prop]:cache.curValue;
            cache.curBalance=target.balance;
            cache.curValue=value;
            return value;
        }
        return target[prop];
    }
};

let wrappedBackAccount=new Proxy(backAccount,handle);
console.log(wrappedBackAccount.dollars);
console.log(wrappedBackAccount.dollars);