// proxy隐藏属性
const enablePrivate=(target,prefix='_')=>new Proxy(target,{
    has:(target,k)=>(!k.startsWith(prefix) && k in target),
    ownKeys:(target)=>Reflect.ownKeys(target).filter(k=>(typeof k !== 'string'||!k.startsWith(prefix))),
    get:(target,k,rec)=>(k in rec)?target[k]:undefined
})
let userData = enablePrivate({
    firstName: 'Tom',
    mediumHandle: '@tbarrasso',
    _favoriteRapper: 'Drake'
});  
console.log(userData._favoriteRapper);// undefined
console.log(('_favoriteRapper' in userData)); // false
console.log(Object.keys(userData));// ['firstName', 'mediumHandle']