// 重载操作符
const rang=(min,max)=>new Proxy(Object.create(null),{
    has:(target,k)=>{
        return (+k>min&&+k<max)
    }
})

const X=10.5;
const nums = [1, 5, X, 50, 100];

if(X in rang(1, 100)){
    console.log('we');
}
let num=nums.filter(n=>n in rang(1,10));
console.log(num);