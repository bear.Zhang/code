// 只读属性
const nope = () => {
    throw new Error('不能改变只读属性')
}
const read_only = (target) => new Proxy(target, {
    set: nope,
    defineProperty: nope,
    deleteProperty: nope,
    preentExtensions: nope,
    setPrototypeOf: nope
});
let obj={
    name:'TOM',
    age:1
}
read_only(obj).name='Marry';// Error: 不能改变只读属性
console.log(read_only(obj).name);