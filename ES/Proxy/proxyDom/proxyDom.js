// proxy操作DOM
let backAccount={
    balance:2020,
    name:'ZhangYanXiong',
    get text(){
        return `${this.name} Balance Is: ${this.balance}`;
    }
};

const objectWithDom=(obj,domId)=>{
    const handle={
        set:function(target,prop,value){
            target[prop]=value;
            document.getElementById(domId).innerHTML=obj.text;
            return true;
        }
    };
    return new Proxy(obj,handle);
}

let wrappedBackAccount=objectWithDom(backAccount,'back-account');
wrappedBackAccount.balance=26;
wrappedBackAccount.balance=1000;