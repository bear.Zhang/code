// proxy枚举
const createEnum=(targt)=>new Proxy(targt,{
    get:(targt,k)=>{
        if(k in targt){
            return Reflect.get(targt,k);
        }
        throw new ReferenceError(`找不到属性${k}`)
    }
})
let obj={
    name:'TOM',
    age:1
}
console.log(createEnum(obj).age1);