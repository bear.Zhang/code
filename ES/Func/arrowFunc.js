/**
 * 箭头函数
 */

// this绑定
// 普通函数并不是通过上下文作用域来决定this的值，而是通过实际调用函数的对象来决定的。
let printNumberOld={
    phrase:'The current value is',
    numbers:[1,2,3,4,5],
    loop(){
        this.numbers.forEach(function(item){
            console.log(this.phrase,item);
        })
    }
}
printNumberOld.loop();
// undefined 1
// undefined 2
// undefined 3
// undefined 4
// undefined 5

// 箭头函数中的this是由上下文作用域决定的。
let printNumber={
    phrase:'The current value is',
    numbers:[1,2,3,4,5],
    loop(){
        this.numbers.forEach((item)=>{
            console.log(this.phrase,item);
        })
    }
};
printNumber.loop();
// The current value is 1
// The current value is 2
// The current value is 3
// The current value is 4
// The current value is 5