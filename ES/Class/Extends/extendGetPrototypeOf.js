// class继承

class Parent {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}

class Child extends Parent {
    constructor(name, age, salary) {
        super(name, age)
        this.salary = salary
    }
}

console.log(Object.getPrototypeOf(Child));//[Function: Parent]

console.log(Object.getPrototypeOf(Child)===Parent);//true