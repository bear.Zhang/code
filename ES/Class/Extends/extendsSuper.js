// super作为函数调用
class Parent {
    constructor() {
      console.log(new.target.name);
    }
}
class Child extends Parent {
    constructor() {
      super();
    }
}
new Parent() // Parent
new Child() // Child

// super作为对象
class Parent1 {
  constructor(){
    this.name=1;
  };
  fn(){
    return 2;
  }
}
class Child1 extends Parent1 {
  constructor() {
    super();
    console.log(super.fn());//2
  };
  static staticFn(){
    console.log(super.name);//Parent1
  };
  parentData(){
    return super.name
  }
}
Child1.staticFn();
let c1=new Child1();
console.log(c1.parentData());//undefined

class Parent2 {

}

Parent2.prototype.name=5;
class Child2 extends Parent2 {
  constructor() {
    super();
    console.log(super.name);//5
  };
}
let c2=new Child2();

