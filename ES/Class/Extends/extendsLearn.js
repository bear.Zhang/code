// class继承,子类必须在constructor方法中调用super方法

class Parent {
    //调用类的构造方法
    constructor(name, age) {
        this.name = name
        this.age = age
    }
}
class Child extends Parent {
    constructor(name, age, salary) {
        this.salary = salary 
    };
    fn(){
        super()//SyntaxError: 'super' keyword unexpected here
    }
}
let c = new Child('wade', 38, 1000000000);

class Parent1 {
    constructor() {
      this.x = 1;
    }
  }
  
  class Child1 extends Parent1 {
    constructor() {
      super();
      this.x = 2;
      super.x = 3;
      console.log(super.x); // undefined
      console.log(this.x); // 3
    }
  }
  
  let c = new Child1();


