// class继承

class Parent {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    static staticFn(){
        console.log('父级静态方法');
    }
}

class Child extends Parent {
    constructor(name, age, salary) {
        super(name, age)
        this.salary = salary
    }
}

Child.staticFn();//父级静态方法