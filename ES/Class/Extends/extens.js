// class继承

class Parent {
    //调用类的构造方法
    constructor(name, age) {
        this.name = name
        this.age = age
    }
    //定义一般的方法
    showName() {
        console.log("调用父类的方法")
        console.log(this.name, this.age);
    }
}

let p = new Parent('kobe', 39);
console.log(p);//Parent { name: 'kobe', age: 39 }
//定义一个子类
class Child extends Parent {
    constructor(name, age, salary) {
        super(name, age) //通过super调用父类的构造方法
        this.salary = salary
    }
    showName() { //在子类自身定义方法
        console.log("调用子类的方法")
        console.log(this.name, this.age, this.salary);
    }
}
let c = new Child('wade', 38, 1000000000)
console.log(c)
c.showName()//Child { name: 'wade', age: 38, salary: 1000000000 }