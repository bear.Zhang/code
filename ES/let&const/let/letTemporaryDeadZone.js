/*暂存死区*/ 
function do_something(){
    console.log(a);//undefined
    console.log(b);//ReferenceError: b is not defined
    var a='a';
    let b='b';      
}
//do_something()

/*暂存死区与typeof*/
console.log(typeof n);//undefined

console.log(typeof i);//undefined
var i=1;

console.log(typeof j);//ReferenceError: j is not defined
let j=2;

console.log(typeof n);
