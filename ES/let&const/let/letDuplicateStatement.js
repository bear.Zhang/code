/*重复声明*/
let x=1;

// let x=true;
// if(x){
//     let a=10;
//     let a=10;
//     //SyntaxError: Identifier 'a' has already been declared
// }

// if(x){
//     let b=1;
//     var b=1;
//     //SyntaxError: Identifier 'b' has already been declared
// }

switch(x) {
  case 0:
    let foo;
    break;
    
  case 1:
    let foo; // SyntaxError for redeclaration.
    break;
}