/*let、var作为变量循环定时器*/
var a=1;
let b=2;


// console.log(window.a);//1
// console.log(window.b);//undefined
console.log(global.a);//undefined
console.log(global.b);//undefined
console.log(process.a);//undefined
console.log(process.b);//undefined

for(let i=0;i<=5;i++){
    setTimeout(()=>{
      console.log(i);
    },0)
}
// 0 1 2 3 4 5

for(var i=0;i<=5;i++){
    setTimeout(()=>{
        console.log(i);
    },10)
}
// 6 6 6 6 6 6 

for(var i=0;i<=5;i++){
    (function(a){
        setTimeout(()=>{
        console.log(a)
        },10)
    })(i);
}
// 0 1 2 3 4 5

// var存在变量提升
for(var i=0 ;i<5;i++){
    console.log(i); // 0 1 2 3 4 
}
console.log(i); // 5

for(let i=0 ;i<5;i++){
    console.log(i); // 0 1 2 3 4 
}
console.log(i); // ReferenceError: i is not defined




