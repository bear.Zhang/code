/*模仿私有成员*/
var Thing;
{
  let privateScope = new WeakMap();
  let counter = 0;
  Thing = function() {
    this.someProperty = 'foo';
    privateScope.set(this, {
      hidden: ++counter,
    });
  };

  Thing.prototype.showPublic = function() {
    return this.someProperty;
  };

  Thing.prototype.showPrivate = function() {
    return privateScope.get(this).hidden;
  };
}

console.log(typeof privateScope);// undefined

var thing = new Thing();

console.log(thing);// Thing {someProperty: "foo"}

console.log(thing.showPublic());// "foo"

console.log(thing.showPrivate());// 1