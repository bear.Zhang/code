/*作用域规则*/
{
    var a=10;
    let b=5;
}

console.log(a);// 10

//console.log(b);//ReferenceError: b is not defined
for(var i=0;i<5;i++){
    console.log(i);// 1 2 3 4
}
console.log(i);// 5

for(let i=0;i<5;i++){
    console.log(i);// 0 1 2 3 4
}
console.log(i);//ReferenceError: i is not defined

for(let i=0;i<2;i++){
    let i=6;
    console.log(++i);// 7 7
}

for(var i=0;i<2;i++){
    var i=6;
    console.log(++i);// 7
}

var a = [];
for (var i = 0; i < 10; i++) {
  a[i] = function () {
    console.log(i);
  };
}
a[6](); // 10

var a = [];
for (let i = 0; i < 10; i++) {
  a[i] = function () {
    console.log(i);
  };
}
a[6](); // 6