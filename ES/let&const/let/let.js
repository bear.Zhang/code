let x=1;
if(x==1){
  let x=2;
  console.log(x);//2
}
console.log(x);//1

/*变量提升问题*/
console.log(foo); // 输出undefined
var foo = 2;

console.log(bar); // 报错ReferenceError: bar is not defined
let bar = 2;

/*暂时性死区*/
var tmp=123;
if(true){
    tmp='abc';// ReferenceError: tmp is not defined
    let tmp;
}

/*块级作用域与函数声明 */
function f() { console.log('I am outside!'); }

(function () {
  if (false) {
    // 重复声明一次函数f
    function f() { console.log('I am inside!'); }
  }

  f();
}());//TypeError: f is not a function