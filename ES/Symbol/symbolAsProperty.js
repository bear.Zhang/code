// Symbol 作为属性
let s=Symbol();

// 第一种
let a={};
a[s]='hello symbol!';
console.log(a[s]);//hello symbol!

// 第二种
let b={
    [s]:'hello Symbol!'
};

console.log(b[s]);//hello Symbol!

// 第三种
let c={};
Object.defineProperty(c,s,{value:'Hello Symbol'});
console.log(c[s]);//Hello Symbol