let s=Symbol();
console.log(typeof s);//symbol

// Symbol的参数
let s1=Symbol('foo');
let s2=Symbol('boz');

console.log(s1);//Symbol(foo)
console.log(s2);//Symbol(boz)

let obj={
    toString(){
        return 'abc'
    }
}
let s3=Symbol(obj);
console.log(s3);//Symbol(abc)

let s4=Symbol('sym');
//console.log('your symbol is'+s4);//TypeError: Cannot convert a Symbol value to a string
console.log(s4.toString())//Symbol(sym)
console.log(Boolean(s4));//true
//console.log(Number(s4))//TypeError: Cannot convert a Symbol value to a number
