// Promise执行方式

// Promise 构造函数是同步执行的，promise.then 中的函数是异步执行的。
var promise=new Promise((resolve,reject)=>{
    console.log(1);
    resolve('fulfilled');
    console.log(2);
});

promise.then((res)=>{
    console.log(3);
    console.log(res);
})

// 1 2 3 fulfilled