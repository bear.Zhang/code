// 链式调用

var promise=new Promise((resolve,reject)=>{
    resolve(1);
});

promise.then((res)=>{
    console.log('then1:',res);//then1: 1
    return 2;
}).catch((rej)=>{
    console.log(rej);
}).then((res)=>{
    console.log('then2:',res);//then2: 2
})