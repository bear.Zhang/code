let promise=new Promise((resolve,reject)=>{
    resolve(1)
});

promise.finally(()=>{
    console.log(1)
})

// finally()与.then()、.catch()不同
let a=Promise.resolve(2).then(()=>{
},()=>{})

console.log(a);

let b=Promise.resolve(2).finally(()=>{
})

console.log(b)

