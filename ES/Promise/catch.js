let promise=new Promise((resolve,reject)=>{
    throw new Error('Error');
})
promise.catch((err)=>{
    console.log('发生错误：',err);
})

let promise1=new Promise((resolve,reject)=>{
    reject(new Error('Test'))
})
promise1.then(null,(err)=>{
    console.log('发生错误：',err);
})

let promise2=new Promise((resolve,reject)=>{
    resolve('ok');
    throw new Error('ok错误');
})
promise2.then((res)=>{
    console.log(res);
}).catch((err)=>{
    console.log('发生错误：',err);
})

const someAsynThing=function(){
    return new Promise(function(resolve,reject){
        //报错，x没有定义
        resolve(x+2);
    });
};
someAsynThing().then(function(){
    console.log('every thing is great');
});
setTimeout(()=>{console.log(123)},2000);

