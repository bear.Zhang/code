var promise1=new Promise((resolve,reject)=>{
    resolve('fulfilled');
});
console.log('promise1:',promise1);//promise1: Promise { 'fulfilled' }

promise1.then((res)=>{
    console.log('then:',res);
})

var promise2=new Promise((resolve,reject)=>{
    reject('rejected');
});

console.log('promise2:',promise2);//promise2: Promise { <rejected> 'rejected' }

promise2.then((res)=>{
    console.log("then:",res);
})
.catch((rej)=>{
    console.log("catch:",rej);//catch:rejected
})

//promise1 Promise { 'fulfilled' } promise2 Promise { <rejected> 'rejected' } then: fulfilled catch: rejected
