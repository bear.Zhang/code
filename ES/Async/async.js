const { reject } = require("async");

function timeout(ms) {
    return new Promise((resolve) => {
        console.log('await');// 1
        setTimeout(resolve, ms);
    });
}

async function asyncPrint(value, ms) {
    await timeout(ms);
    console.log('awaitAfter',value);// 2
}

asyncPrint('hello world', 1000).then(()=>{
    console.log('asyncThen');// 3
});

// await和for...of配合使用
function processItem(item){
    return new Promise((resolve,reject)=>{
        resolve(item);
    })
}

async function processResponse(someItems){
    for await (let item of someItems){
        let result=processItem(item);
        result.then((res)=>{
            console.log('then',res);
        })
    }
}

processResponse([1,2,3]);

async function processResponseLast(someItems){
    for(let item of someItems){
        let result=await processItem(item);
        console.log(result);
    }
}

processResponseLast([1,2,3]);
