// Object.assign()
const target = { a: 1 };

const source1 = { b: 2 };
const source2 = { c: 3 };

Object.assign(target, source1, source2);
console.log(target);//{ a: 1, b: 2, c: 3 }

const target1 = { a: 1, b: 1 };

const source3 = { b: 2, c: 2 };
const source4 = { c: 3 };

Object.assign(target1, source3, source4);
console.log(target1);//{ a: 1, b: 2, c: 3 }

//Object.assign(undefined);//TypeError: Cannot convert undefined or null to object
//Object.assign(null);//TypeError: Cannot convert undefined or null to object

const v1 = 'abc';
const v2 = true;
const v3 = 10;

const obj = Object.assign({}, v1, v2, v3);
console.log(obj); // { "0": "a", "1": "b", "2": "c" }

console.log(Object(true));
console.log(Object(10));
console.log(Object('abc'));



