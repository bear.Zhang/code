import React,{Component} from 'react';
import { Link } from 'react-router-dom';

class Index extends Component{
    constructor(props){
        super(props);
        this.state={
            list:[
                {
                    id:1,
                    title:'item1'
                },
                {
                    id:2,
                    title:'item2'
                },
                {
                    id:3,
                    title:'item3'
                }
            ]
        }
    };
    render(){
        return(
            <div>
                <ul>
                    {
                        this.state.list.map((item,index)=>{
                            return(
                                <li key={index}>
                                    <Link to={`/list/`+item.id}>{item.title}</Link>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        )
    }
}

export default Index