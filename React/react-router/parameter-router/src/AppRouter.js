import React from 'react';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';
import Index from './pages/Index';
import List from './pages/List';

/**
 * 动态传值：
 * 1、设置规则
 * 2、传递值
 * 3、接受值
 * 4、显示内容 
 */

function AppRouter(){
  return(
    <Router>
      <ul>
        <li><Link to="/">首页</Link></li>
        {/* 传值 */}
        <li><Link to="/list/123">列表</Link></li>
      </ul>
      <Route path="/" exact component={Index}/>
      {/* 设置规则 */}
      <Route path="/list/:id" exact component={List}/>
    </Router>
  )
}

export default AppRouter;
