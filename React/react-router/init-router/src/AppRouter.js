import React from 'react';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';

/**
 * 页面分离
 */
import Index from './pages/Index';
import List from './pages/List';

/**
 * 无状态组件 
 */
function AppNoStatus() {
  return <h2>ReactRouter=>无状态首页</h2>
}

function ListNoStatus(){
  return <h2>ReactRouter=>无状态列表</h2>
}

function AppRouter(){
  return(
    <Router>
      <ul>
        <li><Link to="/">首页</Link></li>
        <li><Link to="/list/">列表</Link></li>
      </ul>
      <Route path="/" exact component={Index}/>
      <Route path="/list/" exact component={List}/>
    </Router>
  )
}

export default AppRouter;
