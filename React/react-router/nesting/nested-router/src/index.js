import React from 'react';
import ReactDOM from 'react-dom';

// import App from './App';

// 动态配置路由
import AppRouter from './AppRouter';

ReactDOM.render(<AppRouter />, document.getElementById('root'));
