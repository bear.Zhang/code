import React from 'react';
import {HashRouter,Route,Link} from 'react-router-dom'

import HomeCss from '@/components/home/Home.css';
import less from '@/components/home/homel.less';

export default class Home extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'我是Home'
        }
    };
    render(){
        return (<div>
            {this.state.msg}
        </div>)
    }
}