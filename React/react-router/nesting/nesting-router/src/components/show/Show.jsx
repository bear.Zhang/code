import React from 'react';
import {HashRouter,Route,Link} from 'react-router-dom'

export default class Show extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'我是Show'
        }
    };
    componentDidMount(){
        console.log('dd',this.props.routes);
    };
    componentWillMount(){
        console.log(this.props.routes);
    }
    render(){
        return (<div>
            <h2>{this.state.msg}</h2>
            <Link to="/Show/ShowList" replace>列表</Link>
            <Link to="/Show/ShowDetail" replace>详情</Link>
            {
                this.props.routes.map((route,key)=>{
                    if(route.exact){
                        return <Route key={key} path={route.path} component={route.component} exact />
                    }else{
                        return <Route key={key} path={route.path} component={route.component} exact/>
                    }
                })
            }
        </div>)
    }
}