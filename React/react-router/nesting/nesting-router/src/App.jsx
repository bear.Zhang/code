import React from 'react';
import {HashRouter,Route,Link} from 'react-router-dom'

import AppCss from '@/App.css';

import route from '@/router/route'

export default class App extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:"App"
        };
    };
    render(){
        return (<div>
            <HashRouter>
                <Link to="/" replace>首页</Link><br/>
                <Link to="/Nav" replace>导航</Link><br/>
                <Link to="/Show" replace>展示</Link><br/>
    
                {
                    route.map((item,key)=>{
                        if(item.exact){
                            return <Route key={key} path={item.path} component={item.component} exact></Route>
                        }else{
                            return <Route key={key} path={item.path} render={props=>(
                                    <item.component {...props} routes={item.routes}/>
                                )}
                            />
                        }
                    })
                }
            </HashRouter>
        </div>)
    }
}