import Home from '@/components/home/Home';
import Show from '@/components/show/Show';
import ShowList from '@/components/show/showList/ShowList';
import ShowDetail from '@/components/show/showList/ShowDetail';
import Nav from '@/components/nav/Nav';

let route=[
    {
        path:'/',
        component:Home,
        exact:true
    },
    {
        path:'/Nav',
        component:Nav
    },
    {
        path:'/Show',
        component:Show,
        routes:[
            {
                path:'/Show/ShowList',
                component:ShowList
            },
            {
                path:'/Show/ShowDetail',
                component:ShowDetail
            }
        ]
    }
]

export default route;