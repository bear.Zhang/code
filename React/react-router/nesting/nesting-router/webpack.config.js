const path=require('path');
const htmlWebpackPlugin=require('html-webpack-plugin');

const htmlPlugin=new htmlWebpackPlugin({
    template:path.join(__dirname,'./src/index.html'),
    filename:'index.html'
})

module.exports={
    mode:"development",
    plugins:[
        htmlPlugin
    ],
    module:{
        rules:[
            {test:/\.js|jsx$/,use:'babel-loader',exclude:/node_modules/},
            {test:/\.css$/,use:["style-loader",{
                loader:'css-loader',
                options:{
                    modules: true
                }
            }]},
            {test:/\.less$/,use:['style-loader','css-loader','less-loader']}
        ]
    },
    resolve:{
        extensions:['.js','.jsx','.json'],
        alias:{
            "@":path.join(__dirname,"./src")
        }
    }
}