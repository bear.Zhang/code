import React from 'react';
import {BrowserRouter,Route,Link,Redirect} from 'react-router-dom'

export default class Show extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'我是Show'
        }
    };
    render(){
        return (<div>
            <BrowserRouter>
                <div>{this.state.msg}</div>
                <Link to="/Show/Login">登录</Link>
                {
                    this.props.routes.map((route,key)=>{
                        if(route.exact){
                            return <Route key={key} path={route.path} exact render={props=>(
                                <route.component {...props} routes={route.routes}/>
                            )}/>
                        }else{
                            return <Route key={key} path={route.path} render={props=>(
                                <route.component {...props} routes={route.routes}/>
                            )}/>
                        }
                    })
                }
            </BrowserRouter>
        </div>)
    }
}