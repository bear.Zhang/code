import React from 'react';
import {BrowserRouter,Route,Link} from 'react-router-dom';

import AppCss from '@/App.css';

import route from '@/router/router';

export default class App extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'APP'
        };
    };
    changeStatus=()=>{
        this.setState({
            isLogin:!this.state.isLogin
        });
    };
    render(){
        return (<div>
            <BrowserRouter>
                <Link to="/">首页</Link>
                <Link to="/Show">展示</Link>
                {
                    route.map((route,key)=>{
                        if(route.exact){
                            return <Route key={key} path={route.path} exact render={props=>(
                                <route.component {...props} routes={route.routes}/>
                            )}/>
                        }else{
                            return <Route key={key} path={route.path} render={props=>(
                                <route.component {...props} routes={route.routes}/>
                            )}/>
                        }
                    })
                }
            </BrowserRouter>
        </div>)
    }
}