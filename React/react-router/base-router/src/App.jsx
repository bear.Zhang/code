import React from 'react';
import {HashRouter,Route,Link} from 'react-router-dom'

import Home from '@/components/home/Home'
import Query from '@/components/query/Query'
import Params from '@/components/params/Params'
import State from '@/components/state/State'
import Search from '@/components/search/Search'

import AppCss from '@/App.css';

export default class App extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:"App"
        };
    };
    render(){
        return (<div>
            <HashRouter>
                <Link to="/Home">首页</Link><br/>
                <Link to={{pathname:'/Query',query:{name:'Query'}}}>Query</Link><br/>
                <Link to={{pathname:'/State',state:{name:'State'}}}>State</Link><br/>
                <Link to="/Params/top250/10">Params</Link><br/>
                <Link to="/Search?id=2323">Search</Link>

                <Route path="/Home" component={Home}></Route>
                <Route path="/Query" component={Query}></Route>
                <Route path="/State" component={State}></Route>
                <Route path="/Params/:type/:id" component={Params}></Route>
                <Route path="/Search" component={Search}></Route>
            </HashRouter>
        </div>)
    }
}