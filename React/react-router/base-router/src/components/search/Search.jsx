import React from 'react';

export default class Search extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'我是Search'
        };
    };
    render(){
        return(<div>
            <div>
                {this.state.msg}
                {this.props.location.search}
            </div>
        </div>)
    }
}