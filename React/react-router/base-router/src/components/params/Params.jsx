import React from 'react';

export default class Params extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'我是Params',
            RouterParams:this.props.match.params
        }
    };
    render(){
        return (<div>
            <div>
                {this.state.msg}<br/>
                {this.props.match.params.type}<br/>
                {this.props.match.params.id},
                {this.state.RouterParams.type},
                {this.state.RouterParams.id}
            </div>
        </div>)
    }
}