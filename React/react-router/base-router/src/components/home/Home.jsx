import React from 'react';

import HomeCss from '@/components/home/Home.css';

import less from '@/components/home/homel.less';

export default class Home extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'我是Home'
        }
    };
    render(){
        return (<div>
            <div className="mssg">{this.state.msg}</div>
        </div>)
    }
}