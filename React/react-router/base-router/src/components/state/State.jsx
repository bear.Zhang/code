import React from 'react';

export default class State extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'我是State'
        };
    };
    render(){
        return(<div>
            <div>
                {this.state.msg}
                {this.props.location.state.name}
            </div>
        </div>)
    }
}