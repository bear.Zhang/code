import React from 'react';

export default class Query extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'我是Query',
            RouterQuery:this.props.location.query
        };
    };
    render(){
        return(<div>
            <div>
                {this.state.msg}
                {this.state.RouterQuery.name}
            </div>
        </div>)
    }
}