import React,{Component} from 'react';

class List extends Component{
    constructor(props){
        super(props);
        this.state={
            id:null
        }
    };
    componentDidMount(){
        console.log(this.props.match.params.id);
        let id=this.props.match.params.id;
        this.setState({
            id:id
        });
    };
    render(){
        return(
        <h2>ReactRouter=>状态列表{this.state.id}</h2>
        )
    }
}

export default List