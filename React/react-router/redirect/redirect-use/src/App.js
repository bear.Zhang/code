import React from 'react';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';
import Index from './pages/Index';
import List from './pages/List';
import Home from './pages/Home';

function App(){
  return(
    <Router>
      <ul>
        <li><Link to="/">首页</Link></li>
        {/* 传值 */}
        <li><Link to="/list/123">列表</Link></li>
      </ul>
      
      <Route path="/" exact component={Index}/>
      {/* 设置规则 */}
      <Route path="/list/:id" component={List}/>
      <Route path="/home/" component={Home}/>
    </Router>
  )
}

export default App;
