import React from 'react';
import {HashRouter,Route,Link,Redirect} from 'react-router-dom'

export default class AuthButton extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'我是AuthButton'
        }
    };
    signOut=()=>{
        this.props.changeStatus();
    };
    render(){
        return (<div>
            {
                this.props.isLogin?<div><Redirect to={{pathname:'/',state:{adsasd:'dd'}}}/><button onClick={()=>{this.signOut()}}>退出</button></div>:<h3>没有登录</h3>
            }
        </div>)
    }
}