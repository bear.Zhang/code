import React from 'react';
import {HashRouter,Route,Link,Redirect} from 'react-router-dom'

export default class Show extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'我是Show'
        }
    };
    componentWillMount(){
        
    }
    componentDidMount(){
       
    }
    login(){
        this.props.changeStatus();
        this.props.history.push('/AuthButton');
    };
    render(){
        return (<div>
            {
                 this.props.isLogin?<h2>{this.state.msg}</h2>:<div><h2>{this.state.msg}</h2><button onClick={()=>this.login()}>登录</button></div>
            }
            {
                this.props.routes.map((route,key)=>{
                    if(route.exact){
                        return <Route key={key} path={route.path} component={route.component} exact />
                    }else{
                        return <Route key={key} path={route.path} component={route.component} exact/>
                    }
                })
            }
        </div>)
    }
}