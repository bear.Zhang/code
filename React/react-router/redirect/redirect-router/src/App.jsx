import React from 'react';
import {HashRouter,Route,Link} from 'react-router-dom';

import AuthButton from '@/components/common/authButton/AuthButton'

import AppCss from '@/App.css';

import route from '@/router/route';

export default class App extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isLogin:false
        };
    };
    changeStatus=()=>{
        this.setState({
            isLogin:!this.state.isLogin
        });
    };
    render(){
        return (<div>
            <HashRouter>
                <div>
                    <AuthButton isLogin={this.state.isLogin} changeStatus={this.changeStatus}/>
                </div>
                <Link to="/" replace>首页</Link><br/>
                <Link to="/Show" replace>登录</Link><br/>
    
                {
                    route.map((item,key)=>{
                        if(item.exact){
                            return <Route key={key} path={item.path} exact render={props=>(
                                    <item.component {...props} routes={item.routes}/>
                                )}
                            />
                        }else{
                            return <Route key={key} path={item.path} render={props=>(
                                    <item.component {...props} routes={item.routes} isLogin={this.state.isLogin} changeStatus={this.changeStatus}/>
                                )}
                            />
                        }
                    })
                }
            </HashRouter>
        </div>)
    }
}