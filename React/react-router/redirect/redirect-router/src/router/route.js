import Home from '@/components/home/Home';
import Show from '@/components/show/Show';
import Login from '@/components/login/Login';

let route=[
    {
        path:'/',
        component:Home,
        exact:true
    },
    {
        path:'/Show',
        component:Show,
        routes:[
            {
                path:'/Show/Login',
                component:Login
            }
        ]
    }
]

export default route;