import React from 'react';

export default class Home extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'我是Home'
        }
    };
    render(){
        return (<div>
            <div>{this.state.msg}</div>
        </div>)
    }
}