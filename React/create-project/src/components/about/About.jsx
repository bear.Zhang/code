import React from 'react';

export default class About extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:'我是About'
        };
    };
    render(){
        return(<div>
            <div>{this.state.msg}</div>
        </div>)
    }
}