import React from 'react';

import Home from '@/components/home/Home'

import AppCss from '@/App.css';

export default class App extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msg:"基础路由"
        };
    };
    render(){
        return (<div>
            <div className={AppCss.msg}>{this.state.msg}</div>
            <Home></Home>
        </div>)
    }
}