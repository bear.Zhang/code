import createStore from './replaceReducerStore';
import counterReducer from '../reducer/counterReducer';
import infoReducer from '../reducer/infoReducer';
import loggerMiddleware from '../loggerMiddleware/loggerMiddleware'
import exceptionMiddleware from '../exceptionMiddleware/exceptionMiddleware'
import timeMiddleware from '../timeMiddleware/timeMiddleware'
import applyMiddleware from '../applyMiddleware/applyMiddleware'

let initState = {
  counter: {
    count: 0
  },
  info: {
    name: '',
    description: ''
  }
}

function combineReducers(reducers) {

  /* reducerKeys = ['counter', 'info']*/
  const reducerKeys = Object.keys(reducers)

  /*返回合并后的新的reducer函数*/
  return function combination(state = {}, action) {
    /*生成的新的state*/
    const nextState = {}

    /*遍历执行所有的reducers，整合成为一个新的state*/
    for (let i = 0; i < reducerKeys.length; i++) {
      const key = reducerKeys[i]
      const reducer = reducers[key]
      /*之前的 key 的 state*/
      const previousStateForKey = state[key]
      /*执行 分 reducer，获得新的state*/
      const nextStateForKey = reducer(previousStateForKey, action)
      nextState[key] = nextStateForKey
    }
    return nextState;
  }
}

const reducer = combineReducers({
  counter: counterReducer
});

const rewriteCreateStoreFunc = applyMiddleware(exceptionMiddleware, timeMiddleware, loggerMiddleware);
const newCreateStore = rewriteCreateStoreFunc(createStore);
const store = newCreateStore(reducer, initState);

/*生成新的reducer*/
const nextReducer = combineReducers({
  counter: counterReducer,
  info: infoReducer
});
/*replaceReducer*/
store.replaceReducer(nextReducer);

store.dispatch({
    type: 'INCREMENT'
});

/*修改 name*/
store.dispatch({
  type: 'SET_NAME',
  name: '前端九部2号'
});



