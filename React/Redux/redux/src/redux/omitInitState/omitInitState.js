/*退订*/
import createStore from './omitStateCreateStore';
import reducer from '../reducer/counterReducer';
import loggerMiddleware from '../loggerMiddleware/loggerMiddleware'
import exceptionMiddleware from '../exceptionMiddleware/exceptionMiddleware'
import timeMiddleware from '../timeMiddleware/timeMiddleware'
import applyMiddleware from '../applyMiddleware/applyMiddleware'

let initState = {
    count: 0
}

const rewriteCreateStoreFunc = applyMiddleware(exceptionMiddleware, timeMiddleware, loggerMiddleware);
const store = createStore(reducer, rewriteCreateStoreFunc);

store.dispatch({
    type: 'INCREMENT'
});


