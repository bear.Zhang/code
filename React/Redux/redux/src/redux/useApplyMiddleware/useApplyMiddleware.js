/*没有中间件的 createStore*/

import createStore from './createStore';

import reducer from '../reducer/counterReducer';
import loggerMiddleware from '../loggerMiddleware/loggerMiddleware'
import exceptionMiddleware from '../exceptionMiddleware/exceptionMiddleware'
import timeMiddleware from '../timeMiddleware/timeMiddleware'
import applyMiddleware from '../applyMiddleware/applyMiddleware'

// /*接收旧的 createStore，返回新的 createStore*/
// const newCreateStore = applyMiddleware(exceptionMiddleware, timeMiddleware, loggerMiddleware)(createStore);

// /*返回了一个 dispatch 被重写过的 store*/
// const store = newCreateStore(reducer);


let initState = {
    count: 0
}

// const rewriteCreateStoreFunc = applyMiddleware(exceptionMiddleware, timeMiddleware, loggerMiddleware);
// const newCreateStore = rewriteCreateStoreFunc(createStore);
// const store = newCreateStore(reducer, initState);

const rewriteCreateStoreFunc = applyMiddleware(exceptionMiddleware, timeMiddleware, loggerMiddleware);



const createStores = (reducer, initState, rewriteCreateStoreFunc) => {
    /*如果有 rewriteCreateStoreFunc，那就采用新的 createStore */
    if(rewriteCreateStoreFunc){
       const newCreateStore =  rewriteCreateStoreFunc(createStore);
       return newCreateStore(reducer, initState);
    }else{
        console.log('dddd');
    }
}

const store = createStores(reducer, initState, rewriteCreateStoreFunc);

store.dispatch({
    type: 'INCREMENT'
});