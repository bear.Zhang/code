const timeMiddleware = (store) => (next) => (action) => {
    console.log('time', new Date(new Date().getTime()));
    next(action);
}

export default timeMiddleware;