/*退订*/
import createStore from '../unsubscribe/unsubscribe';

import reducer from '../counterReducer';

import loggerMiddleware from '../loggerMiddleware'
import exceptionMiddleware from './../exceptionMiddleware'
import timeMiddleware from '../timeMiddleware'

import applyMiddleware from './controlApplyMiddleware'

let initState = {
    count: 0
}

const rewriteCreateStoreFunc = applyMiddleware(exceptionMiddleware, timeMiddleware, loggerMiddleware);

const createStores = (reducer, initState, rewriteCreateStoreFunc) => {
    /*如果有 rewriteCreateStoreFunc，那就采用新的 createStore */
    if(rewriteCreateStoreFunc){
       const newCreateStore =  rewriteCreateStoreFunc(createStore);
       return newCreateStore(reducer, initState);
    }else{
        console.log('dddd');
    }
}

const store = createStores(reducer, initState, rewriteCreateStoreFunc);

const unsubscribe = store.subscribe(() => {
    let state = store.getState();
    console.log('dd',state.count);
});
  
  /*退订*/
unsubscribe();

store.dispatch({
    type: 'INCREMENT'
});


