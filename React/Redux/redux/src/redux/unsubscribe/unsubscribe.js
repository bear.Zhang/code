import createStore from './unsubscribeCreateStore';
import reducer from '../reducer/counterReducer';
import loggerMiddleware from '../loggerMiddleware/loggerMiddleware'
import exceptionMiddleware from '../exceptionMiddleware/exceptionMiddleware'
import timeMiddleware from '../timeMiddleware/timeMiddleware'
import applyMiddleware from '../applyMiddleware/applyMiddleware'

let initState = {
    count: 0
}

const rewriteCreateStoreFunc = applyMiddleware(exceptionMiddleware, timeMiddleware, loggerMiddleware);
const newCreateStore = rewriteCreateStoreFunc(createStore);
const store = newCreateStore(reducer, initState);



store.subscribe(() => {
    let state = store.getState();
    console.log(state.count);
});

store.subscribe(() => {
    let state = store.getState();
    console.log('ddd',state.count);
});

 /*退订*/
const unsubscribe = store.subscribe(() => {
    let state = store.getState();
    console.log(state.count);
});
unsubscribe();

store.dispatch({
    type: 'INCREMENT'
});

store.dispatch({
    type: 'INCREMENT'
});


