const createStore = function (reducer, initState) {
    let state = initState;
    let listeners = [];
  
    function subscribe(listener) {
        listeners.push(listener);
        return function unsubscribe() {
          const index = listeners.indexOf(listener);
          listeners.splice(index, 1);
        }
    }
  
    function dispatch(action) {
        state = reducer(state, action);
        for (let i = 0; i < listeners.length; i++) {
            const listener = listeners[i];
            listener();
        }
    }
  
    function getState() {
      return state;
    }
    /* 注意！！！只修改了这里，用一个不匹配任何计划的 type，来获取初始值 */
    dispatch({ type: Symbol() })

    return {
      subscribe,
      dispatch,
      getState
    }
}

export default createStore;
