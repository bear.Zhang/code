let initState = {
    count: 0
}
function counterReducer(state, action) {
    /*注意：如果 state 没有初始值，那就给他初始值！！*/ 
    if (!state) {
        state = initState;
    }
    switch (action.type) {
      case 'INCREMENT':
        return {
          count: state.count + 1
        }
      default:    
        return state;
    }
}

export default counterReducer;