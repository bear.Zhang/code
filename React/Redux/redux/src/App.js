import React from 'react';

/*bindActionCreators*/ 
// import './redux/bindActionCreators/bindActionCreators'

/*发布订阅*/ 
// import './redux/publishSubscription/publishSubscription'

/*公共发布订阅*/ 
// import './redux/common/common'

/*计数器*/ 
import './redux/counter/counter'

/*计划函数*/ 
// import './redux/plan/plan'

/*reducer 的拆分和合并*/ 
// import './redux/splitMergeReducer/splitMergeReducer'

/*state 的拆分和合并*/ 
// import './redux/splitMergeState/splitMergeState'

import './style/App.css';

function App() {
  return (<div>

  </div>);
}

export default App;
