const { override, fixBabelImports, addLessLoader , overrideDevServer} = require('customize-cra');
const rewirePostcss = require('react-app-rewire-postcss');
const pxtorem = require('postcss-pxtorem');
const autoprefixer=require('autoprefixer');

module.exports ={
    webpack:override(
        fixBabelImports('import', {
            libraryName: 'antd-mobile',
            style: true,
        }),
        addLessLoader({
            javascriptEnabled: true
        }),
        (config)=>{
            config = rewirePostcss(config,{
                plugins: () => [
                    require('postcss-flexbugs-fixes'),
                    require('postcss-preset-env')({
                        autoprefixer: {
                            flexbox: 'no-2009',
                        },
                        stage: 3,
                    }),
                    pxtorem({
                        rootValue: 100,    //以100px为准，不同方案修改这里
                        propWhiteList: [],
                    })
                ],
            });
            return config;
        }
    ),
    devServer:overrideDevServer(

    )
}