import React from 'react';

import { Button } from 'antd-mobile';

import './App.less';

function App() {
  return (
    <div className="App">
      <Button type="primary" className='App-Button'>Start</Button>
    </div>
  );
}

export default App;
