import React from 'react';

import ShoppingCart from './pages/shopingCart/shoppingCart'

function App() {
  return (
    <div>
      <ShoppingCart/>
    </div>
  );
}

export default App;
