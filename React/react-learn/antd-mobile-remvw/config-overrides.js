const { override, fixBabelImports, addLessLoader , overrideDevServer} = require('customize-cra');
const rewirePostcss = require('react-app-rewire-postcss');
const pxtorem = require('postcss-pxtorem');
const autoprefixer=require('autoprefixer');

module.exports ={
    webpack:override(
        fixBabelImports('import', {
            libraryName: 'antd-mobile',
            style: true,
        }),
        addLessLoader({
            javascriptEnabled: true
        }),
        (config)=>{
            config = rewirePostcss(config,{
                plugins: () => [
                    autoprefixer(),
                    pxtorem({
                        rootValue: 100,
                        propList: ['*']
                    })
                ],
            });
            return config;
        }
    ),
    devServer:overrideDevServer(

    )
}