import React from 'react';

import { Button } from 'antd-mobile';
import './App.less'

function App() {
  return (
    <div className="App">
      <Button type="primary">primary</Button>
    </div>
  );
}

export default App;
