// 假设main.js还是入口文件
// webpack-dev-server 打包好的main.js是托管到了内存中，所以在项目中看不到。
// 但是可以在根目录中获取到main.js

import React from 'react';
import ReactDOM from "react-dom";


const div=<div><h1>hello World</h1></div>

ReactDOM.render(div,document.getElementById('app'));