import React from 'react';
import './assets/css/App.css';

import ToDoList from './components/ToDoList'

function App() {
  return (
    <ToDoList/>
  );
}

export default App;
