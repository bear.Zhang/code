import React from 'react'

import '../assets/css/ToDoList.css'

export default class ToDoList extends React.Component{
    constructor(props){
        super(props);
        this.state={
            processingList:[],
            completedList:[]
        }
    };
    addData(e){
        e.preventDefault();
        let item={
            title:null,
            checked:false,
        }
        item.title=this.refs.ins.value;
        let tempList=this.state.processingList;
        tempList.push(item);
        this.refs.ins.value=null;
        this.setState({
            processingList:tempList
        })
    }
    changeEvent(key){
        let tempList=this.state.processingList;
        let tempList1=this.state.completedList;
        tempList.splice(key,1,{
            title:tempList[key].title,
            checked:true
        })
        tempList1.push(tempList[key]);
        tempList.splice(key,1);
        this.setState({
            processingList:tempList,
            completedList:tempList1
        })
    };
    changeEvent1(key){
        let tempList1=this.state.processingList;
        let tempList=this.state.completedList;
        tempList.splice(key,1,{
            title:tempList[key].title,
            checked:false
        })
        tempList1.push(tempList[key]);
        tempList.splice(key,1);
        this.setState({
            processingList:tempList1,
            completedList:tempList
        })
    };
    itemChange(e,key){
        let tempList=this.state.processingList;
        let item=e.target.value;
        tempList.splice(key,1,{
            title:item,
            checked:tempList[key].checked
        })
        this.setState({
            processingList:tempList,
        })
        
    };
    itemChange1(e,key){
        let tempList=this.state.completedList;
        let item=e.target.value;
        tempList.splice(key,1,{
            title:item,
            checked:tempList[key].checked
        })
        this.setState({
            completedList:tempList,
        })
        
    };
    del(key){
        let tempList=this.state.processingList;
        tempList.splice(key,1)
        this.setState({
           processingList:tempList,
        })
    };
    del1(key){
        let tempList=this.state.completedList;
        tempList.splice(key,1)
        this.setState({
            completedList:tempList,
        })
    }
    render(){
        return(
            <div>
                <header className="title">
                    <section>
                        <form>
                            <label htmlFor="ins" className="label">ToDoList</label>
                            <input id="ins" ref="ins" />
                            <button onClick={(e)=>this.addData(e)}>添加</button>
                        </form>
                    </section>
                </header>
                <section>
                    <h2>处理中{this.state.processingList.length}</h2>
                    <ol>
                        {
                            this.state.processingList.map((item,key)=>{
                                return(
                                    <li key={key}>
                                       <input type="checkbox" checked={item.checked} onChange={()=>this.changeEvent(key)}/>
                                       <input value={item.title} onChange={(e)=>{this.itemChange(e,key)}}/>
                                       <button onClick={()=>this.del(key)}>删除</button>
                                    </li>
                                )
                            })
                        }
                    </ol>
                    <h2>已完成{this.state.completedList.length}</h2>
                    <ul>
                        {
                            this.state.completedList.map((item,key)=>{
                                return(
                                    <li key={key}>
                                       <input type="checkbox" checked={item.checked} onChange={()=>this.changeEvent1(key)}/>
                                       <input value={item.title} onChange={(e)=>{this.itemChange1(e,key)}}/>
                                       <button onClick={()=>this.del1(key)}>删除</button>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </section>
            </div>
        )
    }
}