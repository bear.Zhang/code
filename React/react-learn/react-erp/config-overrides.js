const { override,overrideDevServer, fixBabelImports, addLessLoader,addWebpackAlias} = require('customize-cra');
const path = require('path')
const resolve = dir => path.join(__dirname, '.', dir);

module.exports={ 
    webpack:override(
        fixBabelImports('import', {
            libraryName: 'antd',
            libraryDirectory: 'es',
            style: true
        }),
        addLessLoader({
            javascriptEnabled: true,
            modifyVars: { '@primary-color': '#1DA57A' },
        }),
        addWebpackAlias({
            ['@']: resolve('src')
        }),
    ),
};
