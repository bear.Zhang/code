import { combineReducers } from 'redux'
import counter from './reCounter'
import reUserInfo from './reUserInfo'

const combineReducer = combineReducers({
  counter,
  reUserInfo
});

export default combineReducer;