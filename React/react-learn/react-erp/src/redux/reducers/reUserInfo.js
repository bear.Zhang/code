import {GET_TOKEN,GET_USERINFO} from '../actions/acUserInfo';
import auth from '@/utils/auth';
import { getToken,getInfo} from '@/api/login/login'

const initState = {
    isLoading: false,
    errorMsg: ''
};
export default (state=initState,action)=>{
    switch(action.type){
        case GET_TOKEN:
            return new Promise((resolve,reject)=>{
                getToken(action.data).then((res)=>{
                    const data=res.access_token;
                    auth.token=data;
                    resolve(data);
                }).catch(err=>{
                    reject(err);
                })
            })
        case GET_USERINFO:
            return new Promise((resolve,reject)=>{
                getInfo(action.data).then((res)=>{
                    const data=res.data;
                    auth.user=data;
                    resolve(data);
                }).catch(err=>{
                    reject(err);
                })
            })
        default:
            return state
        
    }

}