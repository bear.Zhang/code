import {INCREMENT,DECREMENT} from '../actions/acCounter';

const initState={
    count:0
}

export default (state=initState.count,action)=>{
    switch(action.type){
        case INCREMENT:
            return state+1
        case DECREMENT:
            return state-1
        default:
            return state
        
    }

}