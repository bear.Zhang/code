import {createStore} from 'redux'
import combineReducer from './reducers/reducers'

const store=createStore(combineReducer,window.STATE_FROM_SERVER);

export default store;