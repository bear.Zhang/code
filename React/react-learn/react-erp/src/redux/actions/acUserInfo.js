export const GET_TOKEN='GET_TOKEN';
export const GET_USERINFO='GET_USERINFO';

export function getToken(data){
    return{
        type:GET_TOKEN,
        data:data
    }
}

export function getUserInfo(data) {
    return {
        type:GET_USERINFO,
        data:data
    }
}