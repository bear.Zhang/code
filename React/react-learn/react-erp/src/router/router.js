import Login from '@/pages/login/Login';
import LayoutPage from '@/components/layout/LayoutPage';
import Error from '@/pages/error/Error';
import HomeRouter from './modules/home';
import GoodsRouter from './modules/goods';

const router=[
    {
        path:'/Index',
        name:'Index',
        component:LayoutPage,
        routes:[
            ...HomeRouter,
            ...GoodsRouter,
            { path: '/Index', exact: true, redirect: '/Index/Home' },
        ]
    },
    {
        path:'/Login',
        name:'Login',
        component:Login,
    },
    {
        path:'*',
        name:'404',
        component:Error,
    },
    
]

export default router;