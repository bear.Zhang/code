import Goods from '@/pages/goods/Goods';
import ProjectEdit from '@/pages/goods/ProjectEdit';

export default [
    { 
        path: '/Index/Goods', 
        title:'商品',
        component: Goods,
        routes:[
        ]
    },
    {
        path: '/Index/ProjectEdit', 
        title:'编辑',
        component: ProjectEdit,
    }
]