import request from '@/utils/request'
import api from '../api'
export function getToken(data) {
  return request({
    url: api + '/user-service/oauth/token',
    method: 'post',
    headers: {
      'content-type': 'application/x-www-form-urlencoded',
      'mac': data.mac
    },
    data: data.user,
    unloading:true
  })
}
export function getInfo() {
  return request({
    url: api + '/user-service/user/getUserInfo',
    method: 'get',
    unloading:true
  })
}