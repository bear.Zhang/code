import request from '@/utils/request'
import api from '../api'

export function getStatus() {
    return request({
      url:'/api/customer-service/project/status-count',
      method:'get',
    })
}

export function getProjectList(data) {
  return request({
    url:api+`/customer-service/project/available-list`,
    method:'post',
    data:data
  })
}