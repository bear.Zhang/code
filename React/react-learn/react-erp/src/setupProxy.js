const proxy = require('http-proxy-middleware')

module.exports = function(app) {
  app.use(
    proxy('/api', {
      target: 'https://lxerptest.66123123.com/',
      changeOrigin: true
    })
  )
}