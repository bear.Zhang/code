import React from 'react';
import { Table} from 'antd';
import { Button } from 'antd';
import {getProjectList,getStatus} from '@/api/goods/goods'
import './goods.less'

export default class Goods extends React.Component{
    constructor(props){
        super(props);
        this.state={
            columns:[
                {
                  title: '项目ID',
                  width: 100,
                  dataIndex: 'no',
                  key: 'no',
                },
                {
                  title: '项目名称',
                  width: 100,
                  dataIndex: 'name',
                  key: 'name',
                },
                {
                  title: '项目类型',
                  dataIndex: 'type',
                  key: 'type',
                  width: 150,
                },
                {
                  title: '项目方联系人姓名',
                  dataIndex: 'partyAContactName',
                  key: 'partyAContactName',
                  width: 150,
                },
                {
                  title: 'Column 3',
                  dataIndex: 'partyBName',
                  key: 'partyBName',
                  width: 150,
                },
                {
                  title: 'Column 4',
                  dataIndex: 'partyBPrincipalName',
                  key: 'partyBPrincipalName',
                  width: 150,
                },
                {
                  title: 'Column 5',
                  dataIndex: 'userDataAuthorityName',
                  key: 'userDataAuthorityName',
                  width: 150,
                },
                {
                  title: 'Column 6',
                  dataIndex: 'statusName',
                  key: 'statusName',
                  width: 150,
                },
                {
                  title: '操作',
                  key: 'operation',
                  fixed: 'right',
                  width: 100,
                  render: (text,record) => {
                    return(<div>
                        <Button type="primary" onClick={()=>this.goEdit(record.id)}>编辑</Button>
                    </div>)
                  },
                },
            ],
            data:[],
            statusData:{}
        }
        this.goEdit=this.goEdit.bind(this);
        this.getStatus=this.getStatus.bind(this);
        this.goEdit=this.goEdit.bind(this);
    };
    componentWillMount(){
      this.getProjectList();
      this.getStatus();
    };
    getProjectList(){
      getProjectList({}).then((res)=>{
        if(res.code==='000000'){
            res.data.items.forEach((item,index)=>{
                item.key=index;
            })
            this.setState({
                data:res.data.items
            })
        }
      })
    };
    getStatus(){
      getStatus().then((res)=>{
        if(res.code==='000000'){
          this.setState({
            statusData:res.data
          });
        }
      })
    };
    goStatus(){
      
    };
    goEdit(id){
      console.log(this.props);
      this.props.history.push('/Index/ProjectEdit',{id});
    };
    render(){
        return(<div>
            <div className="title-btn">
              {
                Object.keys(this.state.statusData).map((item)=>{
                  return <Button key={item} onClick={}>{this.state.statusData[item].statusName}</Button>
                })
              }
            </div>
            <Table columns={this.state.columns} dataSource={this.state.data} scroll={{ x: 1500, y: 600 }} />
        </div>)
    };
}
