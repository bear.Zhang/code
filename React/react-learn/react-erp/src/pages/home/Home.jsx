import React from 'react';
import {connect} from 'react-redux'

class Home extends React.Component{
    constructor(props){
        super(props);
        this.state={
            name:'首页的'
        }
    }
    increment(){
        this.props.onAdd();
    }
    decrement(){
        this.props.onCut();
    }
    render(){
        return(<div>
        {this.state.name}:{this.props.count}
        <p>
                Clicked: {this.props.count} times
                {' '}
                <button onClick={this.increment.bind(this)}>+</button>
                {' '}
                <button onClick={this.decrement.bind(this)}>-</button>
            </p>
    </div>)
    }
}

//将state绑定到props的counter
const mapStateToProps = (state)=> {
    return {
        count: state.counter
    }
};

//将action的所有方法绑定到props上
const mapDispatchToProps = (dispatch) => {
    return {
        onAdd: ()=> {
            dispatch({type: "INCREMENT",data:1});
        },
        onCut: ()=> {
            dispatch({type: "DECREMENT"});
        }
    };
};

//通过react-redux提供的connect方法将我们需要的state中的数据和actions中的方法绑定到props上
export default connect(mapStateToProps, mapDispatchToProps)(Home)

