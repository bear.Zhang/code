import React from 'react';
import Qs from 'qs';
import {connect} from 'react-redux'
import './login.less'
import { Input } from 'antd';
import { Button } from 'antd';

class Login extends React.Component{
    constructor(props){
        super(props);
        this.state={
            mac:'NIBUSHIZHENZHENGDEKUAILE',
            user:null,
            password:null,
        };
        this.login=this.login.bind(this);
    };
    componentWillMount(){
        React.$auth.token=null;
        React.$auth.user=null;
    };
    render(){
        return(
            <div className="login-page">
                <div className="login-wrap">
                    <div className="login-init">
                        <img src={[require("../../assets/img/logo.png")]} alt="平台图"/>
                        <p>领先未来办公平台</p>
                    </div>
                    <div className="login-info">
                        <ul>
                            <li>
                                <Input size="large" name="user" onChange={event=>this.changeValue(event)} placeholder="请输入账号" />
                            </li>
                            <li>
                                <Input.Password size="large" name="password" onChange={event=>this.changeValue(event)}  placeholder="请输入密码" />
                            </li>
                            <li>
                                <Button type="link">忘记密码</Button>
                                <Button type="link">立即注册</Button>
                            </li>
                            <li>
                                <Button size="large" type="primary" onClick={this.login}>登录</Button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    };
    changeValue(e){
        if(e&&e.target&&e.target.value){
            let valueName=e.target.name;
            let value=e.target.value;
            this.setState({
                [valueName]:value
            })
        }
    };
    async login(){
        let info=Qs.stringify({
            grant_type: "password",
            username: this.state.user,
            password: this.state.password
        });
        let loginInfo={
            user:info,
            mac:this.state.mac
        };
        if(this.state.mac){
            await this.props.getToken(loginInfo);
            this.props.token.then((token)=>{
                this.props.getUserInfo(token);
                this.props.userInfo.then(()=>{
                    React.$authorities=React.$auth.user.authorities;
                    this.props.history.push('/Index');
                })

            })
        }
    };
}

// 将所有的reducer的返回值放到props上
const mapStateToProps = (state)=> {
    // console.log(state);
    return { 
        token:state.reUserInfo,
        userInfo:state.reUserInfo
    }
};

//将action的所有方法绑定到props上
const mapDispatchToProps = (dispatch) => {
    return {
        getToken: (loginInfo)=> {
            dispatch({type: "GET_TOKEN",data:loginInfo});
        },
        getUserInfo: (token)=> {
            dispatch({type: "GET_USERINFO",data:token});
        }
    };
};

//通过react-redux提供的connect方法将我们需要的state中的数据和actions中的方法绑定到props上
export default connect(mapStateToProps, mapDispatchToProps)(Login)

