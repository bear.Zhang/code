import React from 'react';
import {Link} from 'react-router-dom'
import { Layout, Menu} from 'antd';

import router from '../../router/router'

const { Header} = Layout;

export default class Home extends React.Component{
    render(){
        return(
            <Header className="header">
                <div className="logo"></div>
                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={['1']}
                    style={{ lineHeight: '64px' }}>
                    {
                        router[0].routes.map((item,index)=>{
                        return <Menu.Item key={index}><Link to={item.path} replace>{item.title}</Link></Menu.Item>
                        })
                    }
                </Menu>
            </Header>
        )
    }
}
