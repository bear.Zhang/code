import React from 'react';
import { Layout} from 'antd';
import './layout.less'
import Header from '../header/Header'
import LetNav from '../letNav/LetNav'
import Breadcrumb from '../breadcrumb/Breadcrumb'
import ContentPage from '../content/Content'

export default class LayoutPage extends React.Component{
    render(){
        return(
            <Layout >
                <Header></Header>
                <Layout>
                    <LetNav></LetNav>
                    <Layout style={{ padding: '0 24px 24px' }}>
                        <Breadcrumb></Breadcrumb>
                        <ContentPage></ContentPage>
                    </Layout>
                </Layout>
            </Layout>
        )
    }
}
