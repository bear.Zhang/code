import React from 'react';
import {BrowserRouter} from 'react-router-dom'
import './App.css';
// import LayoutPage from '../Layout/LayoutPage.jsx'
// import Login from '../../pages/login/Login'
import auth from '@/utils/auth';
import RouterView from '@/router/RouterView'
import router from '@/router/router'

React.$auth=auth;

function App() {
  return (<BrowserRouter>
    <div className="App">
      <RouterView routes={router}/>
    </div>
  </BrowserRouter>);
}

export default App;
