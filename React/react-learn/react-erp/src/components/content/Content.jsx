import React from 'react';
import { Layout} from 'antd';
import RouterView from '@/router/RouterView'
import router from '@/router/router'

const { Content} = Layout;
export default class ContentPage extends React.Component{
    render(){
        return(
            <Content
                style={{
                    background: '#fff',
                    padding: 24,
                    margin: 0,
                    minHeight: 280,
                }}>
                <RouterView routes={router[0].routes}/>
            </Content>
        )
    }
}
