// 假设main.js还是入口文件
// webpack-dev-server 打包好的main.js是托管到了内存中，所以在项目中看不到。
// 但是可以在根目录中获取到main.js

import React from 'react';
import ReactDOM from "react-dom";

// const myH1=React.createElement('h1',null,'hello World!');

/*
// 单个DOM元素
const myH1=React.createElement('h1',{
    id:'myH1',
    title:'我的h1'
},'hello World!');

ReactDOM.render(myH1,document.getElementById('app'));
*/


// DOM嵌套
const myH1=React.createElement('h1',{
    id:'myH1',
    title:'我的h1'
},'hello World!');

const myDiv=React.createElement('div',null,'DIV',myH1);

ReactDOM.render(myDiv,document.getElementById('app'));