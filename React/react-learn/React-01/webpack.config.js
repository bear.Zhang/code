const path=require('path');
// 导入在内存中自动生成index.html的插件
const htmlWebpackPlugin=require('html-webpack-plugin');

const htmlPlugin=new htmlWebpackPlugin({
    // 源文件
    template:path.join(__dirname,'./src/index.html'),
    // 生在在内存中的首页的名称
    filename:'index.html'
});

// 向外暴露一个打包的配置对象，webpack 是基于Node构建的，所以webpack支持所有的 Node API和语法 
module.exports={
    mode:'development',//development、production
    // 在webpack 4.x 中有一个特性，即约定大于配置，约定默认的打包入口文件路径是src下的index.js(src/index.js。
    plugins:[
        htmlPlugin
    ]
}
