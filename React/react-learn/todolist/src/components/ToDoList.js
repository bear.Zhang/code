import React from 'react'

class ToDoList extends React.Component{
    constructor(props){
        super(props);
        this.state={
            list:[]
        };
    };
    addData(){
        let tempList=this.state.list;
        tempList.push(this.refs.in.value);
        this.setState({
            list:tempList
        })
    };
    del(key){
        let tempList=this.state.list;
        tempList.splice(key,1);
        this.setState({
            list:tempList
        })
    }
    render(){
        return (
            <div>
                <h2>React ToDoList</h2>
                <input ref="in"/> <button onClick={()=>this.addData()}>添加</button>
                <ul>
                    {
                        this.state.list.map((item,key)=>{
                            return <li key={key}>{item}<button onClick={()=>this.del(key)}>删除</button></li>
                        })
                    }
                </ul>
            </div>
        );
    }
}

export default ToDoList;