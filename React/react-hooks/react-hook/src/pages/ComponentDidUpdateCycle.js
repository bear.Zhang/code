import React, { useState, useEffect } from 'react'
import {message} from 'antd'

/**
 * 模拟实现  componentDidUpdate
 */

export default function ComponentDidUpdateCycle(){
    const [count,changCount]=useState(0);
    useEffect(()=>{
        message.info(`我在每次页面更新时打印！`);
    });
    return(
        <div>
            {count}
            <button onClick={()=>changCount(Math.ceil(Math.random()*1000))}>改变count</button>
        </div>
    )
} 