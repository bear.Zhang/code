import React, { useState, useEffect } from 'react'
import {message} from 'antd'
/**
 * useEffect模拟实现componentDidMount和componentWillUnmount
 */
function Child({visible}){
    useEffect(()=>{
        message.info(`我只在页面挂载时打印！`);
        return ()=>{
            message.info(`我只在页面卸载时打印！`);
        }
    },[]);
    return visible ? 'true' : 'false';
} 

export default function SimulationCycle(){
    const [visible, changeVisible]=useState(true);
    return(
        <div>
            {
                visible && <Child visible={visible} />
            }
            <button onClick={() => { changeVisible(!visible); }}>
                改变visible
            </button>
        </div>
    )
} 