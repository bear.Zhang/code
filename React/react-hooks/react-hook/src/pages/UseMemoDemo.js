import React, { useState, useMemo } from 'react';
import { message } from 'antd';

function Child({ count }) {
  return <p>当前传递的count为:{count}</p>;
}

export default function UseMemoDemo() {
  const [count1, changeCount1] = useState(0);
  const [count2, changeCount2] = useState(10);

  const child = useMemo(() => {
    message.info('重新生成Child组件');
    return <Child count={count1} />;
  }, [count1]);
  return (
    <div>
      {child}
      <button onClick={() => { changeCount1(count1 + 1); }}>改变count1</button>
      <button onClick={() => { changeCount2(count2 + 1); }}>改变count2</button>
    </div>
  );
}