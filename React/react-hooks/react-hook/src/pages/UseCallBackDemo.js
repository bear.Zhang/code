import React, { useState, useEffect ,useCallback} from 'react'
import {message} from 'antd'

/**
 * useCallback模拟实现  componentDidUpdate
 */

export default function UseCallBackDome(){
    const [count1,changeCount1]=useState(0);
    const [count2, changeCount2] = useState(10);

    const calculateCount = useCallback(() => {
        if (count1 && count2) {
          return count1 * count2;
        }
        return count1 + count2;
      }, [count1, count2])
    useEffect(()=>{
        const result = calculateCount(count1, count2);
        message.info(`执行副作用，最新值为${result}！`);
    },[calculateCount]);
    return(
        <div>
            {count1}
            <button onClick={()=>changeCount1(Math.ceil(Math.random()*1000))}>改变count1</button>
            <button onClick={()=>changeCount2(Math.ceil(Math.random()*1000))}>改变count2</button>
        </div>
    )
} 