import React, { useState, useMemo} from 'react'
import {message} from 'antd'

/**
 * useRef
 */

export default function UseMemoDome(){
    const [count1,changeCount1]=useState(0);
    const [count2, changeCount2] = useState(10);
    const calculateCount = useMemo(() => {
        message.info('重新生成计算结果');
        return count1 * 10;
    }, [count1]);
    return(
        <div>
            {calculateCount}
            <button onClick={() => { changeCount1(count1 + 1); }}>改变count1</button>
            <button onClick={() => { changeCount2(count2 + 1); }}>改变count2</button>
        </div>
    )
} 