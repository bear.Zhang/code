import React, { useState, useEffect } from 'react'
import {message} from 'antd'

export default function HookDemo(){
    /**
     * useState：用于创建一个新的状态，参数为一个固定的值或者一个有返回值的方法。钩子执行后的结果为一个数组，分别为生成的状态以及改变该状态的方法
     */
    const [count,changCount]=useState(0);
    /**
     * useEffect：第一个参数为副作用需要执行的回调，生成的回调方法可以返回一个函数（将在组件卸载时运行）；第二个为该副作用监听的状态数组，当对应状态发生变动时会执行副作用，如果第二个参数为空，那么在每一个 State 变化时都会执行该副作用。 
     */
    useEffect(()=>{
        message.info(`count发生变动，最新的值${count}`);
    },[count]);
    return(
        <div>
            {count}
            <button onClick={()=>changCount(Math.ceil(Math.random()*1000))}>改变count</button>
        </div>
    )
} 