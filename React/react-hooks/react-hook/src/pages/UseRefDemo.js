import React, { useState, useEffect ,useRef} from 'react'
import {message} from 'antd'

/**
 * useRef
 */

export default function UseCallBackDome(){
    const [count1,changeCount1]=useState(0);
    const [count2, changeCount2] = useState(0);
    // 创建初始值为空对象的prestate
    const preState = useRef({});
    // 依赖preState进行判断时可以先判断，最后保存最新的state数据
    useEffect(() => {
        const  current= preState.current;
        if (current) {
            console.log(current);
        }
        // 保存最新的state
        preState.current = {
            count1,
            count2,
        }
    });
    return(
        <div>
            {count1}
            <button onClick={()=>changeCount1(Math.ceil(Math.random()*1000))}>改变count1</button>
            <button onClick={()=>changeCount2(Math.ceil(Math.random()*1000))}>改变count2</button>
        </div>
    )
} 