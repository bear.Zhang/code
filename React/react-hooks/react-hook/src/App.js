import React from 'react';
// import HookDemo from './pages/HookDemo'
// import SimulationCycle from './pages/SimulationCycle'
// import ComponentDidUpdateCycle from './pages/ComponentDidUpdateCycle'
// import UseCallBackDemo from './pages/UseCallBackDemo'
// import UseRefDemo from './pages/UseRefDemo'
// import UseMemoDome from './pages/UseMemoDome'
import UseMemoDemo from './pages/UseMemoDemo'



function App() {
  return (
    <div>
      {/* <HookDemo/>   */}
      {/* <SimulationCycle/>    */}
      {/* <ComponentDidUpdateCycle/> */}
      {/* <UseCallBackDemo/> */}
      {/* <UseRefDemo/> */}
      {/* <UseMemoDome/> */}
      <UseMemoDemo/>
    </div>
  );
}

export default App;
