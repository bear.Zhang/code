import React ,{useState} from 'react';

import KeepAlive, { AliveScope } from 'react-activation'

function Test() {
  const [count, setCount] = useState(0);
  
  return (
    <div>
      count: {count}
      <button onClick={() => setCount(count => count + 1)}>add</button>
    </div>
  )
}

function App() {
  const [show, setShow] = useState(true);

  return (
    <AliveScope>
      <button onClick={() => setShow(show => !show)}>Toggle</button>
      <div>无 KeepAlive 功能</div>
      {show && <Test />}
      <div>有 KeepAlive 功能</div>
      {show && (
        <KeepAlive>
          <Test />
        </KeepAlive>
      )}
    </AliveScope>
  )
}

export default App;
