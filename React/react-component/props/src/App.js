import React from 'react';

import Parent from './components/Parent'

function App() {
  return (
    <div className="app">
      <Parent/>
    </div>
  );
}

export default App;
