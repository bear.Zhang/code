import React,{Component} from 'react'

class Child extends Component{
    constructor(props){
        super(props);
        this.state={
            msg:'子组件'
        }
    }
    render(){
        return(<div>
            <h4>{this.state.msg}</h4>
            <p>父组件传参：{this.props.p1}--{this.props.p2}--{this.props.p3}</p>
        </div>)
    }
}

export default Child;