import React,{Component} from 'react'

class CallbackChild extends Component{
    constructor(props){
        super(props);
        this.state={
            msg:'子组件',
            count:0
        }
        this.callbackParent=this.callbackParent.bind(this);
        this.addCount=this.addCount.bind(this);
        this.reduceCount=this.reduceCount.bind(this);
    }
    addCount(){
        this.callbackParent(true)
    }
    reduceCount(){
        this.callbackParent(false);
    }
    callbackParent(type){
        let prevVal=this.state.count;
        let newVal=type?prevVal+1:prevVal-1;
        this.setState({
            count:newVal
        })
        this.props.parentFunc(newVal,prevVal);
    }
    render(){
        return(<div>
            <h4>{this.state.msg}</h4>
            <button onClick={this.addCount}>+</button>
            <span>{this.state.count}</span>
            <button onClick={this.reduceCount}>-</button>
        </div>)
    }
}

export default CallbackChild;