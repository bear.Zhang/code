import React,{Component} from 'react'

import Child from './Child';
import CallbackChild from './CallbackChild';

class Parent extends Component{
    constructor(props){
        super(props);
        this.parentFunc=this.parentFunc.bind(this);
        this.state={
            msg:'父组件',
            parameter:{
                p1:'父参数',
                p2:18,
                p3:false
            },
            changeVal:0
        }
    }
    parentFunc(newVal,prevVal){
        console.log(newVal,prevVal);
        this.setState({
            changeVal:newVal
        })
    }
    render(){
        return(<div>
            <h3>{this.state.msg}</h3>
            <p>{this.state.changeVal}</p>
            {/* 传递基本类型参数 */}
            {/* <Child p1={this.state.parameter.p1} p2={this.state.parameter.p2} p3={this.state.parameter.p3}/> */}
            {/* 使用spread优化传值 */}
            <Child {...this.state.parameter} />
            {/* 传递引用类型参数 */}
            {/* <Child parameter={this.state.parameter} /> */}

            {/* 回调函数 */}
            <CallbackChild parentFunc={this.parentFunc}/>
        </div>)
    }
}

export default Parent;