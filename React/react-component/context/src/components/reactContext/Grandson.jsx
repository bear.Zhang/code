import React,{Component} from 'react'

import {ContextData} from './ContextData';

class Grandson extends Component{
    constructor(props){
        super(props);
        this.state={
            msg:'孙组件'
        }
        this.addCount=this.addCount.bind(this);
        this.reduceCount=this.reduceCount.bind(this);
    }
    componentDidMount(){
        console.log(this.context);
    }
    addCount(){
        this.setContext(true)
    }
    reduceCount(){
        this.setContext(false);
    }
    setContext(type){
        let prevVal=this.context.contextVal;
        let newVal=type?prevVal+1:prevVal-1;
        this.context.changeContext(newVal);
    }
    render(){
        return(<ContextData.Consumer>
            {(value,changeContext)=>(
                <div>
                    <h4>{this.state.msg}</h4>
                    <button onClick={this.addCount}>+</button>
                    <span>{value.contextVal}</span>
                    <button onClick={this.reduceCount}>-</button>
                </div>
            )}
        </ContextData.Consumer>)
    }
}

export default Grandson;