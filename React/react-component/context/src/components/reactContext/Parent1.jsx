import React,{Component} from 'react'

import Child1 from './Child1';
import Child2 from './Child2';

import {ContextData} from './ContextData';

class Parent1 extends Component{
    constructor(props){
        super(props);
        this.changeContext=(val)=>{
            this.setState(state=>({
                contextValue:{
                    contextVal:val,
                    changeContext:this.changeContext
                }
            }));
        }
        this.state={
            msg:'React.createContext父组件',
            contextValue:{
                contextVal:1,
                changeContext:this.changeContext
            }
        };
        
    }
    render(){
        return(<div>
            <h3>{this.state.msg}</h3>
            <p>{this.state.contextValue.contextVal}</p>
            <ContextData.Provider value={this.state.contextValue}>
                <Child1/>
                <Child2/>
            </ContextData.Provider>
        </div>)
    }
}

export default Parent1;