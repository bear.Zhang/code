import React,{Component} from './node_modules/react'

import PropTypes from './node_modules/prop-types';

class Child2 extends Component{
    constructor(props){
        super(props);
        this.state={
            msg:'子组件2'
        }
        this.addCount=this.addCount.bind(this);
        this.reduceCount=this.reduceCount.bind(this);
    }
    static contextTypes={
        contextVal:PropTypes.number
    }
    static contextTypes={
        contextVal:PropTypes.number,
        changeContext:PropTypes.func
    }
    addCount(){
        this.setContext(true)
    }
    reduceCount(){
        this.setContext(false);
    }
    setContext(type){
        let prevVal=this.context.contextVal;
        let newVal=type?prevVal+1:prevVal-1;
        this.context.changeContext(newVal);
    }
    render(){
        return(<div>
            <h4>{this.state.msg}</h4>
            <button onClick={this.addCount}>+</button>
            <span>{this.context.contextVal}</span>
            <button onClick={this.reduceCount}>-</button>
        </div>)
    }
}

export default Child2;