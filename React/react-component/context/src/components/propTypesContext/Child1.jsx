import React,{Component} from 'react'
import PropTypes from 'prop-types';

import Grandson from './Grandson'

class Child1 extends Component{
    constructor(props){
        super(props);
        this.state={
            msg:'子组件1'
        }
        this.addCount=this.addCount.bind(this);
        this.reduceCount=this.reduceCount.bind(this);
    }
    static contextTypes={
        contextVal:PropTypes.number,
        changeContext:PropTypes.func
    }
    addCount(){
        this.setContext(true)
    }
    reduceCount(){
        this.setContext(false);
    }
    setContext(type){
        let prevVal=this.context.contextVal;
        let newVal=type?prevVal+1:prevVal-1;
        this.context.changeContext(newVal);
    }
    render(){
        return(<div>
            <h4>{this.state.msg}</h4>
            <button onClick={this.addCount}>+</button>
            <span>{this.context.contextVal}</span>
            <button onClick={this.reduceCount}>-</button>
            <Grandson/>
        </div>)
    }
}

export default Child1;