import React,{Component} from 'react'
import PropTypes from 'prop-types';

import Child1 from './Child1';
import Child2 from './Child2';

class Parent extends Component{
    constructor(props){
        super(props);
        this.state={
            msg:'父组件',
            contextVal:0
        };
    }
    static childContextTypes={
        contextVal:PropTypes.number,
        changeContext:PropTypes.func
    }
    getChildContext(){
        return{ 
            contextVal:this.state.contextVal,
            changeContext:this.changeContext
        }
    }
    changeContext=(contextVal)=>{
        this.setState({contextVal});
    }
    render(){
        return(<div>
            <h3>{this.state.msg}</h3>
            <p>{this.state.contextVal}</p>
            <Child1/>
            <Child2/>
        </div>)
    }
}

export default Parent;