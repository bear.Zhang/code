import React from 'react';

import Parent from './components/propTypesContext/Parent'
import Parent1 from './components/reactContext/Parent1'

function App() {
  return (
    <div>
      <Parent/>
      <Parent1/>
    </div>
  );
}

export default App;
