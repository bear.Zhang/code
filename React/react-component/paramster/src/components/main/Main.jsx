import React from 'react';

export default class Main extends React.Component{
  constructor(props){
    super(props);
    this.state={
      msg:'Main'
    }
  };
  render(){
    return (<div>
        {this.state.msg}
        {this.props.children}
    </div>)
  }
}
