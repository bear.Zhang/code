import React from 'react';

export default class Child extends React.Component{
  constructor(props){
    super(props);
    this.state={
      msg:'子组件数据'
    }
  };
  child(){
      alert("我是子组件")
  }
  render(){
    return (<div>
        {this.state.msg}
        {this.props.children}
        {this.props.params}
        <div>
            <button onClick={this.props.father}>调用父级的方法</button>
        </div>
        <div>
            <button onClick={this.props.newFather.father}>整个父级实例</button>
            {
                this.props.newFather.state.msg
            }
        </div>

        <div>
            <button onClick={this.props.newFather.father.bind(this,this.state.msg)}>整个父级实例</button>   
        </div>
    </div>)
  }
}
