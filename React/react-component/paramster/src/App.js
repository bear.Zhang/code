import React from 'react';

import Main from './components/main/Main';
import Child from './components/child/Child';

export default class App extends React.Component{
  constructor(props){
    super(props);
    this.state={
      msg:'App',
      params:'给子组件传值'
    }
  };
  father(val){
    console.log('我是父组件的方法');
    console.log(val);
  };
  getChild=()=>{
    console.log(this.refs.childRef.state.msg);
    this.refs.childRef.child();
  }
  render(){
    return (<div>
      <Main>APP来的</Main>
      <Child ref="childRef" params={this.state.params} father={this.father} newFather={this}>App来的</Child>
      <button onClick={this.getChild}>获取子组件实例</button>
    </div>)
  }
}
