/**
 * add(2,3,4)=9
 * add(2)(3)(4)=9
 * add(2,3)(4)=9
 * add(2)(3,4)=9
 */
function add(...rest){
    let res=rest.reduce((act,cur)=>{
        return act+cur
    },0);
    console.log(res);
    return function(y){
        return res+y
    }
}

add(2,3,4)(2);