// 深度优先遍历
let tree = [{
    id: '1',
    title: '节点1',
    children: [{
            id: '1-1',
            title: '节点1-1'
        },
        {
            id: '1-2',
            title: '节点1-2'
        }
    ]
},
{
    id: '2',
    title: '节点2',
    children: [{
        id: '2-1',
        title: '节点2-1'
    }]
}
]

// 递归实现-先序遍历
function ftreeForeach(tree,func){
    tree.forEach(data=>{
        func(data);
        data.children&&ftreeForeach(data.children,func);
    })
}

// 递归实现-后序遍历
function ltreeForeach(tree,func){
    tree.forEach(data=>{
        data.children&&ltreeForeach(data.children,func);
        func(data);
    })
}

ftreeForeach(tree,node=>{console.log(node.title)});
ltreeForeach(tree,node=>{console.log(node.title)});