/*
在一个二维数组中（每个一维数组的长度相同），每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。请完成一个函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
*/

function find(arr,n){
    let i=arr.length-1;
    let j=0;
    return compare(arr,i,j,n);
}
function compare(arr,i,j,n){
    if(i<0||arr[i][j]==undefined){
        return false;
    }else if(arr[i][j]==n){
        return true;
    }else if(arr[i][j]>n){
        i--;
        return compare(arr,i,j,n);
    }else if(arr[i][j]<n){
        j++;
        return compare(arr,i,j,n);
    }
}

var arr=[
    [4,5,6],
    [8,9,10]
];

console.log(find(arr,1));//false
console.log(find(arr,11));//true