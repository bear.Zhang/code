// 反转整数
function Stack(){
    this.items=[];
    this.push=function(el){
        this.items.push(el);
    },
    this.pop=function(){
        return this.items.pop();
    },
    this.peek=function(){
        return this.items[this.items.length-1];
    },
    this.size=function(){
        return this.items.length;
    }
}

var number=123;
function reverseInteger(number){
    var stack =new Stack(),
        str='';
    for(i of String(number)){
        stack.push(i);
    }
    var length=stack.size();
    for(let i=0;i<length;i++){
        str+=stack.pop();
    }
    return Number(str);
}


console.log(reverseInteger(number));
