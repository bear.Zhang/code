// 计算出n阶乘中尾部零的个数

function trailingZeros1(n) {
    function factorial(n){
        return n>1 ? n*factorial(n-1):1;
    }
    var sum=factorial(n);
    var i=0;
    String(sum).split('').reverse().forEach((item)=>{
        if(item==0){
            i++;
        }
    });
    return i;
}
console.log(trailingZeros1(5));

function trailingZeros2(n){
    let count=0;
    let pwr=25;
    for(let temp=5;temp<=n;temp+=5){
        count++;
        pwr=25;
        while(temp%pwr==0){
            count++;
            pwr*=5;
        }
    }
    return count;
}

console.log(trailingZeros2(5));

function trailingZeros3(n){
    let count = 0;
    let temp=n/5;
    temp=Math.floor(temp);
    while (temp!=0) {
        count+=temp;
        temp/=5;
        temp=Math.floor(temp);
    }
    return count;
}

console.log(trailingZeros3(101));














