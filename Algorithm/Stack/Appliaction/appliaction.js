// 定义两个字符串，遇到#号删除#和上一个元素比较两个字符
function Stack(){
    this.items=[],
    this.push=function(el){
        this.items.push(el)
    },
    this.pop=function(){
        this.items.pop();
    }
    this.peek=function(){
        return this.items[this.items.length-1];
    },
    this.print=function(){
        return this.items.toString();
    }
};

var str1='ab#c';
var str2='acb#';

function compare(el){
    var stack=new Stack();
    for( i of el){
        if(i=='#'){
            stack.pop();
        }else{
            stack.push(i);
        }
    }
    return stack.print();
}

var str3=compare(str1);
var str4=compare(str2);
console.log(str3==str4);//true

