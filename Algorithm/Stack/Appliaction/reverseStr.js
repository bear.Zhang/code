function Stack(){
    this.items=[],
    this.push=function(el){
        this.items.push(el)
    },
    this.pop=function(){
        this.items.pop();
    }
    this.print=function(){
        return this.items.reverse().join('');
    }
};

// 反转字符串
function strStack1(str){
    var stack=new Stack();
    for( i of str){
        stack.push(i);
    }
    return stack.print();
}

// 反转字符

function strStack(el){
    var stack=new Stack();
    let str='',n=0;
    for( i of el){
        ++n;
        if(i==' '||n==el.length){
            if(n==el.length){
                str+=' ';
                stack.push(str);
                stack.push(i);
            }else{
                str+=i;
                stack.push(str);
                str='';
            }
            
        }else{
            str+=i;
        }
    }
    return stack.print();
}

let str='This is a word.';
console.log(strStack(str));

