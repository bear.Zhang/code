function Queue(){
    this.items=[];
    this.enqueue=function(el){
        this.items.push(el);
    };
    this.dequeue=function(){
        return this.items.shift();
    };
    this.front=function(){
        return this.items[0];
    }
    this.isEmpty=function(){
        if(this.items.length>0){
            return false
        }else{
            return true
        }
    }
}

let queue=new Queue();
queue.enqueue(1);
queue.enqueue(2);

console.log(queue.dequeue());//1
console.log(queue.isEmpty());//false