function Deque() {
    let items = []
    this.addFirst = function(e) {
        items.unshift(e)
    }
    this.removeFirst = function() {
        return items.shift()
    }
    this.addLast = function(e) {
        items.push(e)
    }
    this.removeLast = function() {
        return items.pop()
    }
    this.isEmpty = function() {
        return items.length === 0
    }
    this.front = function() {
        return items[0]
    }
    this.clear = function() { 
        items = [] 
    }
    this.size = function() {
        return items.length
    }
}

let deque=new Deque();
deque.addFirst(1);
console.log(deque.front());//1

