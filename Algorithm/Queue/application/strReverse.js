function Deque() {
    let items = []
    this.addFirst = function(e) {
        items.unshift(e)
    }
    this.removeFirst = function() {
        return items.shift()
    }
    this.addLast = function(e) {
        items.push(e)
    }
    this.removeLast = function() {
        return items.pop()
    }
    this.isEmpty = function() {
        return items.length === 0
    }
    this.front = function() {
        return items[0]
    }
    this.clear = function() { 
        items = [] 
    }
    this.size = function() {
        return items.length
    }
    this.print=function(){
        return items.join(' ');
    }
}

function strReverse(str){
    let index=-1,
    lastIndex=0,
    strChart=null;
    let deque=new Deque();
    do {
        index=str.indexOf(' ',index+1);
        if(index==-1){
            strChart=str.substring(lastIndex,str.length);
        }else{
            strChart=str.substring(lastIndex,index);
        }
        deque.addFirst(strChart);
        lastIndex=index;
    } while (index<str.length&&index!==-1);
    return deque.print();
}
let str="the sky is blue";
console.log(strReverse(str));
