// 计算n的阶乘

function factorial(n){
    return n>1 ? n*factorial(n-1):1;
}

console.log(factorial(5));//120

function scientificNotationToString(param) {
    let strParam = String(param)
    let flag = /e/.test(strParam)
    if (!flag) return param
  
    // 指数符号 true: 正，false: 负
    let sysbol = true
    if (/e-/.test(strParam)) {
      sysbol = false
    }
    // 指数
    let index = Number(strParam.match(/\d+$/)[0])
    // 基数
    let basis = strParam.match(/^[\d\.]+/)[0].replace(/\./, '')
  
    if (sysbol) {
      return basis.padEnd(index + 1, 0)
    } else {
      return basis.padStart(index + basis.length, 0).replace(/^0/, '0.')
    }
  }

var sum=scientificNotationToString(factorial(105));
console.log(sum)
var n=0;
String(sum).split('').reverse().forEach((item)=>{
    if(item==0){
        n++;
    }
});
console.log(n);