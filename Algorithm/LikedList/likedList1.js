function Node(el){
    this.el=el;
    this.next=null;
}

function LikedList(){
    let head=null;
    let length=0;
    this.add=function(el){
        var node=new Node(el);
        if(head==null){
            head=node;
        }else{
            var currentNode=head;
            while(currentNode.next){
                currentNode=currentNode.next;
            }
            currentNode.next=node;
        }
        length++;
    };
    this.head=function(){
        return head;
    };
    this.size=function(){
        return length;
    };
    this.remove=function(el){
        let currentNode=head;
        let prevNode=null;
        if(currentNode.el===el){
            head=currentNode.next
        }else{
            while(currentNode.next){
                prevNode=currentNode;
                currentNode=currentNode.next;
                if(currentNode.el===el){
                    prevNode.next==currentNode;
                }
            }
            head.next=prevNode;
        }
        length--;
    };
    this.indexOf=function(el){
        let index=-1;
        let currentNode=head;
        if(currentNode.el==el){
            index++;
            return index;
        }else{
            index++;
            while(currentNode.next){
                index++;
                currentNode=currentNode.next;
                if(currentNode.el==el){
                    return index;
                }
            }
        }
    };
    this.elementAt=function(targetIndex){
        let currentNode=head;
        let index=0;
        if(currentNode==null){
            index=-1;
            console.log('此链表为空');
        }else{
            while(index!==targetIndex&&currentNode.next){
                index++;
                currentNode=currentNode.next;
            }
            if(index===targetIndex){
                return currentNode;
            }else{
                return -1;
            }
        }
    };
    this.addAt=function(targetIndex,el){
        let node=new Node(el);
        let currentNode=head;
        let index=-1;
        if(currentNode==null||targetIndex==0){
            head=node;
            if(currentNode!=null){
                head.next=currentNode;
            }
            index++;
            length++;
        }else{
            index=index+2;
            while(currentNode.next&&targetIndex!=index){
                index++;
                currentNode=currentNode.next;
            }
            if(targetIndex===index){
                let next=currentNode.next;
                currentNode.next=node;
                currentNode=currentNode.next;
                currentNode.next=next;
                length++;
            }
            
        }
    };
    this.removeAt=function(targetIndex){
        let currentNode=head;
        let index=-1;
        let prevNode=null;
        if(targetIndex<length){
            index++;
            while(currentNode.next&&targetIndex!=index){
                index++;
                prevNode=currentNode
                currentNode=currentNode.next;
            }
            if(targetIndex==index){
                prevNode.next=currentNode.next;
            }
        }
    }
}

let likedList=new LikedList();
// 向链表尾部添加一个节点
likedList.add(1);
likedList.add(2);
likedList.add(3);
likedList.add(4);

// 返回链表的头部
console.log(likedList.head());//Node { el: 1, next: null }

// 返回链表的节点个数
console.log(likedList.size());//4

// 删除某个节点
likedList.remove(2);
console.log(likedList.head());//Node {el: 1,next: Node { el: 3, next: Node { el: 4, next: null } } }

// 返回某个节点的index
console.log(likedList.indexOf(1));//0
console.log(likedList.indexOf(4));//2

//返回某个index处的节点
console.log(likedList.elementAt(1));
console.log(likedList.elementAt(4));

// 在某个index处插入一个节点
likedList.addAt(1,2);
console.log(likedList.head());//Node {el: 1,next: Node { el: 2, next: Node { el: 3, next: [Node] } } }

// 删除某个index处的节点
likedList.removeAt(3);
console.log(likedList.head())

