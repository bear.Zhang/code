/**
 * 描述给定一个数组nums, 你需要返回这个数组所有子数组之和。
 * 如果nums = [2, 4, 1], 数组所有的子集是 {[2], [4], [1], [2, 4], [4, 1], [2, 4, 1]}
 * 保证返回的结果是int的类型
 */ 

function func(nums){
    let newNums=[];
    function fn(nums){
        for(let i =0 ;i<nums.length;i++){
            newNums.push([nums[i]]);
            let tempNums=[];
            for(let j=i+1;j<nums.length;j++){
                tempNums.push(nums[j]);
                newNums.push([nums[i],...tempNums]);
            }
        }
        return newNums;
    }
    let res=fn(nums).flat().reduce((acc,cur)=>{
        return acc+cur
    },0)
    return res;
}

let nums=[1, 2, 3];
let res=func(nums);
console.log(res);