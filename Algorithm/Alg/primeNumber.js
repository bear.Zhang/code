/**
 * 输出n以内所有的素数。
 * 保证 n 是100以内的整数。
 */

function fn(n){
    if(Number.isInteger(n)&&n>1){
        let arr=[];
        for(let i=2;i<=n;i++){
            let isPrime=false;
            for(let j=2;j<=i;j++){
                if(i%j==0&&i!=j){
                    isPrime=false;
                    break;
                }else{
                    isPrime=true;
                }
            }
            if(isPrime){
                arr.push(i);
            }
        }
        return arr;
    }else{
        return [];
    }
}

let res=fn(10);
console.log(res);