/**
 * 数据结构化
 * 
 * 输入：
 *{
 *  name:'Tom',
 *  age:12,
 *  parent:{
 *     tid:'222',
 *      job:'work'
 *  },
 *  friend:{
 *      id:'22',
 *      rel:'bb',
 *      circle:{
 *          uuid:'23',
 *          work:'beijing'
 *      }
 *  }
 * 
 * }
 * 
 * 输出：
 * {
 *  name:'Tom'
 *  age:12,
 *  'parent.tid':'222',
 *  'parent.job':'work',
 *  'friend.id':'22',
 *  'friend.rel':'bb',
 *  'friend.circle.uuid':'23',
 *  'friend.circle.work':'beijing'
 * }
 * 
 */

function func(req){
    let newReq={};
    function fn(req,newReq,lastkey=null){
        for(let key in req){
            if(req[key].constructor==Object){
                fn(req[key],newReq,lastkey==null?`${key}.`:`${lastkey+key}.`);
            }else{
                if(lastkey){
                    newReq[lastkey+key]=req[key];
                }else{
                    newReq[key]=req[key];
                }
            }
        }
        return newReq;
    }
    return fn(req,newReq);
}
let req={
    name:'Tom',
    age:12,
    parent:{
        tid:'222',
        job:'work'
    },
    friend:{
        id:'22',
        rel:'bb',
        circle:{
            uuid:'23',
            work:'beijing',
            other:{
                sex:'男',
                time:'2',
                tit:{
                    dd:'we'
                }
            }
        }
    }
};
let res=func(req);
console.log(res);