/**
 * 给定一个非空数组，返回此数组中第三大的数。如果不存在，则返回数组中最大的数。要求算法时间复杂度必须是O(n)。
 */

function arrSort(arr){
    return arr.sort((item1,item2)=>item1-item2);
}

function func(num){
    let newNum=Array.from(new Set(num));
    if(newNum.length<3){
        return Math.max(...newNum);
    }else{
        let res=arrSort(newNum);
        return res[res.length-3];
    }
}

let num=[3,2,2,1];
let res=func(num);
console.log(res);