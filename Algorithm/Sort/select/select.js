function swap(arr, i, j){
    var temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
};

function selectSort(arr){
    let len=arr.length;
    for(let i=0;i<len;i++){
        let miniIndex=i;
        for(let j=i+1;j<len;j++){
            if(arr[miniIndex]>arr[j]){
                miniIndex=j;
            }
        }
        swap(arr,i,miniIndex);
    }
    return arr;
}
const arr = [1,6,3,2,4,5];
console.log(selectSort(arr));//[ 1, 2, 3, 4, 5, 6 ]
