// 归并排序
const arr = [5,8,6,3,9,2,1,7];

function mergeSort(arr){
      const len = arr.length;
      for(let seg =1; seg < len; seg += seg){
         let arrB = [];
         for(let start =0; start < len; start +=2*seg){
               let row = start, mid =Math.min(start+seg, len), heig =Math.min(start +2*seg, len);
               let start1 = start, end1 = mid;
               let start2 = mid, end2 = heig;
               while(start1 < end1 && start2 < end2){
                  arr[start1] < arr[start2] ? arrB.push(arr[start1++]) : arrB.push(arr[start2++]);
               }
               while(start1 < end1){
                  arrB.push(arr[start1++]);
               }
               while(start2 < end2){
                  arrB.push(arr[start2++]);
               }
         }
         arr = arrB;
      }
      return arr;
};
console.log(mergeSort(arr));//[ 1, 2, 3, 5, 6, 7, 8, 9 ]