// 冒泡排序
let arr = [1,6,3,2,4,5];

function bubbleSort(arr){
    let len=arr.length;
    for(let i=0;i<len;i++){
        for(let j=0;j<len-i;j++){
            if(arr[j]>arr[j+1]){
                swap(arr,j,j+1);
            }
        }
    }
    return arr;
}

function swap(arr, i, j){
    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}
bubbleSort(arr)
console.log(arr);



