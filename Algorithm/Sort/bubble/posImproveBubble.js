// 改进冒泡排序，记录最后一次交换的位置
const arr = [1, 20, 10, 30, 22, 11, 55, 24, 31, 88, 12, 100, 50, 112];

function swap(arr, i, j) {
    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp
};

function improveBubble(arr, len) {
    for (let i = len - 1; i >= 0; i--) {
        let pos = 0;
        for (let j = 0; j < i; j++) {
            if (arr[j] > arr[j + 1]) {
                swap(arr, j, j + 1);
                pos = j + 1;
            }
        }
        len = pos + 1;
    }
    return arr;
};
console.log(improveBubble(arr, arr.length)); // [ 1, 10, 11, 12, 20, 22, 24, 30, 31, 50, 55, 88, 100, 112 ]
