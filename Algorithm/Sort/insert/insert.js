// 插入排序
function insertSort(arr){
    let len=arr.length;
    for(let i =0; i < len; i++){
        let temp = arr[i];
        for(let j =0; j < i; j++){
            if(temp < arr[j] && j==0){
                arr.splice(i,1);
                arr.unshift(temp);
                break;                                                                                                                         
            }else if(temp > arr[j] && temp < arr[j+1] && j < i -1){
                arr.splice(i,1);
                arr.splice(j+1,0, temp);
                break;
            }
        }
    }
    return arr;
}
const arr = [5,8,6,3,4,2,1,7];
console.log(insertSort(arr));//[ 1, 2, 3, 4, 5, 6, 7, 8 ]