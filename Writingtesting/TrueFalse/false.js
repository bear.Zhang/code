function fn(all){
    return !!all;
}

console.log(fn(''));// false
console.log(fn(-1));// true
console.log(fn(NaN));// false
console.log(fn('0'));// true