function test(arr){
    for(let i=0;i<arr.length;i++){
        for(let j=i;j<arr.length;j++){
            if(arr[j]>arr[j+1]){
                let tem=arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=tem;
            }
        }
    }
    return arr;
}

test([1,3,2]);

function Child(){
    this.name=null;
    this.age=null,
    Parent.apply(this, this.name,this.age)
}

Child.prototype=new Parent();

function twoSum(nums, target) {
    let arr=[];
    for(let i=0;i<nums.length;i++){
        for(let j=i+1;j<nums.length;j++){
            if((nums[i]+nums[j])==target){
                arr.push(i);
                arr.push(j);
                return arr;
            }else{
                return arr;
            }
        }
    }
}
