/**
 * 二次封装函数：
 * 已知函数fn执行需要3个参数。请实现函数partial，调用之后满足如下条件：
 * 返回一个函数result，该函数接受一个参数；
 *  执行result(str3)，返回的结果与fn(str1,str2,str3)一致
 */
function partial(fn,str1,str2){
    return function result(str3){
        return fn(str1,str2,str3);
    }
}

function fn(str1,str2,str3){
    return str1+str2+str3;
}

var result=partial(fn,1,2);

console.log(result(3));

console.log(fn(1,2,3));


/**
 * 实现 fizzBuzz 函数，参数 num 与返回值的关系如下：
 * 如果 num 能同时被 3 和 5 整除，返回字符串 fizzbuzz
 * 如果 num 能被 3 整除，返回字符串 fizz
 * 如果 num 能被 5 整除，返回字符串 buzz
 * 如果参数为空或者不是 Number 类型，返回 false
 * 其余情况，返回参数 num
 */


function fizzBuzz(n){
    if(typeof n ==="number"&& !isNaN(n)){
        let str='';
        if(n%3===0){
            str+='fizz';
        };
        if(n%5===0){
            str+='buzz'
        }
        return str||n;
    }else{
        return false;
    }
    

}

console.log(fizzBuzz(3));

/**
 * 分别使用递归和迭代的方式实现斐波那契数列值的计算： F(0) = 0,F(1) = 1,F(n) = F(n - 1) + F(n - 2);(n>=2, n为正整数)
 */
//递归
function func(n,preN){
    if((preN==undefined&&n<2)||!Number.isInteger(n)){
        return new Error('请输入n>=2的正整数');
    }
    if(n==0||n==1){
        return n;
    }else{
        return func(n-1,n)+func(n-2,n);
    }
}

// 迭代
function func2(n){
    if(n<2||!Number.isInteger(n)){
        return new Error('请输入n>=2的正整数');
    }else{
        let arr=[];
        for(let i=0;i<=n;i++){
            if(i==0||i==1){
                arr.push(i)
            }else{
                arr.push(arr[i-1]+arr[i-2]);
            }
        }
        return arr.pop();
    }
}
console.log(func2(1));