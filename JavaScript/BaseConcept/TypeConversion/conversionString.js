// 其他类型转化为字符串

// 使用toString
var a = 123;
console.log(a.toString()); //'123'
console.log(typeof (a.toString())); //string

let bol=true;
console.log(bol.toString())//'true'

var b = null;
//console.log(b.toString());//TypeError: Cannot read property 'toString' of null
var c = undefined;
//console.log(c.toString());//TypeError: Cannot read property 'toString' of undefined

var isNum = 10;
// 转化为二进制
console.log(isNum.toString(2)); //1010
// 转化为8进制
console.log(isNum.toString(8)); //12
// 转化为16进制
console.log(isNum.toString(16)); //a

// 使用String()
var d=123;
console.log(String(d));//'123'
var e=null;
console.log(String(e));//'null'
var f=undefined;
console.log(String(f));//'undegfined'
var obj={a:1};
console.log(String(obj));//'[object,object]'
var arr=[1,2,3];
console.log(String(arr));//'1,2,3'

console.log(typeof(String({a:1})));//string
console.log(typeof(String([1,2,3])));//string

