var str='';

/*typeof*/
console.log(typeof str=='string');//true

/*instanceof*/
console.log(str instanceof String);//false

var newStr=new String('new str');

console.log(newStr instanceof String);//true

/*toString*/
console.log(Object.prototype.toString.call(str)==='[object String]');//true

console.log(Object.prototype.toString.call(str).indexOf('String'));//8

console.log(Object.prototype.toString.call(str).indexOf('String')!==-1);//true
