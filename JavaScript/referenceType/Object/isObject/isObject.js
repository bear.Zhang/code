var obj={};

/*toStirng()*/
console.log(Object.prototype.toString.call(obj)==='[object Object]');//true

console.log(Object.prototype.toString.call(arr).indexOf('Array'));//8

console.log(Object.prototype.toString.call(arr).indexOf('Array')!==-1);//true
/*constructor*/
console.log(obj.constructor===Object);//true

/*instanceof*/
console.log(obj instanceof Object);//true

/*typeof*/
console.log(typeof obj==='object');//true
