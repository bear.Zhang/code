// console.log(1)
// setTimeout(() => console.log(5), 0)
// new Promise((resolve, reject) => {
// 	console.log(2)
// 	resolve()
// }).then(() => {
// 	console.log(4)
// })
// console.log(3)

// 1 2 3 4 5

console.log(1)

setTimeout(() => {
    console.log(5)
    new Promise((resolve, reject) => {
        console.log(6)
        resolve()
    }).then(() => {
        console.log(7)
    })
}, 0)

new Promise((resolve, reject) => {
    console.log(2)
    resolve()
}).then(() => {
    console.log(4)
})

setTimeout(() => {
    console.log(8)
    new Promise((resolve, reject) => {
        console.log(9)
        resolve()
    }).then(() => {
        console.log(10)
    })
}, 0)

console.log(3)

// Node：1 2 3 4 5 6 8 9 7 10
// 浏览器：1 2 3 4 5 6 7 8 9 10

// Promise.resolve().then(()=>{
//     console.log('Promise1')  
//     setTimeout(()=>{
//       console.log('setTimeout2')
//     },0)
//   });
//   setTimeout(()=>{
//     console.log('setTimeout1')
//     Promise.resolve().then(()=>{
//       console.log('Promise2')    
//     })
//   },0);
//   console.log('start');

  // Nnode：start Promise1 setTimeout1 setTimeout2 Promise2
  // 浏览器：start Promise1 setTimeout1 Promise2 setTimeout2