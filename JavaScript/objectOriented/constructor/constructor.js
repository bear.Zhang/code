function Parent(age) {
    this.age = age;
}

var p = new Parent(50);
console.log(p.constructor === Parent); // true
console.log(p.constructor === Object); // false

function parent3(age) {
    return {
        age: age
    }
}
var p3 = parent3(50);
console.log(p3.constructor === Object); // true

//引用类型
function Foo() {
    this.value = 42;
}
Foo.prototype = {
    method: function() {}
};

function Bar() {}

// 设置 Bar 的 prototype 属性为 Foo 的实例对象
Bar.prototype = new Foo();
Bar.prototype.foo = 'Hello World';

console.log(Bar.prototype.constructor === Object);
// true

// 修正 Bar.prototype.constructor 为 Bar 本身
Bar.prototype.constructor = Bar;

var test = new Bar() // 创建 Bar 的一个新实例
console.log(test);//Bar{}

//基本类型
function Type() { };
var	types = [1, "hello", true, Symbol(123)];

for(var i = 0; i < types.length; i++) {
	types[i].constructor = Type;
	types[i] = [ types[i].constructor, types[i] instanceof Type, types[i].toString() ];
};

console.log(types.join("\n") );
//function Number() { [native code] },false,1
//function String() { [native code] },false,hello
//function Boolean() { [native code] },false,true
//function Symbol() { [native code] },false,Symbol(123)

function ParentProto() {}
var p = new ParentProto();
console.log(p.__proto__ ===ParentProto.prototype);//true