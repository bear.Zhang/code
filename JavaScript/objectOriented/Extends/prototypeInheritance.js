// 原型式继承

function Parent(obj){
    function Fn(){};
    Fn.prototype=obj;
    return new Fn();
};
var person={
    name:"Nicholas",
    friends:["shelby","Court","Van"]
};

var anotherPerson=Parent(person);
anotherPerson.name="Greg";
anotherPerson.friends.push("Rob");

var yetAnotherPerson=Parent(person);
yetAnotherPerson.name="Linda";
yetAnotherPerson.friends.push("Rob2");

console.log(person.friends);//[ 'shelby', 'Court', 'Van', 'Rob', 'Rob2' ]   