/**
 * Oss 对象服务存储前端方法封装 
 */

// 根据oss url 获取文件数据流
/**
 * @description 获取文件数据流
 * @export
 * @param {*} files 文件列表
 * @returns 文件数据流对象
 */
export function getFileStream(files) {
    return new Promise(resolve => {
        if (files.length > 0) {
            let data = [];
            files.forEach(item => {
                ossRepository
                .download(getFileUrl(item.url), p => {})
                .then(res => {
                    data.push({
                        name: `${item.name}${item.type}`,
                        blob: res,
                    });
                    if (data.length == files.length) {
                        resolve(data);
                    }
                });
            });
        }
    });
}

// 文件单个/多个下载
export function fileDownload(files) {
    if (files instanceof Array) {
        // 多文件打包下载
        getFileStream(files).then(datas => {
        // 拼装压缩包格式
        if (datas.length > 0) {
            SaveMultipleFile(`附件.zip`, datas).then(() => {
            console.log('下载成功');
            });
        }
        });
    } else {
        // 单文件下载
        ossRepository
        .download(getFileUrl(files.url), progress => {})
        .then(blob => {
            SaveSingleFile(`${files.name}${files.type}`, files.size, blob).then(a => {
                console.log('下载成功');
            });
        });
    }
}

/**
 * @description 批量下载
 * @export
 * @param {*} zipName 压缩包名称
 * @param {*} files 需要压缩的文件信息
 */
export function SaveMultipleFile(zipName, files) {
    return new Promise((resolve,error) => {
        // 首先定义一个文件夹
        let zip=new jsZIP();
        files.forEach(a=>{
        zip.file(a.name,a.blob);
        });
        zip.generateAsync({type:"blob"})
        .then(function(content) {
            saveAs(content, zipName);
            resolve();
        }).catch(()=>{
        error();
        });
    });
}

// 常用的文件类型
export let resourceIcon = {
    folder: 'folder-open',
    xls: 'file-excel',
    xlsx: 'file-excel',
    md: 'file-markdown',
    pdf: 'file-pdf',
    ppt: 'file-ppt',
    txt: 'file-text',
    doc: 'file-word',
    unknown: 'file-unknown',
    zip: 'file-zip',
    other: 'file',
    jpg: 'file-image',
};

// 创建文件格式对应表
export const FileTypes = [
    { k: 'audio/3gpp', v: '3gpp' },
    { k: 'video/3gpp', v: '3gpp' },
    { k: 'audio/ac3', v: 'ac3' },
    { k: 'allpication/vnd.ms-asf', v: 'asf' },
    { k: 'audio/basic', v: 'au' },
    { k: 'text/css', v: 'css' },
    { k: 'text/csv', v: 'csv' },
    { k: 'application/msword ', v: 'dot' },
    { k: 'application/xml-dtd', v: 'dtd' },
    { k: 'image/vnd.dwg', v: 'dwg' },
    { k: 'image/vnd.dxf', v: 'dxf' },
    { k: 'image/gif', v: 'gif' },
    { k: 'text/htm', v: 'htm' },
    { k: 'text/html', v: 'html' },
    { k: 'image/jp2', v: 'jp2' },
    { k: 'image/jpeg', v: 'jpeg' },
    { k: 'text/JavaScript', v: 'js' },
    { k: 'application/JavaScript', v: 'js' },
    { k: 'application/json', v: 'json' },
    { k: 'audio/mpeg', v: 'mp2' },
    { k: 'audio/mp4', v: 'mp4' },
    { k: 'video/mpeg', v: 'mpeg' },
    { k: 'application/vnd.ms-project', v: 'mpp' },
    { k: 'application/ogg', v: 'ogg' },
    { k: 'audio/ogg', v: 'ogg' },
    { k: 'application/pdf', v: 'pdf' },
    { k: 'image/png', v: 'png' },
    { k: 'application/vnd.ms-powerpoint', v: 'ppt' },
    { k: 'application/rtf', v: 'rtf' },
    { k: 'text/rtf', v: 'rtf' },
    { k: 'image/vnd.svf', v: 'svf' },
    { k: 'image/tiff', v: 'tif' },
    { k: 'text/plain', v: 'txt' },
    { k: 'application/vnd.ms-works', v: 'wdb' },
    { k: 'text/xml', v: 'xml' },
    { k: 'application/xhtml+xml', v: 'xhtml' },
    { k: 'application/xml', v: 'xml' },
    { k: 'application/vnd.ms-excel', v: 'xls' },
    { k: 'aplication/zip', v: 'zip' },
    { k: 'pplication/vnd.openxmlformats-officedocument.spreadsheetml.sheet', v: 'xlsx' },
];

// 4、文件大小转换
/**
 * @description 文件大小转换
 * @export
 * @param {*} fileSize
 * @returns kb GB  G
 */
export function FileSizeTrans(fileSize) {
    let size = '';
    if (fileSize < 0.1 * 1024) {
        //如果小于0.1KB转化成B
        size = fileSize.toFixed(2) + 'B';
    } else if (fileSize < 0.1 * 1024 * 1024) {
        //如果小于0.1MB转化成KB
        size = (fileSize / 1024).toFixed(2) + 'KB';
    } else if (fileSize < 0.1 * 1024 * 1024 * 1024) {
        //如果小于0.1GB转化成MB
        size = (fileSize / (1024 * 1024)).toFixed(2) + 'MB';
    } else {
        //其他转化成GB
        size = (fileSize / (1024 * 1024 * 1024)).toFixed(2) + 'GB';
    }
}