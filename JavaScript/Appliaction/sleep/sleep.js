function sleep (interval=20) {
    return new Promise((resolve) =>{ 
        setTimeout(()=>{
            resolve()
        }, interval)
    });
}

async function sleepAsync() {
    for(let i = 1; i <= 5; i++) {
        console.log(i);
        await sleep(1000)
    }
}

function sleepPromise() {
    function logAndSleep(i) {
        console.log(i);
        if (i === 5) {
            return;
        }
        return sleep(1000).then(() => logAndSleep(i + 1));
    }
    logAndSleep(1);
}

// sleepAsync();
sleepPromise();