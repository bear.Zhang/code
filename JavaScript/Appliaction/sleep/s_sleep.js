function sleep (interval=20) {
    return new Promise((resolve) =>{ 
        setTimeout(()=>{
            resolve()
        }, interval)
    });
}

function one2FiveInPromise() {
    function logAndSleep(i) {
        console.log(i);
        if (i === 5) {
            return;
        }
        return sleep(1000).then(() => logAndSleep(i + 1));
    }
    logAndSleep(1);
}

one2FiveInPromise();