// 生成随机十六进制颜色

const randomColor=()=>{
    return `#${Math.random().toString(16).slice(2,8).padStart(6,'0')}`;
}
console.log(randomColor());