/*浅拷贝*/

//Object.assign()
let a = {
    name: "muyiy",
    book: {
        title: "You Don't Know JS",
        price: "45"
    }
}
let b = Object.assign({}, a);
console.log(b);//{ name: 'muyiy',book: { title: 'You Don\'t Know JS', price: '45' } }

a.name = "change";
a.book.price = "55";
console.log(a);//{ name: 'change',book: { title: 'You Don\'t Know JS', price: '55' } }

console.log(b);//{ name: 'muyiy',book: { title: 'You Don\'t Know JS', price: '55' } }

//...
let c = {
    name: "muyiy",
    book: {
        title: "You Don't Know JS",
        price: "45"
    }
}
let d = {...c};
console.log(d);//{ name: 'muyiy',book: { title: 'You Don\'t Know JS', price: '45' } }

c.name = "change";
d.book.price = "55";
console.log(c);//{ name: 'change',book: { title: 'You Don\'t Know JS', price: '55' } }

console.log(d);//{ name: 'muyiy',book: { title: 'You Don\'t Know JS', price: '55' } }

//Array.prototype.slice()
let e = [0, "1", [2, 3]];
let f = e.slice(1);
console.log(f);// ["1", [2, 3]]

e[1] = "99";
e[2][0] = 4;
console.log(e);// [0, "99", [4, 3]]

console.log(f);//  ["1", [4, 3]]

