/*赋值*/

/*引用类型进行赋址*/

let a = {
    name: "muyiy",
    book: {
        title: "You Don't Know JS",
        price: "45"
    }
}
let b = a;
console.log(b);//{ name: 'muyiy',book: { title: 'You Don\'t Know JS', price: '45' } }

a.name = "change";
a.book.price = "55";
console.log(a);//{ name: 'change',book: { title: 'You Don\'t Know JS', price: '55' } }

console.log(b);//{ name: 'change',book: { title: 'You Don\'t Know JS', price: '55' } }

