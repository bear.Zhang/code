let a = {
    name: "muyiy",
    book: {
        title: "You Don't Know JS",
        price: "45"
    }
}
let b = JSON.parse(JSON.stringify(a));
console.log(b);//{ name: 'muyiy',book: { title: 'You Don\'t Know JS', price: '45' } }

a.name = "change";
a.book.price = "55";
console.log(a);//{ name: 'change',book: { title: 'You Don\'t Know JS', price: '55' } }

console.log(b);//{ name: 'muyiy',book: { title: 'You Don\'t Know JS', price: '45' } }

//数组深拷贝
let c = [0, "1", [2, 3]];
let d = JSON.parse(JSON.stringify(c.slice(1) ));
console.log(d);// ["1", [2, 3]]

c[1] = "99";
c[2][0] = 4;
console.log(c);// [0, "99", [4, 3]]

console.log(d);//  ["1", [2, 3]]

//undefined、symbol 和函数

let obj = {
    name: 'muyiy',
    a: undefined,
    b: Symbol('muyiy'),
    c: function() {}
}
console.log(obj);//{ name: 'muyiy',a: undefined,b: Symbol(muyiy),c: [Function: c] }

let e = JSON.parse(JSON.stringify(obj));
console.log(e);// {name: "muyiy"}

//循环引用
let objDeep = {
    a: 1,
    b: {
        c: 2,
   	    d: 3
    }
}
objDeep.a = objDeep.b;
objDeep.b.c = objDeep.a;

// let f = JSON.parse(JSON.stringify(objDeep));// Uncaught TypeError: Converting circular structure to JSON

//Date()
console.log(new Date());//Sun Sep 29 2019 11:20:18 GMT+0800  Node环境下:2019-09-29T03:19:47.036Z

console.log(JSON.stringify(new Date()));//"2019-09-29T03:22:04.304Z"

console.log(JSON.parse(JSON.stringify(new Date())));//2019-09-29T03:23:37.883Z

let date = (new Date()).valueOf();

console.log(date);//1569727921589

console.log(JSON.stringify(date));//1569727921589

console.log(JSON.parse(JSON.stringify(date)));//1569727921589

//正则情况下
let objTrue= {
    name: "muyiy",
    a: /'123'/
}
console.log(objTrue);// {name: "muyiy", a: /'123'/}

let t = JSON.parse(JSON.stringify(objTrue));
console.log(t);// {name: "muyiy", a: {}}
