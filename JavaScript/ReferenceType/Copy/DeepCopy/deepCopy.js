/**
 * 递归实现深拷贝 
 */

function checkType(target){
    return Object.prototype.toString.call(target).slice(8,-1);
}

function deepCopy(target){
    let result, targetType=checkType(target);
    if(targetType==='Object'){
        result={};
    }else if(targetType==='Array'){
        result=[];
    }else{
        return target
    }
    for(let i in target){
        let value=target[i];
        if(checkType(value)==='Object'||checkType(value)==='Array'){
            deepCopy(value);
        }else{
            result[i]=value;
        }
    }
    return result;
}

// 基本类型的深拷贝
let str='abc';
let str1=deepCopy(str);
str1='123';
console.log(str1);// 123
console.log(str);// abc

// 引用类型的深拷贝
let arr=[1,2,3,undefined,Symbol('arr')];
let arr1=deepCopy(arr);
arr1[1]='a';
console.log(arr1);
console.log(arr);

let obj={
    a:1,
    b:2,
    c: function() {}
};
let obj1=deepCopy(obj);
obj1.a='a';
console.log(obj1);
console.log(obj);

