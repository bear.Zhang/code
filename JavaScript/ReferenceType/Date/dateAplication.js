/**
 * 时间应用 
 */

// 获取当前日期和时间
let currentDateTime=new Date();
console.log(currentDateTime);
// 浏览器：Mon Nov 09 2020 23:08:13 GMT+0800 (中国标准时间)
// node：2020-11-09T15:07:39.276Z

// 获取当前日期
let currentDate=new Date().toLocaleDateString();
console.log(currentDate);
// 浏览器：2020/11/9
// node：2020-11-9

/**
 * formateDate方法，获取指定格式日期
 * @param {String} format [日期中间格式]
 * 说明：b.substr(-2)的含义是将字符串b字符串从后面数起，返回倒数两位的字符
 */
function formatDate(format='-'){
    let t = new Date();
    let fn1 = (d) =>('0'+d).substr(-2);
    let callbackDate=t.getFullYear()+`${format}`+fn1(t.getMonth()+1)+`${format}`+fn1(t.getDate());
    return callbackDate;
}
console.log(formatDate('/')); // 2020/11/09

// 返回当前时间
// 获取的是12小时制：
let currentTime12=new Date().toLocaleTimeString();
console.log(currentTime12);
// 获取的是24小时制：
let currentTime24=new Date().toLocaleTimeString('chinese',{hour12:false});
console.log(currentTime24);

// 从Date()对象返回当前 年份
let currentYear=new Date().getFullYear();
console.log(currentYear);

// 从Date()对象返回当前 月份，注意：月份的返回值范围是 0~11，所以要获得当前月份，要+1
let currentMonth=new Date().getMonth()+1;
console.log(currentMonth);

// 从Date()对象返回月份的当前 日
let currentDay=new Date().getDate();
console.log(currentDay);

// 从Date()对象返回一个星期的某一天，当前是星期几，注意：获取的返回值范围是 0~6 , 0表示星期天
let currentWeek=new Date().getDay();
console.log(currentWeek);

// 从Date()对象的 当前 小时,注意：获取返回值的范围是 0~23
let currentHours=new Date().getHours();
console.log(currentHours);

// 返回Date()对象的 当前 分钟，注意：获取返回值的范围是 0~59
let currentMinutes=new Date().getMinutes();
console.log(currentMinutes);

// 返回Date()对象的 当前 秒数，注意：获取返回值的范围是 0~59
let currentSeconds=new Date().getSeconds();
console.log(currentSeconds);

// 返回Date()对象的 当前 毫秒数，注意：范围是 0~999
let currentMilliseconds=new Date().getMilliseconds();
console.log(currentMilliseconds);

// 返回 日期 1970.01.01 距现在的毫秒数
let currentTime=new Date().getTime();
console.log(currentTime);

// 两个日期间隔天数
const diffDays=(startDate,endDate)=>Math.ceil(Math.abs((new Date(startDate)-new Date(endDate))/(1000*60*60*24)));
console.log(diffDays('2021-06-01','2021-06-24'));