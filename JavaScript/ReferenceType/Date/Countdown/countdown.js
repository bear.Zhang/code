// 倒计时

countdownTime(this.startUp,priceQuoteEndTime,(val)=>{
    this.priceQuoteOverTime=val;
  });
  countdownTime(this.startUp,endTime,(val)=>{
    this.biddingOverTime=val;
  })

export function countdownTime(startUp,endTime,callback){
    var timeout=null;
    if(startUp){
      timeout =setInterval(function(){
        // 获取现在的时间
        console.log(1);
        var date = new Date();
        var until = new Date(endTime);
        // 计算时会发生隐式转换，调用valueOf()方法，转化成时间戳的形式
        var days = (until - date)/1000/3600/24; 
        // 下面都是简单的数学计算 
        var day = Math.floor(days);
        var hours = (days - day)*24;
        var hour = Math.floor(hours);
        var minutes = (hours - hour)*60;
        var minute = Math.floor(minutes);
        var seconds = (minutes - minute)*60;
        var second = Math.floor(seconds);
        var back = day+'天'+hour+'小时'+minute+'分钟'+second+'秒';
        callback(back);
      },1000)
    }else{
      clearTimeout(timeout);
    }
  }
  
  
  export function countdownTime(){
    this.timeout=null;
  }
  countdownTime.prototype.startUp=function(endTime,callback){
    this.timeout =setInterval(function(){
        // 获取现在的时间
        var date = new Date();
        var until = new Date(endTime);
        // 计算时会发生隐式转换，调用valueOf()方法，转化成时间戳的形式
        var days = (until - date)/1000/3600/24; 
        // 下面都是简单的数学计算 
        var day = Math.floor(days);
        var hours = (days - day)*24;
        var hour = Math.floor(hours);
        var minutes = (hours - hour)*60;
        var minute = Math.floor(minutes);
        var seconds = (minutes - minute)*60;
        var second = Math.floor(seconds);
        var back = day+'天'+hour+'小时'+minute+'分钟'+second+'秒';
        callback(back);
    },1000)
  }
  
  countdownTime.prototype.shutDown=function(){
    console.log(11);
    clearTimeout(this.timeout);
  }
  

  /**
 * 倒计时器
 */
function CountdownTimer(params) {
  var _settings = {
      startTime: new Date(),
      endTime: new Date(),
      callback: undefined,
      endCallback: undefined
  };
  //this.settings = $.extend(_settings, params);
  this.settings = _settings;
  for(var p in params){
    this.settings[p] = params[p]
  }
  this.startTime = this.settings.startTime;
  this.endTime = this.settings.endTime;
  this.callback = this.settings.callback;
  this.endCallback = this.settings.endCallback;
  this.running = false;
  this.intervalTimer = undefined;

};
CountdownTimer.prototype.run = function (ct, tt) {
  this.running = true;
  var tt = tt || this.endTime.getTime();
  var ct = ct || this.startTime.getTime();
  var cb = this.callback;
  //开始执行计时的客户端时间，用来校准
  var startAt = new Date();
  // 如果截止时间在开始时间之前，直接触发倒计时截止事件
  if (tt <= ct) {
      this.stop();
      if (cb) {
          cb({ d: 0, h: 0, m: 0, s: 0 });
      }
      return { d: 0, h: 0, m: 0, s: 0 };
  }
  var selfobj = this;
  var timeToGo = tt - ct;
  this.intervalTimer = setInterval(function () {
      var diffTime = tt - ct;
      ct += 1000;
      var timePassed = new Date().valueOf() - startAt.valueOf() - 1000;//客户端实际走过的时间
      var timeCounted = timeToGo - diffTime;//interval计算出的时间差
      //当timePassed和timeCounted不相同时，说明interval已经有偏差。偏差大于1000ms时，需要进行校准，以timePassed为准。前提是用户没有手动修改客户端时间
      //todo:加上用户手动修改客户端时间的判断
      if (Math.abs(timePassed - timeCounted) > 1000) {
          diffTime = timeToGo - timePassed;
      }
      if (diffTime <= 0) {
          selfobj.stop.apply(selfobj);
          /*
          if (cb) {
              cb({ d: 0, h: 0, m: 0, s: 0 });
          }
          */
          return { d: 0, h: 0, m: 0, s: 0 };
      }

      var d = parseInt(diffTime / 86400000, 10);
      var h = parseInt(diffTime % 86400000 / 3600000, 10);
      var m = parseInt(diffTime % 86400000 % 3600000 / 60000, 10);
      var s = parseInt(diffTime % 86400000 % 3600000 % 60000 / 1000, 10);
      if (cb) {
          cb({ d: d, h: h, m: m, s: s });
      }
      /*
      if(selfobj.delegateTime && selfobj.delegateTime.h == h && selfobj.delegateTime.m == m && selfobj.delegateTime.s == s){
          selfobj.delegateHandler();
      }
      */
  }, 1000);
};
/**
* 清除倒计时器
*/
CountdownTimer.prototype.clear = function () {
  if (this.intervalTimer) {
      clearInterval(this.intervalTimer);
      this.intervalTimer = undefined;
  }
  this.running = false;
};
CountdownTimer.prototype.stop = function () {
  this.clear();
  if (this.endCallback) {
      this.endCallback();
  }
};
CountdownTimer.prototype.delegate = function (time, handler) {
  //time = 0:5:0小时：分钟：秒
  var t = time.split(":");
  this.delegateTime = { h: t[0], m: t[1], s: t[2] };
  this.delegateHandler = handler;
}