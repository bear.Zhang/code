/**
 * number.toString()
 */
const num=123;
console.log(num.toString());
console.log((100+44).toString());
// 输出二进制
const num2=2;
console.log(num2.toString(2));

// 输出八进制
const num8=8;
console.log(num8.toString(8));

// 输出十六进制
const num16=16;
console.log(num16.toString(16));
/**
 * number.toExponential()
 */

const nume=3.141;
console.log(nume.toExponential(2));
