console.log(typeof undefined);//undefined
console.log(typeof null);//object
console.log(typeof true);//boolean
console.log(typeof 1);//number
console.log(typeof 'abc');//string
console.log(typeof function() {});//function
console.log(typeof {});//object
console.log(typeof []);//object