function fn(){};

console.log(Object.prototype.toString.call(1));//'[object Number]'
console.log(Object.prototype.toString.call('1'));//'[object String]'
console.log(Object.prototype.toString.call(NaN));//'[object Number]'
console.log(Object.prototype.toString.call(fn));//'[object Function]'
console.log(Object.prototype.toString.call([1,2,3]));//'[object Array]'
console.log(Object.prototype.toString.call(undefined));//'[object Undefined]'
console.log(Object.prototype.toString.call(null));//'[object Null]'
console.log(Object.prototype.toString.call(true));//'[object Boolean]'