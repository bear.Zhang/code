// 反转字符串

// 第一个种
var str1 = 'abcdef';
console.log(str1.split('').reverse().join(''));//fedcba

// 第二种
var str2="abcdef"
var i=str2.length;
i=i-1;
var strNew='';
for (var x = i; x >=0; x--){
    strNew+=str2.charAt(x);
}
console.log(strNew);//fedcba

