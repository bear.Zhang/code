// 分组

let regExp1=/T(oo||ii)m/g;
let str1='abToomhaTiimmm';
console.log(str1.replace(regExp1,'-')); //ab-ha-mm

console.log('.............');
/**
 * 反向引用
 */

let str2='2018-02-11';
let regExp2=/(\d{4})\-(\d{2})\-(\d{2})/g;
console.log(str2.replace(regExp2,'$2/$3/$1'));//02/11/2018

console.log('.............');
/**
 * 后向引用
 * 使用后向引用时前面匹配的结果影响后面的匹配，即后面的匹配会按照前面进行匹配
 */

var regExp3 = /\d{4}(\-|\/|\.)\d{1,2}\1\d{1,2}/;
let str3='2016-03-26';
let str33='2016-03.26';
console.log(regExp3.test(str3));// true
console.log(regExp3.test(str33));// false

console.log('.............');
/**
 * 分组命名 
 */

// ES2018 之前的分组是通过数字命名的：
const regExp4= /(\d{4})-(\d{2})-(\d{2})/u;
const result =regExp4.exec('2018-10-25');
console.log(result[0]); // 2018-10-25
console.log(result[1]); // 2018
console.log(result[2]); // 10
console.log(result[3]); // 25

// ES2018:
let regExp44 = /(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})/u;
let str4='2018-10-25'
const result4 = regExp44.exec(str4);
console.log(result4.groups.year);  //2018
console.log(result4.groups.month); //10
console.log(result4.groups.day); //25

console.log(str4.replace(regExp44,'$<month>-$<day>-$<year>'));// 10-25-2018

console.log('.............');
/**
 * 忽略分组 
 */

let str5='tom.ok-tom.ok';
let regExp5=/(?:tom)\.(ok)/g;
console.log(str5.replace(regExp5,'$1'));// ok-ok

