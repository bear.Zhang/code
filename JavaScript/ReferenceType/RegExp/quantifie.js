// 量词 

/**
 * {n} 代表待操作的字符串内必须最少含有连续n个正则表达式里的字符串
 */
let str1='heeel';
let str11='heel';
let str111='heeeel';
let regExp1=/e{3}/g;
console.log(regExp1.test(str1));// true
console.log(regExp1.test(str11));// false
console.log(regExp1.test(str111));// true

console.log('{n,m}');
/**
 * {n,m} 代表前面的字符最少有一个，最多有3个
 * 使用test()时优先考虑n，只要n满足了就返回true，如果不满足n则返回false；
 * 使用replace()时优先考虑n，再考虑m，如果不满足n则返回原字符串；
 */
let str2='heeeel';
let str22='hel';
let regExp2=/e{2,3}/g;
console.log(regExp2.test(str2));// true
console.log(regExp2.test(str22));// false
console.log(str22.replace(regExp2,'*'));// hel
console.log(str2.replace(regExp2,'*'));// h*el

console.log('n*');
/**
 * n* 匹配零个或多个n；
 */
let str3='hell';
let str33='hll';
let str333='heeell';
let regExp3=/e*/g;
let regExp33=/e2*/g;
console.log(regExp3.test(str3));// true
console.log(str3.replace(regExp3,'*'));// *h**l*l*
console.log(regExp33.test(str3));// true
console.log(regExp33.test(str33));// false
console.log(str33.replace(regExp3,'*'));// *h*l*l*

console.log('n?');
/**
 * / n? 匹配零个或多个n
 */
let str4='hell';
let regExp4=/e4*/g;
console.log(regExp4.test(str4));
console.log(str4.replace(regExp4,'*'));
