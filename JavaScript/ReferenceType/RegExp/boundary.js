// 边界

/**
 * ^ 以xx开始，在类 []中表示非
 */

 let regExp1=/^1/g;
 let str1='12334';
 let str11='2334';
 console.log(regExp1.test(str1));// true
 console.log(regExp1.test(str11));// false

/**
 * $以xx结束
 */

 let regExp2=/1$/g;
 let str2='23341';
 let str22='12334';
 console.log(regExp2.test(str2));// true
 console.log(regExp2.test(str22));// false

/**
 * \b 单词边界
 */

let regExp3=/er\b/g;
let str3='never';
let str33='serve';
console.log(regExp3.test(str3));// true
console.log(regExp3.test(str33));// false

/**
 * \B 非单词边界
 */

let regExp4=/er\B/g;
let str4='serve';
let str44='never';
console.log(regExp4.test(str4));// true
console.log(regExp4.test(str44));// false

// 6~16字符只允许使用字母和数字
let regExp5=/^[a-zA-Z0-9]{6,16}$/g;
console.log(regExp5.test('11111111111111111'));

// 6~16字符不允许使用汉字
let regExp6=/^[^\(\u4E00-\u9FA5)]{2,16}$/;
console.log(regExp6.test('12323撒大声地'));

let regExp6=/^[a-zA-Z0-9\(`~!#@$%^&*()-_+=.?,<>{})]{6,16}$/g;
console.log(regExp6.test('123243.()'));


