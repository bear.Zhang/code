var arr=[1,2,6,4,5];

// Math.max() 与扩展运算符（...）
var maxValue=Math.max(...arr);
console.log(maxValue);//6

// Math.max() 与apply
var maxVal=Math.max.apply(null,arr);
console.log(maxVal);//6

// Math.max()、call、(...)
var maxVal1=Math.max.call(...arr);
console.log(maxVal1);//6