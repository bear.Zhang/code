/**
 * 数组去重
 */
var fruits = ["banana", "apple", "orange", "watermelon", "apple", "orange", "grape", "apple"];

// 使用Array.from+Set()
var uniqueFruits = Array.from(new Set(fruits));
console.log(uniqueFruits);// ["banana", "apple", "orange", "watermelon", "grape"]

// 使用spread+Set()
var uniqueFruits2 = [...new Set(fruits)];
console.log(uniqueFruits2);// ["banana", "apple", "orange", "watermelon", "grape"]