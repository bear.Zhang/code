/**
 * 转换
 */
// 数组
var colors=["red","blue","green"];
console.log(colors.join());// red,blue,green
console.log(colors.join(''));// redbluegreen
console.log(colors.join(' '));// red blue green
console.log(colors.join('+'));// red+blue+green

// 类数组
function fn(a, b, c) {
    var s = Array.prototype.join.call(arguments);
    console.log(s); // '1,a,true'
}
fn(1, 'a', true);

// 分隔符为undefined
var a = [1,2,3];
console.log(a.join(undefined));

// 数组中存在undefined、null
var colors = [1,undefined,2,null,3];
console.log(colors.join());//'1,,2,,3'

// 创建重复某些字符N次的函数
function repeatString(str,n){
    return new Array(n+1).join(str);
}
console.log(repeatString('a',3));//'aaa'
console.log(repeatString('Hi',5));//'HiHiHiHiHi'
