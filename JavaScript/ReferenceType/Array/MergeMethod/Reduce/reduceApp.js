/**
 * 数组求和
 */
let arr=[1,2,3];
let sum=arr.reduce((acc,cur)=>{
    return acc+cur;
},0);
console.log(sum);// 6

/**
 * 数组去重
 */
let arrDedup=[1,2,3,3,2,1,4];
let arrDeduplication=arrDedup.reduce((acc,cur)=>{
  if(!acc.includes(cur)){
    acc.push(cur);
  }
  return acc;
},[]);
console.log(arrDeduplication);// [1,2,3,4]

/**
 * 求数组最大值
 */
let arrM=[1, 2, 3, 4],
    arrMax=arrM.reduce((acc,cur)=>{
      return Math.max(acc,cur);
    });
console.log(arrMax);

/**
 * 将二维数组降为一维
 */
let arrDi=[[1,2], [3,4], [5,6]],arrDimension=arrDi.reduce((acc,cur)=>{
  return acc.concat(cur);
},[]);
console.log(arrDimension);// [1,2,3,4,5,6]

/**
 * 对象属性求和
 */
let arrO = [
  {subject: 'Math', score: 90},
  {subject: 'Chinese', score: 90},
  {subject: 'English', score: 100}
],arrObj=arrO.reduce((acc,cur)=>{
  return cur.score+acc;
},0);
console.log(arrObj);

/**
 * 算数组中每个元素出现的个数
 */
let arrN=[1, 2,3,3,2,1,2,1],
  arrNum=arrN.reduce((acc,cur)=>{
    if(!(cur in acc)){
      acc[cur]=1;
    }else{
      acc[cur]=acc[cur]+1;
    }
    return acc;
  },{});
  console.log(arrNum);// { '1': 3, '2': 3, '3': 2 }

/**
 * 数组分类
 */
let arrC=[
  {subject: 'Math', score: 90},
  {subject: 'Chinese', score: 90},
  {subject: 'English', score: 100},
  {subject: 'Math', score: 80},
  {subject: 'Chinese', score: 95}
],arrClass=arrC.reduce((acc,cur)=>{
  if(!(cur.subject in acc)){
    acc[cur.subject]=[];
  }
  acc[cur.subject].push(cur)
  return acc;
},{});
console.log(arrClass);

/**
 * 实现Map
 */
let arrM=[
  {subject: 'Math', score: 90},
  {subject: 'Chinese', score: 90},
  {subject: 'English', score: 100},
  {subject: 'Math', score: 80},
  {subject: 'Chinese', score: 95}
];
Array.prototype.reduceMap=function(callback){
  return this.reduce((acc,cur,index,arr)=>{
    const item=callback(cur,index,arr);
    acc.push(item);
    return acc;
  },[])
}
let arrMap=arrM.reduceMap((item,index)=>{
  return item.score+index;
})
console.log(arrMap);

/**
 * 实现forEach
 */
let arrF=[
  {subject: 'Math', score: 90},
  {subject: 'Chinese', score: 90},
  {subject: 'English', score: 100},
  {subject: 'Math', score: 80},
  {subject: 'Chinese', score: 95}
];
Array.prototype.reduceForEach=function(callback){
  return this.reduce((acc,cur,index,arr)=>{
    callback(cur,index,arr);
    return acc;
  },undefined)
}
let arrForEach=arrF.reduceForEach((item,index)=>{
  console.log(item.score+index);
})
console.log(arrForEach);

/**
 * 实现filter
 */
 let arrFi=[
  {subject: 'Math', score: 90},
  {subject: 'Chinese', score: 90},
  {subject: 'English', score: 100},
  {subject: 'Math', score: 80},
  {subject: 'Chinese', score: 95}
];
Array.prototype.reduceFilter=function(callback){
  return this.reduce((acc,cur,index,arr)=>{
    let isTrue=callback(cur,index,arr);
    if(isTrue){
      acc.push(cur);
    }
    return acc;
  },[])
}
let arrFilter=arrFi.reduceFilter((item)=>{
  return item.score>85;
})
console.log(arrFilter);

/**
 * 实现find
 */
 let arrFin=[
  {subject: 'Math', score: 90},
  {subject: 'Chinese', score: 90},
  {subject: 'English', score: 100},
  {subject: 'Math', score: 80},
  {subject: 'Chinese', score: 95}
];
Array.prototype.reduceFind=function(callback){
  return this.reduce((acc,cur,index,arr)=>{
    if(callback(cur,index,arr)){
      if(acc instanceof Array&&acc.length==0){
        acc=cur
      }
    }
    if((index==arr.length-1)&&(acc instanceof Array)&&acc.length==0){
      acc=undefined;
    }
    return acc;
  },[])
}
let arrFind=arrFin.reduceFind((item)=>{
  return item.score>85;
})
console.log(arrFind);

/**
 * 组合多个数组方法
 */
var wizards = [
    {
      name: 'Harry Potter',
      house: 'Gryfindor'
    },
    {
      name: 'Cedric Diggory',
      house: 'Hufflepuff'
    },
    {
      name: 'Tonks',
      house: 'Hufflepuff'
    },
    {
      name: 'Ronald Weasley',
      house: 'Gryfindor'
    },
    {
      name: 'Hermione Granger',
      house: 'Gryfindor'
    }
];
let combinationArr=wizards.reduce((acc,cur)=>{
    if(cur.house==='Hufflepuff'){
        acc.push(cur.name);
    };
    return acc;
},[]);
console.log(combinationArr);// [ 'Cedric Diggory', 'Tonks' ]

/**
 * 从数组生成 HTML 标签
 */
var hufflepuffList = '<ul>' + wizards.reduce(function (html, wizard) {
    if (wizard.house === 'Hufflepuff') {
      html += '<li>' + wizard.name + '</li>';
    }
    return html;
  }, '') + '</ul>';
console.log(hufflepuffList);// <ul><li>Cedric Diggory</li><li>Tonks</li></ul>

// 数组元素分组
var groupBy = function (arr, criteria) {
    return arr.reduce(function (obj, item) {
  
      // 判断criteria是函数还是属性名
      var key = typeof criteria === 'function' ? criteria(item) : item[criteria];
      // 如果属性不存在，则创建一个
      if (!obj.hasOwnProperty(key)) {
        obj[key] = [];
      }
  
      // 将元素加入数组
      obj[key].push(item);
  
      // 返回这个对象
      return obj;
  
    }, {});
};

console.log(groupBy(['one', 'two', 'three'],'length'));// { '3': [ 'one', 'two' ], '5': [ 'three' ] }

// 合并数据到单个数组
var points = {
    HarryPotter: 500,
    CedricDiggory: 750,
    RonaldWeasley: 100,
    HermioneGranger: 1270
};
var wizardsWithPoints = wizards.reduce(function (arr, wizard) {

    // 移除巫师名字中的空格，用来获取对应的 points
    var key = wizard.name.replace(' ', '');
  
    // 如果wizard有points，则加上它，否则设置为0
    if (points[key]) {
      wizard.points = points[key];
    } else {
      wizard.points = 0;
    }
  
    // 把wizard对象加入到新数组里
    arr.push(wizard);
  
    // 返回这个数组
    return arr;
  
}, []);
console.log(wizardsWithPoints);
// [ { name: 'Harry Potter', house: 'Gryfindor', points: 500 },
// { name: 'Cedric Diggory', house: 'Hufflepuff', points: 750 },
// { name: 'Tonks', house: 'Hufflepuff', points: 0 },
// { name: 'Ronald Weasley', house: 'Gryfindor', points: 100 },
// { name: 'Hermione Granger', house: 'Gryfindor', points: 1270 } ]

// 合并数据到单个对象
var wizardsAsAnObject = wizards.reduce(function (obj, wizard) {

    // 移除巫师名字中的空格，用来获取对应的 points
    var key = wizard.name.replace(' ', '');
  
    // 如果wizard有points，则加上它，否则设置为0
    if (points[key]) {
      wizard.points = points[key];
    } else {
      wizard.points = 0;
    }
  
    // 删除 name 属性
    delete wizard.name;
  
    // 把 wizard 数据添加到新对象中
    obj[key] = wizard;
  
    // 返回该对象
    return obj;
  
  }, {});
  console.log(wizardsAsAnObject);
//   { HarryPotter: { house: 'Gryfindor', points: 500 },
//   CedricDiggory: { house: 'Hufflepuff', points: 750 },
//   Tonks: { house: 'Hufflepuff', points: 0 },
//   RonaldWeasley: { house: 'Gryfindor', points: 100 },
//   HermioneGranger: { house: 'Gryfindor', points: 1270 } }