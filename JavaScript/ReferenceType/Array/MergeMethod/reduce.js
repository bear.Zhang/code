var arr=[1,2,3,4,5];

arr.reduce((previousVal,nowVal,index,arrObj)=>{
    console.log('上一值：',previousVal);
    console.log('当前值：',nowVal);
    console.log('项索引：',index);
    console.log('数组对象：',arrObj);
    return nowVal;
})

arr.reduce((previousVal,nowVal,index,arrObj)=>{
    console.log('上一值：',previousVal);
    console.log('当前值：',nowVal);
    console.log('项索引：',index);
    console.log('数组对象：',arrObj);
    return nowVal;
},0)

console.log('..............................')

arr.reduceRight((previousVal,nowVal,index,arrObj)=>{
    console.log('上一值：',previousVal);
    console.log('当前值：',nowVal);
    console.log('项索引：',index);
    console.log('数组对象：',arrObj);
    return nowVal;
})

