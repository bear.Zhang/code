var arr=[];

/*instanceof*/
console.log(arr instanceof Array);//true

/*isArray*/
console.log(Array.isArray(arr));//true

/*toString()*/
console.log(Object.prototype.toString.call(arr)==='[object Array]');//true

console.log(Object.prototype.toString.call(arr).indexOf('Array'));//8

console.log(Object.prototype.toString.call(arr).indexOf('Array')!==-1);//true

/*isPrototypeOf()*/
console.log(Array.prototype.isPrototypeOf(arr));//true

/*constructor*/
console.log(arr.constructor);//[Function: Array]
console.log(arr.constructor.toString());//function Array() { [native code] }
console.log(arr.constructor.toString().indexOf('Array')!==-1);//true