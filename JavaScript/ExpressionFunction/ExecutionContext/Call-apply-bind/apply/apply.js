// 模拟实现apply
Function.prototype.myApply=function(content){
    var content=content||window;
    content.fn=this;
    var arg=[...arguments].slice(1);
    var reslut;
    if(arg[0]){
        reslut=content.fn(...arg[0]);
    }else{
        reslut=content.fn()
    }
    delete reslut;
    return reslut;
}

var foo={
    value:1
}

function boo(name,age){
    console.log(name);
    console.log(age);
    console.log(this.value);
}

boo.myApply(foo,[3,4]);// 3 4 1