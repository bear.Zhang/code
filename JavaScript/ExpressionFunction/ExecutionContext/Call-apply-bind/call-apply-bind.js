// bind、call、apply 区别

let a = {
    value: 1
}
function getValue(name, age) {
    console.log(name)
    console.log(age)
    console.log(this.value)
}

getValue.call(a, 'yck', '24')//yck 24 1

getValue.apply(a, ['yck', '24'])//yck 24 1

var bin=getValue.bind(a,'yck', '24');

bin()//yck 24 1
