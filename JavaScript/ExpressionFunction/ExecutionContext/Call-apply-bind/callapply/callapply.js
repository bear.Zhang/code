/*合并两个数组*/

var vegetables = ['parsnip', 'potato'];
var moreVegs = ['celery', 'beetroot'];

// 将第二个数组融合进第一个数组
// 相当于 vegetables.push('celery', 'beetroot');
Array.prototype.push.apply(vegetables, moreVegs);

console.log(vegetables);//[ 'parsnip', 'potato', 'celery', 'beetroot' ]

/*处理当数组过大时的方法*/

function concatOfArray(arr1, arr2) {
    var QUANTUM = 32768;
    for (var i = 0, len = arr2.length; i < len; i += QUANTUM) {
        Array.prototype.push.apply(
            arr1, 
            arr2.slice(i, Math.min(i + QUANTUM, len) )
        );
    }
    return arr1;
}

// 验证代码
var arr1 = [-3, -2, -1];
var arr2 = [];
for(var i = 0; i < 1000000; i++) {
    arr2.push(i);
}
// Array.prototype.push.apply(arr1, arr2);

// console.log(arr1);// Uncaught RangeError: Maximum call stack size exceeded

// console.log(concatOfArray(arr1, arr2));//(1000003) [-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ...]

var numbers = [5, 458 , 120 , -215 ]; 

console.log(Math.max.apply(Math, numbers));   //458    
console.log(Math.max.call(Math, 5, 458 , 120 , -215)); //458

// ES6
console.log(Math.max.call(Math, ...numbers)); // 458