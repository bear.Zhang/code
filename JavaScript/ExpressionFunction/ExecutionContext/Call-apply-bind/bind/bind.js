// 模拟实现bind
Function.prototype.myBind = function (context) {
    if (typeof this !== 'function') {
      throw new TypeError('Error')
    }
    var _this = this
    var args = [...arguments].slice(1)
    return function returnFn() {
      if (this instanceof returnFn) {
        return new _this(...args, ...arguments)
      }
      return _this.apply(context, args.concat(...arguments))
    }
}

var foo={
  value:1
}

var value=2;

function boo(name,age){
  console.log(name);
  console.log(age);
  console.log(this.value);
}

boo.myBind(foo,3,4)();//3 4 1