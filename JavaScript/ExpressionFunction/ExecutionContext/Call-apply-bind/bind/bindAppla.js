var value = 2;
var foo = {
    value: 1
};
function bar(name, age) {
    return {
		value: this.value,
		name: name,
		age: age
    }
};

console.log(bar.call(foo, "Jack", 20)); // 直接执行了函数 输出: {value: 1, name: "Jack", age: 20}

console.log(bar.bind(foo, "Jack", 20))// 返回一个函数 输出: [Function: bound bar]
var bindFoo1 = bar.bind(foo, "Jack", 20); 
console.log(bindFoo1());//{ value: 1, name: 'Jack', age: 20 }

var bindFoo2 = bar.bind(foo, "Jack"); // 返回一个函数
console.log(bindFoo2(20));// {value: 1, name: "Jack", age: 20}

var nickname = "Kitty";

function Person(name){
    this.nickname = name;
    this.distractedGreeting = function() {
        var self=this;
        setTimeout(function(){
            console.log("Hello, my name is " + self.nickname);
        }, 500);
    }
}
 
var person = new Person('jawil');
person.distractedGreeting();//Hello, my name is Kitty Node环境中 Hello, my name is undefined

var add = function(x) {
    return function(y) {
      return x + y;
    };
  };
  
  var increment = add(1);
  var addTen = add(10);
  
console.log(increment(2));// 3
  
console.log(addTen(2));// 12
  
console.log(add(1)(2));// 3