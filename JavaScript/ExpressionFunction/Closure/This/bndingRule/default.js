/*默认绑定*/

// 运行在严格模式下，this会绑定到undefined
function strictDefault() {
    "use strict";
    console.log(this.a);
}
var a = 2;
strictDefault(); // TypeError: Cannot read property 'a' of undefined

// 严格模式下调用函数则不影响默认绑定
function unstrictDefault() {
    console.log(this.b);
}
var b=2;

(function() { 
    "use strict";
    unstrictDefault(); //2
})();