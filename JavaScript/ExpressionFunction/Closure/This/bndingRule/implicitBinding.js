/*隐式绑定*/

function implicitBinding() {
    console.log(this.a);
}

var obj = {
    a: 2,
    implicitBinding:implicitBinding
};

obj.implicitBinding(); // 2

// 虽然bar是obj.foo的一个引用，但是实际上，它引用的是foo函数本身。
// bar()是一个不带任何修饰的函数调用，应用默认绑定。
function lose() {
    console.log(this.a);
}

var objLose = {
    a: 2,
    lose:lose
};

var loseFn = objLose.lose; // 函数别名

var a = "oops, global"; // a是全局对象的属性

loseFn(); // "oops, global"

function foo() {
    console.log( this.a );
}

function doFoo(fn) {
    // fn其实引用的是foo
    
    fn(); // <-- 调用位置！
}

var obj = {
    a: 2,
    foo: foo
};

var a = "oops, global"; // a是全局对象的属性

doFoo( obj.foo ); // "oops, global"