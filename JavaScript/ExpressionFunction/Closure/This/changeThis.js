// fn是全局函数，没有直接调用它的对象，也没有使用严格模式，this指向window
function fn() { 
    console.log(this);// window 
}  
fn();

// fn是全局函数，没有直接调用它的对象，但指定了严格模式（'use strict'），this指向undefined
function fn() { 
    'use strict';
    console.log(this);// undefined
}  
fn();

// fn直接调用者是obj，第一个this指向obj，setTimeout里匿名函数没有直接调用者，this指向window
const obj = {
    num: 10,
    fn: function () {
        console.log(this);// obj
        setTimeout(function (){
            console.log(this);// window
        });
    }    
}
obj.fn();

// fn直接调用者是obj，第一个this指向obj，setTimeout箭头函数，this指向最近的函数的this指向，即也是obj
const obj = {
    num: 10,
    fn: function () {
        console.log(this);// obj
        setTimeout(() => {
            console.log(this);// obj
        });
    }    
}
obj.fn();

// 函数自调用时，this指向window。
const fn=function(){
    console.log(this);
}()

function Obj(){
    console.log(this);//Obj
}
let obj=new Obj();

var object={
    name:"myobject",
    getName:function(){
        console.log(this.name)
        return function(){
            return this.name;
        }();
    }()
};
console.log(object.getName);//the window

var name="the window";
var obj={
    name:"myobject",
    getName:()=>{
        console.log(this)//window
        return ()=>{
            return this.name;
        };
    }
};
console.log(obj.getName()());//the window
