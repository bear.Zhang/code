// 闭包与变量

// 闭包只能取得包含函数中任何变量的最后一个值。
function createFunctions(){
    var result=new Array();
    for(var i=0;i<10;i++){
        result[i]=function(){
            return i;
        };
    }
    return result;
};
for(let i=0;i<createFunctions().length;i++){
    console.log(createFunctions()[i]());
}
// 10 10 10 10 10 10 10 10 10 10 

function createFunction(){
    var result=new Array();
    for(var i=0;i<10;i++){
        result[i]=function(num){
            return num;
        }(i);
    }
    return result;
};

console.log(createFunction());//[ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]