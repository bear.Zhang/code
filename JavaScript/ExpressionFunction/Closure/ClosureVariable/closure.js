/*闭包：调用外部函数返回内部函数，内部函数引用了外部函数的变量，这些变量的集合就是外部函数的闭包*/ 
function getGreet() {
  const name = 'vian'
  var innerFunc = function() {
    console.log(name);
    console.log(this)//全局对象
  }
  return innerFunc
}
let greet = getGreet();
greet(); // vian

/*闭包可以访问当前函数以外的变量*/
function getOuter() {
  let date = '815';
  function getDate(str) {
    console.log(str + date);
  }
  return getDate('今天是：');
}
getOuter(); //"今天是：815"

/*即使外部函数已经返回，闭包仍能访问外部函数定义的变量*/
function getOuterFn() {
  var date = '815';
  function getDate(str) {
    console.log(str + date);
  }
  return getDate;
}
var today = getOuterFn();

today('今天是：'); //"今天是：815"
today('明天不是：'); //"明天不是：815"

/*闭包可以更新外部变量的值*/
function updateCount() {
  var count = 0;
  function getCount(val) {
    count = val;
    console.log(count);
  }
  return getCount;
}
var count = updateCount();
count(815); //815
count(816); //816
