// 递归实现

// 求阶乘
function factorial(n){
    if(n<=1){
        return 1;
    }else{
        return n*factorial(n-1);
    }
}

console.log(factorial(5));

// n个台阶一次只能跳一个或者两个有几种跳法
function steps(n,i,j){
    if(n==1){
        return 1;
    }
    if(n==2){
        return 2;
    }
    return steps(n-1)+steps(n-2);
}

console.log(steps(3));

// 求和
function sum(n){
    if(n==1){
        return 1;
    }else{
        return n+sum(n-1);
    }
}
console.log(sum(5));

// 字符串中是否有相同的内容
function Judge(str){
    if(str.length<=1){
        return false;
    }else{
        let temp=str.charAt(str.length-1),
            tempStr=str.slice(0,str.length-1);
        if(tempStr.indexOf(temp)==-1){
            return Judge(tempStr);
        }else{
            return true;
        }
    }
}

console.log(Judge('abc'));//false
console.log(Judge('abca'));//true

// 河内塔问题
function hanio(n,arr1,arr2,arr3){
    if(n==1){
        arr3[0]=arr1[0];
        return arr3;
    }else{
        hanio(n-1,arr1,arr2,arr3);
        hanio(n-1,arr1,arr2,arr3);
    }
}
var arr1=[1];
var arr2=[];
var arr3=[];
console.log(hanio(1,arr1,arr2,arr3));
