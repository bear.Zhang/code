// 参数

/**
 * replacer 作为函数时 
 */
const parameter2 = {
    a: "aaa",
    b: undefined,
    c: Symbol("dd"),
    fn: function() {
        return true;
    }
};

// 不用 replacer 参数时
console.log(JSON.stringify(parameter2));// {"a":"aaa"}
// 使用 replacer 参数作为函数时
let stringifyParameter2=JSON.stringify(parameter2, (key, value) => {
    switch (true) {
        case typeof value === "undefined":
            return "undefined";
        case typeof value === "symbol":
            return value.toString();
        case typeof value === "function":
            return value.toString();
        default:
        break;
    }
    return value;
});

console.log(stringifyParameter2)// {"a":"aaa","b":"undefined","c":"Symbol(dd)","fn":"function() {\r\n        return true;\r\n    }"}

console.log('传入 replacer 函数的第一个参数：')
//传入 replacer 函数的第一个参数
const data = {
    a: 2,
    b: 3,
    c: 4,
    d: 5
};

JSON.stringify(data, (key, value) => {
    console.log(value);
    return value;
});

//{ a: 2, b: 3, c: 4, d: 5 }
// 2 3 4 5

console.log('实现 map 函数:')
//实现 map 函数
const mapData = {
    a: 2,
    b: 3,
    c: 4,
    d: 5
};
const objMap = (obj, fn) => {
    if (typeof fn !== "function") {
        throw new TypeError(`${fn} is not a function !`);
    }
    return JSON.parse(JSON.stringify(obj, fn));
};
let result=objMap(mapData, (key, value) => {
    if (value % 2 === 0) {
        return value / 2;
    }
    return value;
});
console.log(result);// { a: 1, b: 3, c: 2, d: 5 }

console.log('replacer 作为数组时:');
/**
 * replacer 作为数组时，结果非常简单，数组的值就代表了将被序列化成 JSON 字符串的属性名。
 */
const jsonObj = {
    name: "JSON.stringify",
    params: "obj,replacer,space"
};
    
// 只保留 params 属性的值
let arrReplacer=JSON.stringify(jsonObj, ["params"])
console.log(arrReplacer);//{"params":"obj,replacer,space"}

console.log('space参数：');
const parameter3 = {
    name: "弹铁蛋同学",
    describe: "今天在学 JSON.stringify()",
    emotion: "like shit"
};
console.log(JSON.stringify(parameter3, null, "🐷"));

console.log(JSON.stringify(parameter3, null, 2));