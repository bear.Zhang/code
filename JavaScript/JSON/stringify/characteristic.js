// 特性

/**
 * 对于 undefined、任意的函数以及 symbol 三个特殊的值分别作为对象属性的值、数组元素、单独的值时 JSON.stringify()将返回不同的结果。
 */

// undefined、任意的函数以及 symbol 作为对象属性值时 JSON.stringify() 将跳过（忽略）对它们进行序列化
let characteristic1={
    a: "aaa",
    b: undefined,
    c: Symbol("cc"),
    fn: function() {
        return true;
    }
};

let stringifyData=JSON.stringify(characteristic1);

console.log(stringifyData); //{"a":"aaa"}

//undefined、任意的函数以及 symbol 作为数组元素值时，JSON.stringify() 会将它们序列化为 null
let arr=["aaa", undefined, function fn() {
    return true
    }, Symbol('arr')];
let stringifyArr=JSON.stringify(arr);
console.log(stringifyArr); //["aaa",null,null,null]

// undefined、任意的函数以及 symbol 被 JSON.stringify() 作为单独的值进行序列化时都会返回 undefined
console.log(JSON.stringify(function a (){console.log('a')}));// undefined
console.log(JSON.stringify(undefined));// undefined
console.log(JSON.stringify(Symbol('dd')));// undefined

console.log('特性2：');
/**
 * 非数组对象的属性不能保证以特定的顺序出现在序列化后的字符串中。
 */

let characteristic2 = {
    a: "aaa",
    b: undefined,
    c: Symbol("dd"),
    fn: function() {
        return true;
    },
    d: "ddd"
};
console.log(JSON.stringify(characteristic2)); // "{"a":"aaa","d":"ddd"}"
console.log(JSON.stringify(["aaa", undefined, function aa() {
    return true
    }, Symbol('dd'),"eee"])) ;// "["aaa",null,null,null,"eee"]"

console.log('特性3：');
/**
 * 转换值如果有 toJSON() 函数，该函数返回什么值，序列化结果就是什么值，并且忽略其他属性的值。
 */

 let characteristic3=JSON.stringify({
    say: "hello JSON.stringify",
    toJSON: function() {
        return "return toJSON val";
    }
});
console.log(characteristic3);// "return toJSON val"

console.log('特性4：');
/**
 * JSON.stringify() 将会正常序列化 Date 的值
 */

 let characteristic4=JSON.stringify({ now: new Date() });
console.log(characteristic4);// {"now":"2019-12-27T07:13:57.711Z"}

console.log('特性5：');
/**
 * NaN 和 Infinity 格式的数值及 null 都会被当做 null
 */

let characteristic5=JSON.stringify(NaN);
let characteristic55=JSON.stringify(null);
let characteristic555=JSON.stringify(Infinity);
console.log(characteristic5,characteristic55,characteristic555);// null null null

console.log('特性6：');
/**
 * 布尔值、数字、字符串的包装对象在序列化过程中会自动转换成对应的原始值。
 */
let characteristic6=JSON.stringify([new Number(1), new String("false"), new Boolean(false)]);
console.log(characteristic6);// [1,"false",false]

console.log('特性7：');
/**
 * 关于对象属性的是否可枚举，其他类型的对象，包括 Map/Set/WeakMap/WeakSet，仅会序列化可枚举的属性。
 */
let characteristic7=JSON.stringify(
    Object.create(
        null,
        {
            x: { value: 'json', enumerable: false },
            y: { value: 'stringify', enumerable: true }
    }));
console.log(characteristic7);// {"y":"stringify"}

console.log('特性8：');
/**
 * JSON.parse(JSON.stringify())，这个方式实现深拷贝会因为序列化的诸多特性从而导致诸多的坑。
 */

 // 对包含循环引用的对象（对象之间相互引用，形成无限循环）执行此方法，会抛出错误。
const obj = {
    name: "loopObj"
};
const loopObj = {
    obj
};

// 对象之间形成循环引用，形成闭环
obj.loopObj = loopObj;
function deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
}
//deepClone(obj); //浏览器：Uncaught TypeError: Converting circular structure to JSON Node：TypeError: Converting circular structure to JSON

console.log('特性9：');
/**
 * 所有以 symbol 为属性键的属性都会被完全忽略掉，即便 replacer 参数中强制指定包含了它们。
 */
let characteristic9=JSON.stringify({ [Symbol.for("json")]: "stringify" }, function(k, v) {
    if (typeof k === "symbol") {
        return v;
    }
});
console.log(characteristic9);//undefined



