// 继承

function Student(name,age,subject){
    this.name=name;
    this.age=age;
    this.subject=subject;
    this.Studys=function(){
        console.log(this.subject);
    }
    
}

Student.prototype.Study=function(){
    console.log('我在学习',this.subject);
}

function Pupil(name,age,subject,school){
    Student.call(this,name,age,subject);
    this.school=school;
}

var p=new Pupil('小张',8,'大数据','清华');
p.Studys();
