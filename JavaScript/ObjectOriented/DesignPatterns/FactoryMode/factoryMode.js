/*工厂模式*/ 

//ES5 创建工厂模式
function creatPerson(name,age){
    var obj=new Object();
        obj.name=name;
        obj.age=age;
        obj.showName=function(){
            console.log("名字"+this.name+"年龄"+this.age);
        };
        return obj;
};
var obj=creatPerson("zhang","22");//名字zhang年龄22
var obj1=creatPerson("yan","21");//名字yan年龄21
    obj.showName();
    obj1.showName();

console.log(obj.showName==obj1.showName);//false

// ES6创建工厂模式
class Product{
    constructor(name){
        this.name=name;
    };
    init(){
        console.log('初始化产品！');//初始化产品！
        console.log(this.name);//p1
    }
}

class Factory{
    create (name) {
        return new Product(name);
    }
}

let f=new Factory();
let p=f.create('p1');
p.init();

