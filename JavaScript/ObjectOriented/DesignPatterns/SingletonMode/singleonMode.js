// 单例模式

class Modal{
    login(){
        console.log('login...');
    }
}

Modal.create=(function(){
    let instance;
    return function(){
        if(!instance){
            instance=new Modal();
        }
        return instance;
    }
})();

let m1=Modal.create();
let m2=Modal.create();
console.log(m1===m2);//true
m1.login()//login...
m2.login()//login...

console.log('................................')

// “透明版”单例模式
let Modal1 = (function(){
    let instance;
    return function(name) {
        if (instance) {
            return instance;
        }
        this.name = name;
        return instance = this;
    }
})();

Modal1.prototype.getName = function() {
    return this.name
}

let question = new Modal1('问题框');
let answer = new Modal1('回答框');

console.log(question === answer); // true
console.log(question.getName());  // '问题框'
console.log(answer.getName());  // '问题框'

