// 模拟实现一个new操作符：使用Object.create()进行链接原型

function myNew(func){
    let obj=null;
    if(func.prototype!==null){
        obj=Object.create(func.prototype);
    }
    var result=func.apply(obj,Array.from(arguments).slice(1));
    if((typeof result ==='object'|| typeof result ==='function')&&result!==null){
        return result;
    }
    return obj;
}

function fn(a,b){
    this.name='实现一个new!';
    this.fa=a;
    this.fb=b;
    this.getName=function(){
        console.log(this.name);
        console.log(this.fa);
        console.log(this.fb);
    }
}

var obj=myNew(fn,1,2);
obj.getName()
