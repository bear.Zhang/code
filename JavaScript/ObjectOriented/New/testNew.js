function testNew(func){
    if(func.prototype!==null){
        obj=Object(func.prototype);
    }
    let result=func.apply(obj,Array.prototype.slice.call(arguments,1));
    if((typeof result==='function'||typeof result==='object')&&result!==null){
        return result;
    }
    return obj;
}

function fn(a,b){
    this.name='实现一个new!';
    this.fa=a;
    this.fb=b;
    this.getName=function(){
        console.log(this.name);
        console.log(this.fa);
        console.log(this.fb);
    }
}

let ob=testNew(fn,1,2);
ob.getName();