// 多态

function Person(id,name){
    this.name=name;
    this.id=id;
}

Person.prototype.Study=function(){
    console.log('我在学习');
}

function Student(id,name,subject){
    Person.call(this,id,name);
    this.subject=subject;
    this.Study=function(){
        console.log(this.subject);
    }
}

var p1=new Student(1,'小张','大数据');
var p2=new Student(2,'小张','前端大数据');
p1.Study();//大数据
p2.Study();//前端大数据
