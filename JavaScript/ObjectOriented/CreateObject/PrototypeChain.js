/**
 * 原型、原型链
 */

function Person(){
}

console.log(Person.prototype);// 实例原型

let p=new Person();

console.log(p.__proto__==Person.prototype);// true

console.log(Person.prototype.constructor()==Person); // true

console.log(Person.prototype.__proto__==Object.prototype); // true

console.log(Object.prototype.constructor()==Object);// true

console.log(Object.prototype.__proto__==null); // true