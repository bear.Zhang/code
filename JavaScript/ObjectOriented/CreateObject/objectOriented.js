var arr = [1, 2, 3, 4];
arr.a = 12;
arr.show = function () {
    console.log(this);
    console.log(this.a);
};
arr.show();

function show1() {
    console.log(this);
}
show1();

var obj = new Object();
obj.name = "zhangYanXiong";
obj.age = "22";
obj.showName = function () {
    console.log("名字" + this.name + "年龄" + this.age);
};
obj.showName();