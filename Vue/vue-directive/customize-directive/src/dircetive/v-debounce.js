export default {
    inserted:(el,binding)=>{
        let timer=null;
        el.addEventListener('keyup',()=>{
            if(timer){
                clearTimeout(timer);
            }
            timer=setTimeout(()=>{
                binding.value();
            },1000)
        })
    }
}