import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// 全局指令
import copyDefinition from './dircetive/v-copy'
import debounceDefinition from './dircetive/v-debounce'
import waterMarkerDefinition from './dircetive/v-waterMarker'
Vue.directive('copy',copyDefinition);
Vue.directive('debounce',debounceDefinition);
Vue.directive('waterMarker',waterMarkerDefinition);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
