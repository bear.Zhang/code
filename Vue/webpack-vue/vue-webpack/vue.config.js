const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
// 树状可视化文件展示
const BundleAnalyzer=new BundleAnalyzerPlugin();
module.exports={
    configureWebpack:config =>{
        config.plugins.push(BundleAnalyzer);
        let optimizationSet={
            runtimeChunk: "single",
            splitChunks:{
                cacheGroups:{
                    vendors: {
                        // name选项来修改chunk-vendors.js文件的名字
                        name: `app-chunk-vendors`,
                        test: /[\\/]node_modules[\\/]/,
                        priority: -10,
                        chunks: 'initial'
                    },
                }
            }
        }
        Object.assign(config,{
            optimization:optimizationSet
        })
    }
}
