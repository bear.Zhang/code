# vue-tree

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## 说明
1、整个容器使用相对定位是为了避免在滚动中引起页面回流

2、phantom 容器为了撑开高度，让滚动条出现

3、flattenTree 为了拍平 递归结构的tree数据，同时添加level、expand、visibel属性，分别代表节点层级、是否展开、是否可视

4、contentHeight 动态计算容器的高度，隐藏（收起）节点不应该计算在总高度里面这样一来渲染大数据的tree组件就有了基本的雏形，接下来看看节点展开/收起如何实现