import Vue from 'vue'
import VueRouter from 'vue-router'
import CallDetail from '../views/callDetail/CallDetail.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'CallDetail',
    component: CallDetail
  },
  {
    path:'/callDetailT',
    name:'CallDetailT',
    component: () =>
    import( /*webpackChunkName: "home"*/ '@/views/callDetailT/CallDetailT.vue')
  },
  {
    path:'/callDetailS',
    name:'CallDetailTS',
    component: () =>
    import( /*webpackChunkName: "home"*/ '@/views/callDetailS/CallDetailS.vue')
  },
  {
    path:'/teamdetail',
    name:'TeamDetail',
    component: () =>
    import( /*webpackChunkName: "home"*/ '@/views/teamDetail/TeamDetail.vue')
  },
  {
    path:'/demo',
    name:'Demo',
    component: () =>
    import( /*webpackChunkName: "home"*/ '@/views/demo/CallDetail.vue')
  },
  {
    path:'/calldetail2',
    name:'CallDetail2',
    component: () =>
    import( /*webpackChunkName: "home"*/ '@/views/callDetail2/CallDetail2.vue')
  },
  {
    path:'/calldetail1',
    name:'CallDetail1',
    component: () =>
    import( /*webpackChunkName: "home"*/ '@/views/callDetail1/CallDetail1.vue')
  },
  {
    path:'/home',
    name:'Home',
    component: () =>
    import( /*webpackChunkName: "home"*/ '@/views/Home.vue')
  },
  {
    path:'/detail',
    name:'Detail',
    component: () =>
    import( /*webpackChunkName: "home"*/ '@/views/Home2.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
