const autoprefixer = require('autoprefixer');
const pxtoviewport = require('postcss-px-to-viewport');

// 判断环境
const isPord = process.env.NODE_ENV === 'production';

module.exports={
    css:{
        extract:isPord,
        loaderOptions: {
            postcss: {
                plugins: [
                    autoprefixer(),
                    pxtoviewport({
                        viewportWidth: 375
                    })
                ]
            }
        }
    }
}