import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import 'vant/lib/index.less';
import './style/adaptation.less'

import { initTheme } from "./theme";

let apps=['DingTalk','WeChat'],
userAgent=navigator.userAgent;
let isApp=apps.filter((app,index)=>{
  if(userAgent.indexOf(app)!==-1){
    return app;
  }
})
if(isApp[0]==null){
  initTheme('defaultTheme');
}else{
  initTheme(isApp[0]);
}
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
