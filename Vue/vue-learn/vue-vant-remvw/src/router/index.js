import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

import home from './modules/home'
import user from './modules/user'
import order from './modules/order'
import cart from './modules/cart'

  const routes = [
    ...home,
    ...user,
    ...order,
    ...cart
]

const router = new VueRouter({
  routes
})

export default router
