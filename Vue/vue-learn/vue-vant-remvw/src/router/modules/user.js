export default [
    {
        path: '/user',
        name: 'User',
        hidden: 'false',
        meta: {
            title: '我的'
        },
        component:
            () => import('@/views/User/User.vue'),
    },
    {
        path: '/user/integral',
        name: 'Integral',
        hidden: 'false',
        meta: {
            title: '我的积分'
        },
        component:
            () => import('@/views/User/Integral/Integral.vue'),
    }
]