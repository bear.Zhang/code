export default [
    {
        path: '/order',
        name: 'Order',
        hidden: 'false',
        meta: {
            title: '订单列表'
        },
        component:
            () => import('@/views/Order/Order.vue'),
    },
    {
        path: '/order/orderDetail',
        name: 'OrderDetail',
        hidden: 'false',
        meta: {
            title: '订单详情'
        },
        component:
            () => import('@/views/Order/OrderDetail.vue'),
    }
]