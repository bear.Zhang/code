export default [
    {
        path: '/goodslist',
        name: 'GoodsList',
        meta: {
            title: ''
        },
        component:
            () => import('@/views/Goods/GoodsList'),
    },
    {
        path: '/classify',
        name: 'Classify',
        meta: {
            title: ''
        },
        component:
            () => import('@/views/Goods/Classify'),
    },
]