export default [{
        path: '/cart',
        name: 'Cart',
        component: () => import('@/views/Cart/Index'),
        redirect: '/cart/shopList',
        children: [{
                path: '/cart/shopList',
                name: 'CartShopList',
                meta: {
                    title: '购物车'
                },
                component: () => import('@/views/Cart/Cart'),
            },
            {
                path: '/cart/confirmation',
                name: 'CartConfirmation',
                meta: {
                    title: '订单确认'
                },
                component: () => import('@/views/Cart/CartConfirmation'),
            }
        ]
    }

]