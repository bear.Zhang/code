import axios from 'axios'
import auth from './auth';
import { Message} from 'element-ui'
import router from '../router'

const server =axios.create({
    timeout:1500,
});

// 请求拦截器
server.interceptors.request.use(
    (config)=>{
        // 设置token
        if(auth.token){
            config.headers['Authorization'] = "Bearer "+auth.token
        }else{
            config.headers['Authorization'] = "Basic V0VCQVBQOldFQkFQUA=="
        }
        return config;
    },
    (error)=>{
        return Promise.reject(error);
});

// 响应拦截器
server.interceptors.response.use((response)=>{
    const res=response.data;
    if(res.code){
        if(res.code!=='000000'){
            Message({
                message: res.message,
                showClose: true,
                type: 'error',
              })
            return res;
        }else{
            return res;
        }
    }else{
        return res;
    }
},
(error)=>{
    const {status}=error.response;
    if(status==401){
        router.push('/Login');
    }
    return Promise.reject(error);
});

export default server;