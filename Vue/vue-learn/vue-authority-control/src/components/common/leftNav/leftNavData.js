const leftNavData = {
    "Home": [{
        path: '/AdminView',
        name: 'AdminView',
        meta: {
            title: '系统首页'
        },
        redirect: '/Index',
        children: [{
            path: '/Admin',
            name: 'Admin',
            meta: {
                title: '首页',
            },
        }]
    }],
    "Cafe": [{
        path: "/Cafe",
        name: "Cafe",
        redirect: "/Cafe",
        meta: {
            title: "商品管理",
        },
        children: [
            {
                path: "/Cafe/GoodsManage",
                name: "GoodsManage",
                hidden: false,
                meta: {
                    title: "商品管理",
                    auth: "GoodsManage"
                }
            },
            {
                path: "/Cafe/ClassifyManage",
                name: "ClassifyManage",
                hidden: false,
                meta: {
                    title: "分类管理",
                    auth: "ClassifyManage"
                }
            }
        ]
    },
    {
        path: "/Cafe/OrderManage",
        name: "OrderManage",
        redirect: "/Cafe/OrderManage",
        meta: {
            title: "订单管理",
        },
        children: [
            {
                path: "/Cafe/OrderManage",
                name: "OrderManage",
                hidden: false,
                meta: {
                    title: "订单管理",
                    auth: "OrderManage"
                }
            }
        ]
    },
    {
        path: "/Cafe/PlacardManage",
        name: "PlacardManage",
        redirect: "/Cafe/PlacardManage",
        meta: {
            title: "公告管理",
        },
        children: [
            {
                path: "/Cafe/PlacardManage",
                name: "PlacardManage",
                hidden: false,
                meta: {
                    title: "公告管理",
                    auth: "PlacardManage"
                }
            }
        ]
    }],
    "System": [{
        path: "/System",
        name: "System",
        redirect: "/System",
        meta: {
            title: "权限管理",
        },
        children: [
            {
                path: "/System/UserManage",
                name: "UserManage",
                hidden: false,
                meta: {
                    title: "用户管理",
                    auth: "UserManage"
                }
            },
            {
                path: "/System/PurviewManage",
                name: "PurviewManage",
                hidden: false,
                meta: {
                    title: "角色权限管理",
                    auth: "PurviewManage"
                }
            }
        ]
    }]
}

export default leftNavData;