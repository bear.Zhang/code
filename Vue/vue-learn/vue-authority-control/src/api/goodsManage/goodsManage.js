import request from '@/utils/request'
import api from '../index'

// 获取商品状态
export function getGoodsStatus(){
    return request({
        url: `/coffee-background/sku/status-count`,
        method: 'post',
    })
}

// 获取商品列表
export function getGoodsList(data){
    return request({
        url: `/coffee-background/sku/list`,
        method: 'post',
        data:data
    })
}

// 添加商品
export function addGoods(data){
    return request({
        url: `/coffee-background/sku`,
        method: 'post',
        data:data
    })
}

// 获取分类列表
export function getClassifyList(data){
    return request({
        url:`/coffee-background/category/list`,
        method:'post',
        data:data
    })
}

// 添加分类
export function addClassify(data){
    return request({
        url:`/coffee-background/category`,
        method:'post',
        data:data
    })
}

//分类向下调整 
export function descendingSort(id){
    return request({
        url:`/coffee-background/category/${id}/down`,
        method:'put'
    })
}

//分类向上调整 determine
export function ascendingSort(id){
    return request({
        url:`/coffee-background/category/${id}/up`,
        method:'put'
    })
}

//删除商品分类
export function deleteGoodsClassify(id){
    return request({
        url:`/coffee-background/category/${id}`,
        method:'delete'
    })
}

