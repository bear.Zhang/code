import request from '@/utils/request'
import api from '../index'

// 获取商品状态
export function getOrderStatus(){
    return request({
        url: `/coffee-background/order/status-count`,
        method: 'post',
    })
}

// 获取商品列表
export function getOrderList(data){
    return request({
        url: `/coffee-background/order/list`,
        method: 'post',
        data:data
    })
}