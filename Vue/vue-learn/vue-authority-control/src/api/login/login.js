import request from '@/utils/request'
import api from '../index'

// 获取Token
export function loginGetToken(data) {
  return request({
    url: api + '/user-service/oauth/token',
    method: 'post',
    headers: {
      'content-type': 'application/x-www-form-urlencoded',
      'mac': data.mac
    },
    data: data.user,
    unloading:true
  })
}

// 获取用户信息
export function getUserInfo(token) {
  return request({
    url: api + '/user-service/user/getUserInfo',
    method: 'get',
    unloading:true
  })
}

// 退出登录
export function outLogin() {
  return request({
    url: api+'/user-service/user/logout',
    method: 'get',
  })
}

