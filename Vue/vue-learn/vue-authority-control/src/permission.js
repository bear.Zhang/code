import router from './router'
import store from './store/index'
import Vue from 'vue'
import {Message} from 'element-ui'
import auth from '@/utils/auth'
// 引入判断是否拥有权限的函数
import { includePermission } from '@/utils/permission'

router.beforeEach(async(to, from, next) => {
  if(auth.token){
    if(!auth.user){
        store.dispatch('getUserInfo').then((res)=>{
          next()
        }).catch((rej)=>{
          Message.error('获取信息失败，以返回登陆页')
          auth.token = null;
          next({path: '/Login'});
        })
    }else{
      try {
        // 这里获取 permissionList
        await store.dispatch('getPermissionList');
        // 这里判断当前页面是否有权限
        const { permissions } = to.meta;
        if (permissions) {
          const hasPermission = includePermission(permissions)
          if (!hasPermission) next({ name:  NoPermission  })
        }
        next()
      }catch{

      }
    }
    next();
  }else{
    if(to.fullPath==='/Login'){
      next();
    }else{
      next('/Login')
    }
  }
})
