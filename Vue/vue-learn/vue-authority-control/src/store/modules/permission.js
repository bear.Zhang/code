import {getUserInfo} from '@/api/login/login';

const user={
    state: {
        permissionList: []
    },
    mutations: {
        updatePermissionList: (state, payload) => {
            state.permissionList = payload
        },
    },
    actions: {
        getPermissionList: async ({ state, commit },token) => {
            // 这里是为了防止重复获取
            if (state.permissionList.length) return;
            // 发送请求获取权限
            getUserInfo(token).then((res)=>{
                console.log(res);
            });
            const list = await getPermissionList();
            console.log(list);
            commit('updatePermissionList', list)
        }
    }
}

export default user;