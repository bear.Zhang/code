import {loginGetToken,getUserInfo,outLogin} from '@/api/login/login';
import auth from '@/utils/auth';

const user={
    state: {
        token:auth.token,
        name:null,
        roles:null
    },
    mutations: {
        SET_TOKEN(state,token){
            state.token=token;
        },
        SET_NAME(state,name){
            state.name=name;
        },
        SET_ROLES(state,roles){
            state.roles=roles;
        }
    },
    actions: {
        loginGetToken({commit},userInfo){
            return new Promise((resolve,reject)=>{
                loginGetToken(userInfo).then((res)=>{
                    const data=res.access_token;
                    commit('SET_TOKEN',data);
                    auth.token=data;
                    resolve(data);
                }).catch((error)=>{
                    reject(error);
                })
            })
        },
        getUserInfo({commit},token){
            return new Promise((resolve,reject)=>{
                getUserInfo(token).then((res)=>{
                    commit('SET_NAME',res.data.realName);
                    auth.user=res.data;
                    resolve(res);
                }).catch((error)=>{
                    reject(error);
                })
            })
        },
        outLogin({commit}){
            return new Promise((resolve,reject)=>{
                outLogin(token).then((res)=>{
                    commit('SET_TOKEN',null);
                    commit('SET_ROLES',null);
                })
            })
        }
    }
}

export default user;