const getters = {
    token: state => state.user.token,
    name: state => state.user.name,
    roles: state => state.user.roles,
    permission:state=>state.permission.permissionList
  }
  export default getters
  