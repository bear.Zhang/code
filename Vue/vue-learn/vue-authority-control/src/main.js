import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './assets/font/iconfont.css'

import store from './store'
import auth from './utils/auth';
import './permission'

Vue.use(ElementUI);

Vue.config.productionTip = false;

Vue.prototype.$auth=auth;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

