export default[{
    path: '/AdminView',
    name: 'AdminView',
    hidden: true,
    meta: {
        title: '系统首页',
        auth: 'AdminView'
    },
    redirect: '/Admin',
    component: () => import('@/views/admin/AdminView'),
    children:[
        {
            path: '/Admin',
            name: 'Admin',
            meta: {
                title: '首页',
                auth: 'Admin',
                affix: true,
            },
            component: 
            () => import('@/views/admin/Admin')
        }
    ]
}]
