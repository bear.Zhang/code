import { includePermission, createRedirectFn } from  '@/utils/permission'

export default [{
        path:'/user',
        name:'User',
        label:'用户',
        redirect: { name:'UserList'},
        // redirect: (to) => {
        //   if (includePermission(children[0].meta.permissions)) return { name: children[0].name }
        //   if (includePermission(children[1].meta.permissions)) return { name: children[1].name }
        // },
        // redirect: createRedirectFn({name:'UserList'},children),
        meta: { 
            permissions:['U_1']
        },
        children: [
          {
            path:'/UserList',
            name:'UserList',
            label:'用户列表',
            meta: { 
                permissions:['U_1_1']
            },
            component:()=>import('@/views/user/userList/UserList')
          },
          {
            path:'/UserGroup',
            name:'UserGroup',
            label:'用户组',
            meta: { 
                permissions:['U_1_2']
            },
            redirect: { name:'UserGroupList'},
            children: [
              {
                path:'/UserGroupList ',
                name:'UserGroupList',
                label:'用户组列表',
                meta: { 
                    permissions:['U_1_2_1']
                },
                component:()=>import('@/views/user/userList/UserList')
              },
              {
                path:'/UserGroupConfig ',
                name:'UserGroupConfig ',
                label:'用户组设置',
                meta: { 
                    permissions:['U_1_2_2']
                },
                component:()=>import('@/views/user/userList/UserList')
              }
            ]
          }
        ]
      },
]






