export default[
    {
        path: '/System',
        name: 'System',
        meta: {
            title: '系统',
            auth: 'System'
        },
        redirect: '/System/UserManage',
        component: 
        () => import('@/views/system/System'),
        children:[
            {
                path: '/System/UserManage',
                name: 'UserManage',
                meta: {
                    title: '用户管理',
                    auth: 'UserManage'
                },
                component: 
                () => import('@/views/system/userManage/UserManage')
            },
            {
                path: '/System/PurviewManage',
                name: 'PurviewManage',
                meta: {
                    title: '角色权限管理',
                    auth: 'PurviewManage'
                },
                component: 
                () => import('@/views/system/purviewManage/PurviewManage')
            }
        ]
    },
]