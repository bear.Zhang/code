import Vue from 'vue'
import VueRouter from 'vue-router'

import homeRouter from './modules/home'
import userRouter from './modules/user'
import systemRouter from './modules/system'
import { createPermissionRouter } from  '@/utils/permission'


Vue.use(VueRouter);

const routesConfig= [
  {
    path: '/',
    name: 'Index',
    component: ()=>import('@/views/Index'),
    redirect: '/AdminView',
    children:[
      ...homeRouter,
      ...userRouter,
      ...systemRouter
    ]
  },
  {
    path: '*',
    name: '404',
    meta: { 
      title:'404'
    },
    component:()=>import('@/views/error/Error')
  },
  {
    path: '/Login',
    name: 'Login',
    meta: { 
      title:'登录'
    },
    component:()=>import('@/views/login/Login')
  }
]

export const routes = routesConfig.map(item => createPermissionRouter(item));

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

/**
 * 重写路由的push方法
 */
const routerPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error=> error)
}


export default router
