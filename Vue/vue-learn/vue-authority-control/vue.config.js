module.exports = {
    configureWebpack: {

    },
    devServer: {
        host: "localhost", 
        port: 8080,
        https: false,
        open: true,
        proxy: {
          '/api': {
            target: 'https://lxerptest.66123123.com',
            ws: true,
            changeOrigin: true,
            pathRewrite: {
              '^/api': '/api'
            }
          },
        }
      }
}