const CompressionWebpackPlugin = require('compression-webpack-plugin');

const compress = new CompressionWebpackPlugin({
    filename: info => {
        return `${info.path}.gz${info.query}`
    },
    algorithm: 'gzip', 
    threshold: 10240,
    test: new RegExp(
        '\\.(' +
        ['js'].join('|') +
        ')$'
    ),
    minRatio: 0.8,
    deleteOriginalAssets: false
});


// 判断环境
const isPord=process.env.NODE_ENV==='production';

const externals = {
    'Vue': "vue",
    'ElementUI': "element-ui",
    'VueRouter': "vue-router",
    'Vuex': "vuex",
    'Axios': "axios"
};

module.exports={
    /**
     * 部署应用包时的基本 URL
     */ 
    publicPath:'./',
    /**
     * 关闭生产环境的 source map 以加速生产环境构建
     */
    productionSourceMap: false,
    devServer:{
        before(app) {
            app.get(/.*.(js)$/, (req, res, next) => {
                req.url = req.url + '.gz';
                res.set('Content-Encoding', 'gzip');
                next();
            })
        },
        port:'8080',
        open:true,
        /**
         * 对所有服务启用gzip压缩
         */
        compress: true,
        proxy: {
            '/api': {
                target: 'https://lxerptest.66123123.com',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/api' : '/api'
                }
            },
        }
    },
    configureWebpack:config=>{
        if (isPord) {
            // 为生产环境修改配置
            config.mode='production';
            var optimizationPro = {
                runtimeChunk: "single",
                splitChunks: {
                    chunks: "all",
                    maxInitialRequests: Infinity,
                    minSize: 20000, // 依赖包超过20000bit将被单独打包
                    cacheGroups: {
                        vendor: {
                            test: /[\\/]node_modules[\\/]/,
                            name(module) {
                                const packageName = module.context.match(
                                    /[\\/]node_modules[\\/](.*?)([\\/]|$)/
                                )[1];
                                return `npm.${packageName.replace("@", "")}`;
                            }
                        }
                    }
                },
                minimizer:[
                    new TerserPlugin({
                        terserOptions: {
                            warnings: false,
                            compress: {
                                warnings: false,
                                drop_console: true, // 是否删除代码中所有的console语句
                                drop_debugger: false,
                                pure_funcs: ["console.log"] // 移除console
                            }
                        },
                    })
                ]
            };
            Object.assign(config, {
                optimization:optimizationPro
            });
            /**
            * 开启gzip压缩
            */ 
            config.plugins.push(compress);
            config.devtool = 'cheap-module-source-map';
        } else {
            // 为开发环境修改配置
            config.mode='development';
            var optimizationDev = {
                runtimeChunk: "single",
                splitChunks: {
                    chunks: "all",
                    maxInitialRequests: Infinity,
                    minSize: 20000, // 依赖包超过20000bit将被单独打包
                    cacheGroups: {
                        vendor: {
                            test: /[\\/]node_modules[\\/]/,
                            name(module) {
                                const packageName = module.context.match(
                                /[\\/]node_modules[\\/](.*?)([\\/]|$)/
                                )[1];
                                return `npm.${packageName.replace("@", "")}`;
                            }
                        }
                    }
                }
            };
            Object.assign(config, {
                optimization: optimizationDev
            });
            config.devtool = 'cheap-module-eval-source-map';
        }
        
    },
    chainWebpack:config=>{
        /**
         *  最小化代码
         */
        config.optimization.minimize(true);
        /**
         * 分割代码
         */
        config.optimization.splitChunks({
            chunks: 'all'
        });
        /**
         * 压缩图片
         */
        config.module
            .rule('images')
            .use('image-webpack-loader')
            .loader('image-webpack-loader')
            .options({
                bypassOnDebug: true
            })
            .end()
        /**
        * 配置 externals 引入 cdn 资源
        */
        config.externals=externals;
        

    },
    css:{
        /**
         * css是否开启sourcemap,生成环境不生成
         */
        sourceMap:!isPord,
        /**
         * 是否将组件中的 CSS 提取至一个独立的 CSS 文件中，生产环境下是 true，开发环境下是 false
         */
        extract:isPord
    }
}