const WebSocket =require('ws');
const http = require('http');

const server=http.createServer();

const wss=new WebSocket.Server({
    server:server
});

console.log('开始连接');

wss.on('connection',(ws)=>{
    ws.on('message',(message)=>{
        console.log(message);
        wss.clients.forEach((client)=>{
            client.send('服务器来数据');
        })
    })

    ws.on('close',()=>{
        console.log('服务器连接关闭');
    })
})

server.listen(2021);