const externals = {
    'Vue': "vue",
    'VueRouter': "vue-router",
    'Vuex': "vuex",
    'Axios': "axios"
};
// 判断环境
const isPord = process.env.NODE_ENV === 'production'
module.exports={
    publicPath:'/',
    productionSourceMap:false,
    assetsDir: 'statick',
    devServer:{
        host:'0.0.0.0',
        port:8021,
        open:true,
        inline: true,
        compress: true,
        overlay: {
            warnings: true,
            errors: true
        },
        proxy: {
            '/sinobot1': {
                target: 'http://10.8.192.82:29999',
                ws: true,
                changeOrigin: true,
                bypass: function(req) {
                    if (req.headers.accept.indexOf("html") !== -1) {
                        console.log("Skipping proxy for browser request.");
                        return "/index.html";
                    }
                },
                pathRewrite: {
                    '^/' : '/'
                }
            },
        }
    },
    configureWebpack:config=>{
        if (isPord) {
            // 为生产环境修改配置
            config.mode='production';
            var optimizationPro = {
                runtimeChunk: "single",
                splitChunks: {
                    chunks: "all",
                    maxInitialRequests: Infinity,
                    minSize: 20000, // 依赖包超过20000bit将被单独打包
                    cacheGroups: {
                        vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        name(module) {
                            const packageName = module.context.match(
                            /[\\/]node_modules[\\/](.*?)([\\/]|$)/
                            )[1];
                            return `npm.${packageName.replace("@", "")}`;
                        }
                        }
                    }
                },
                minimize:true
            };
            Object.assign(config, {
                optimization:optimizationPro
            });
            config.devtool = 'cheap-module-source-map';
        } else {
            // 为开发环境修改配置
            config.mode='development';
            var optimizationDev = {
                runtimeChunk: "single",
                splitChunks: {
                    chunks: "all",
                    maxInitialRequests: Infinity,
                    minSize: 20000, // 依赖包超过20000bit将被单独打包
                    cacheGroups: {
                        vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        name(module) {
                            const packageName = module.context.match(
                            /[\\/]node_modules[\\/](.*?)([\\/]|$)/
                            )[1];
                            return `npm.${packageName.replace("@", "")}`;
                        }
                        }
                    }
                }
            };
            Object.assign(config, {
                optimization: optimizationDev
            });
            config.devtool = 'cheap-module-eval-source-map';
        }
    },
    chainWebpack:config=>{
        // 移除prefetch插件
        config.plugins.delete('prefetch');
        // 最小化代码
        config.optimization.minimize(true);
        config.externals=externals;
    },
    css:{
        sourceMap: !isPord,
        extract:isPord
    }
}