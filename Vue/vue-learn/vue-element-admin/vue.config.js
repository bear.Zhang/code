const CompressionWebpackPlugin = require('compression-webpack-plugin');

const compress = new CompressionWebpackPlugin({
    filename: info => {
        return `${info.path}.gz${info.query}`
    },
    algorithm: 'gzip', 
    threshold: 10240,
    test: new RegExp(
        '\\.(' +
        ['js'].join('|') +
        ')$'
    ),
    minRatio: 0.8,
    deleteOriginalAssets: false
});
const is_Production=process.env.NODE_ENV==='production';
const externals = {
    'Vue': "vue",
    'VueRouter': "vue-router",
    'Vuex': "vuex",
    'ELEMENT': "element-ui",
    'axios': "axios",
    'js-cookie':'Cookie'
};

module.exports={
    publicPath:'./',
    productionSourceMap: false,
    assetsDir:'statick',
    devServer:{
        host:'localhost',
        open:true,
        overlay: {
			warnings: false,
			errors: true
        },
        compress: true,
        proxy: {
            '/api': {
                target: 'https://lxerptest.66123123.com',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/api' : '/api'
                }
            },
        }
    },
    configureWebpack:config=>{
        if(is_Production){
            // 开启gzip压缩
            config.plugins.push(compress);
            config.devtool="cheap-module-source-map"
        }else{
            config.mode='development';
            var optimizationDev = {
                runtimeChunk: "single",
                splitChunks: {
                    chunks: "all",
                    maxInitialRequests: Infinity,
                    minSize: 20000, // 依赖包超过20000bit将被单独打包
                    cacheGroups: {
                        vendor: {
                            test: /[\\/]node_modules[\\/]/,
                            name(module) {
                                const packageName = module.context.match(
                                /[\\/]node_modules[\\/](.*?)([\\/]|$)/
                                )[1];
                                return `npm.${packageName.replace("@", "")}`;
                            }
                        }
                    }
                }
            };
            Object.assign(config, {
                optimization: optimizationDev
            });
            config.devtool="cheap-module-eval-source-map"
        }
        config.externals=externals;
    },
    chainWebpack:config=>{
        config.plugins.delete('prefetch');
        if(is_Production){
        }else{
        }
        config.optimization.minimize(true);
        config.optimization.splitChunks({
            chunks: 'all'
        });
        // 兼容IE
        config.entry('main').add('babel-polyfill')
    },
    css:{
        sourceMap: !is_Production,
        extract:is_Production
    }
}