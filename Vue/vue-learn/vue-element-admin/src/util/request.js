import axios from 'axios'
import router from '@/router'

// 创建axios实例
const service = axios.create({
    timeout: 15000
})

service.all=axios.all;
service.spread=axios.spread;

// request 拦截器
service.interceptors.request.use( 
    config => {
        if (auth.token) {
            if(auth.token=='message'){
                config.headers['Authorization'] =null;
            }else{
            config.headers['Authorization'] = "Bearer " + auth.token // 让每个请求携带自定义token 请根据实际情况自行修改
            }
        } else {
            config.headers['Authorization'] = "Basic V0VCQVBQOldFQkFQUA=="
        }
        !config.unloading 
        // && store.dispatch('reviseLoading', true)
        return config
    },
    error => {
        Promise.reject(error)
    }
)

// response 拦截器
service.interceptors.response.use(
    response => {
        const res = response.data
        if (res.code) {
            if (res.code != '000000' && res.code != '000000000' && /^98/.test(res.code) && !response.config.exception) {//自定义异常处理不走默认异常处理
                Message({
                    message: res.message,
                    showClose: true,
                    type: 'error',
                })
                return response.data
            } else {
                return response.data
            }
        } else {
            return response.data
        }
    },
    error => {
        //98开头的code吗 或者 自定义异常处理不走默认异常处理 010090066的code是参数模板状态发生变化的状态码
        if((error.response.data && /^98/.test(error.response.data.code)) || (error.response.config && error.response.config.exception)){
            return error.response.data;
        }
        Message({
            message: (error.response && error.response.data && error.response.data.data && error.response.data.data.error_description) || (error.response && error.response.data && error.response.data.message) || (error.response && error.response.statusText) || error.message,
            showClose: true,
            type: 'error',
        })
        const { status } = error.response
        if (status == 401) {
            router.push('/login')
        }
        return Promise.reject(error)
    }
)



export default service