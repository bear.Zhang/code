import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import './style/viewport.scss'

import { Button, Toast, Infiniteloading, Swiper, Switch, Tabbar } from '@nutui/nutui';
Button.install(Vue);
Switch.install(Vue);
Tabbar.install(Vue);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
