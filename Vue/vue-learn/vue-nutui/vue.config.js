const pxtorem = require('postcss-pxtorem');
module.exports = {
    css: {
        loaderOptions: {
            scss: {
                // @/ 是 src/ 的别名
                // 注意：在 sass-loader v7 中，这个选项名是 "data"
                prependData: ` 
                @import "@nutui/nutui/dist/styles/index.scss";
                `,
            },
            postcss: {
                plugins: [
                    pxtorem({
                        rootValue: 37.5,
                        propList: ['*']
                    })
                ]
            }
        },
    }
}