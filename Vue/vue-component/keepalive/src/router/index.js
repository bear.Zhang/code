import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Home from '@/components/Home'
import KeepaliveComponent from '@/components/KeepaliveComponent'
import NoKeepaliveComponent from '@/components/NoKeepaliveComponent'
import A from '@/components/A'
import B from '@/components/B'
import C from '@/components/C'

import Parmeter from '@/components/parameter/Parameter'
import NoKeep from '@/components/parameter/NoKeep'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/HelloWorld',
      name: 'HelloWorld',
      component: HelloWorld,
      children:[
        {
          path: '/HelloWorld/KeepaliveComponent',
          name: 'KeepaliveComponent',
          component: KeepaliveComponent,
          meta:{
            keepAlive:true
          }
        },
        {
          path: '/HelloWorld/NoKeepaliveComponent',
          name: 'NoKeepaliveComponent',
          component: NoKeepaliveComponent,
        },
        {
          path: '/HelloWorld/A',
          name: 'A',
          component: A,
        },
        {
          path: '/HelloWorld/B',
          name: 'B',
          component: B,
        },{
          path: '/HelloWorld/C',
          name: 'C',
          component: C,
        }
      ]
    },
    {
      path: '/Parameter',
      name: 'Parameter',
      component: Parmeter,
      children:[
        {
          path: '/Parameter/NoKeep',
          name: 'NoKeep',
          component: NoKeep,
        },
      ]
    }
  ]
})
