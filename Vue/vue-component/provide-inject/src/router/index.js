import Vue from 'vue'
import Router from 'vue-router'
import Parent1 from '@/components/Parent1'
import Parent2 from '@/components/Parent2'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Parent1',
      component: Parent1
    }
  ]
})
