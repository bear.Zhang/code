// 组件名称首字母大写
function changeStr(str){
    return str.charAt(0).toUpperCase()+str.slice(1);
}

const requireCom=require.context('./',false,/\.vue$/);

const install=(Vue)=>{
    requireCom.keys().forEach(fileName => {
        let config=requireCom(fileName);
        let comName=changeStr(fileName.replace(/^\.\//,'').replace(/\.\w+$/,''));
        // 注册组件
        Vue.component(comName,config.default||config);
    });
} 

export default{
    install
}
