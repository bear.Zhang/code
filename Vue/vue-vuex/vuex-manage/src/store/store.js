import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);

var store=new Vuex.Store({
    state:{
        count:0,
        type:false,
        num:2,
        list:[1,2,3,4,5,6],
        listData:[
            {
                id:1,
                text:'11'
            },
            {
                id:2,
                text:'22'
            }
        ]
    },
    mutations:{
        increment(state){
            state.count++;
        },
        decrease(state){
            state.count--;
        }
    },
    getters:{
        fliterList:state=>{
            return state.list.filter((item)=>item<3);
        },
        listLength:(state,getters)=>{
            return getters.fliterList.length;
        },
        getListDataById: (state) => (id) => {
            return state.listData.find(item => item.id === id)
        }
    }
})

export default store;