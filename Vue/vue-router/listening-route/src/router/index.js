import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import One from '@/components/One'
import Tow from '@/components/Tow'
import About from '@/components/About'
import AboutOne from '@/components/AboutOne'
import AboutTow from '@/components/AboutTow'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      children:[
        {
          path: '/Home/One',
          name: 'One',
          component: One,
        },
        {
          path: '/Home/Tow',
          name: 'Tow',
          component: Tow,
        }
      ]
    },
    {
      path: '/About',
      name: 'About',
      component: About,
      children:[
        {
          path: '/About/AboutOne',
          name: 'AboutOne',
          component: AboutOne,
        },
        {
          path: '/About/AboutTow',
          name: 'AboutTow',
          component: AboutTow,
        }
      ]
    }
  ]
})
