// 静态对象类型

// 对象类型，值必须是指定类型
const person:{
    name:string,
    age:number
}={
    name:'boy',
    age:23
};

// 数组类型，数组子元素必须是字符串
const persons:string []=['boy1','body2','boy3'];

// 类类型，元素属于类型
class Customer{}
const cus:Customer=new Customer();

// 函数类型，返回必须是字符串
const fn:()=>string=()=>{
    return '函数'
}

// 函数返回类型，注解
function getTotal(one:number,tow:number):number{
    return one+tow
}

const total=getTotal(1,2);
