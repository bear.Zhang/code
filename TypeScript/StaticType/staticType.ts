// 数据静态类型，保证了数据的健壮性
let count:number=1;
console.log(count);

// 自定义静态类型
interface persion{
    uname:string,
    age:number
}
let boy:persion={
    uname:'zhang',
    age:12
};
console.log(boy.age);
