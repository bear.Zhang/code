class ArrayClass extends Array{
    constructor(...args){
        super(...args);
    }
    // 数组求和
    arrSum(){
        let curArr=[...this];
        return curArr.reduce((acc,cur)=>acc+cur,0)
    }
    // 数组求最大值
    arrMax(){
        let curArr=[...this];
        return Math.max(...curArr)
    }
}

let arrClass=new ArrayClass();
arrClass.push(1);
arrClass.push(2);
let sum=arrClass.arrSum();
console.log(sum);
let max=arrClass.arrMax();
console.log(max);