class StringClass extends String{
    constructor(str){
        super(str)
    }
    // 判断是否值字符串
    judgeStr(){
        return Object.prototype.toString.call(this).indexOf('String')!==-1;
    }
    // 字符串大写
    letterUppercase(){
        let isStr=this.judgeStr(this);
        return isStr?this.toUpperCase():new Error('get String');
    }
    // 首字母大写
    fristLetterUppercase(){
        let isStr=this.judgeStr(this);
        return isStr?this.charAt(0).toUpperCase()+this.slice(1):new Error('get String');
    }
}

let strClass=new StringClass('str');
console.log(strClass.judgeStr()); // true
console.log(strClass.letterUppercase()); // STR
console.log(strClass.fristLetterUppercase()); // Str
console.log(strClass.indexOf('s')); // 0
console.log(strClass.substr(1)); // tr