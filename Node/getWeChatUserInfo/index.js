const https=require('https');
const express=require('express');
const path = require("path");

let app=express();
app.set('view engine','ejs');

let host='http://127.0.0.1:8800';
// let host='http://192.168.43.86:8800'

// 获取微信code参数 
let appID='wx2b8c70656608e4c0';
let appsecret='44c2f7bab1e5f48424cf8402f519d2e7';
let redirect_uri='/getWeChatUserInfo';
let scope='snsapi_userinfo';

// 获取微信code的地址
let authorizeUrl=`https://open.weixin.qq.com/connect/oauth2/authorize?appid=${appID}&redirect_uri=${host}${redirect_uri}&response_type=code&scope=${scope}&state=STATE#wechat_redirect`;

app.get("/login", function(req, res) {
    res.sendFile(path.resolve(__dirname,'login.html'));
});

// 把code展示在地址栏
app.get('/auth',(req,res)=>{
    res.writeHead(302,{
        'Location':authorizeUrl
    });
    res.end();
});

app.get('/getWeChatUserInfo',(req,res)=>{
    getCode(req,res);
})

// 获取地址栏中code
async function getCode(req,res){
    let code=req.query.code;
    let accessTokenObj=await getAccessToken(code);
    let userInfo=await getUserInfo(accessTokenObj.access_token,accessTokenObj.openid);
    console.log(userInfo);
    // 把用户信息渲染的ejs页面
    res.render(path.resolve(__dirname,'userInfo.ejs'), {userInfo: userInfo});
}

// 获取access_token和openID
function getAccessToken(code){
    let accessTokenUrl=`https://api.weixin.qq.com/sns/oauth2/access_token?appid=${appID}&secret=${appsecret}&code=${code}&grant_type=authorization_code`;
    return new Promise((resolve,reject)=>{
        https.get(accessTokenUrl,(res)=>{
            var resbody='';
            res.on('data',(d)=>{
                resbody+=d;
            });
            res.on('end',()=>{
                let resObj=JSON.parse(resbody);
                resolve(resObj);
            })
        }).on('error',(e)=>{
            console.error('error：',e);
        })
    })
};

// 获取微信用户的基本信息
function getUserInfo(access_token,openid){
    let userInfoUrl=`https://api.weixin.qq.com/sns/userinfo?access_token=${access_token}&openid=${openid}&lang=zh_CN`;
    return new Promise((resolve,reject)=>{
        https.get(userInfoUrl,(res)=>{
            var resbody='';
            res.on('data',(d)=>{
                resbody+=d;
            });
            res.on('end',()=>{
                let resObj=JSON.parse(resbody);
                resolve(resObj);
            })
        }).on('error',(e)=>{
            console.error('error：',e);
        })
    })
}

app.listen(8800)