// 
setImmediate(()=>{
    console.log(1);
},2000)

// 延迟定时器
setTimeout(()=>{
    console.log(2);
},2000);

// 间隔定时器
setInterval(()=>{
    console.log(3);
},2000);

// 
process.nextTick(()=>{
  console.log(4);
});

// 回调函数
Promise.resolve().then(()=>{
    console.log(5);
});

// 同步函数
(()=>{
   console.log(6);
})()

// 运行结果： 6、4、5、1、2、3、3...   
