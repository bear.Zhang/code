function fn1(){
    setTimeout(function() {
        console.log('a')
    });
      
    new Promise(function(resolve) {
        console.log('b');
      
        for(var i =0; i <10000; i++) {
          i ==99 && resolve();
        }
    }).then(function() {
        console.log('c')
    });
        
    console.log('d');
}

fn1();// b d c a

function fn2(){
    console.log('a');

    setTimeout(function() {
        console.log('b');
        process.nextTick(function() {
            console.log('c');
        })
        new Promise(function(resolve) {
            console.log('d');
            resolve();
        }).then(function() {
            console.log('e')
        })
    })
    process.nextTick(function() {
        console.log('f');
    })
    new Promise(function(resolve) {
        console.log('g');
        resolve();
    }).then(function() {
        console.log('h')
    })

    setTimeout(function() {
        console.log('i');
        process.nextTick(function() {
            console.log('j');
        })
        new Promise(function(resolve) {
            console.log('k');
            resolve();
        }).then(function() {
            console.log('l')
        })
    })
}

//fn2();// a g f h b d i k c j e l

function fn3(){
    console.log(1)

    setTimeout(() => {
        console.log(5)
        new Promise((resolve, reject) => {
            console.log(6)
            resolve()
        }).then(() => {
            console.log(7)
        })
    }, 0)

    new Promise((resolve, reject) => {
        console.log(2)
        resolve()
    }).then(() => {
        console.log(4)
    })

    setTimeout(() => {
        console.log(8)
        new Promise((resolve, reject) => {
            console.log(9)
            resolve()
        }).then(() => {
            console.log(10)
        })
    }, 0)

    console.log(3)
}
fn3();
// Node-：1 2 3 4 5 6 8 9 7 10 
// 浏览器环境/Node+：1 2 3 4 5 6 7 8 9 10
