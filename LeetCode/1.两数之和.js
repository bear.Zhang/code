/*
 * @lc app=leetcode.cn id=1 lang=javascript
 *
 * [1] 两数之和
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
    let numsLenth=nums.length;
    let result=[];
    for(let i=0;i<numsLenth;i++){
        for(let j=i+1;j<numsLenth;j++){
            if(nums[i]+nums[j]==target){
                result.push(i);
                result.push(j);
                return result;
            }
        }
    }
    return result;
};

console.log(twoSum([2, 7, 11, 15],9));
// @lc code=end

